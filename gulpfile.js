process.env.NODE_ENV = 'production';

var config = require('config');

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var collapse = require('bundle-collapser/plugin');
var streamify = require('gulp-streamify');
var bump = require('gulp-bump');

var header = require('gulp-header');

var gutil = require('gulp-util');
var exec = require('child_process').exec;

var fs = require('fs-extra');

var sourcemaps = require('gulp-sourcemaps'),
    buffer = require('vinyl-buffer');

var runSequence = require('run-sequence');

var purify = require('gulp-purifycss');

var concatCss = require('gulp-concat-css');

var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');

var s3 = require('gulp-s3');
var gzip = require('gulp-gzip');
var rimraf = require('rimraf');

//uglifyjs ./content/javascripts/build.js --output=./content/javascripts/build.js -m --mangle-props --mangle-regex="/^_/" -c

var path = {
  MINIFIED_OUT: 'main-base.bundle.js',
  OUT: 'main-base.bundle.js',
  DEST: 'content',
  DEST_BUILD: 'content/dist/js',
  DEST_SRC: 'content/javascripts',
  ENTRY_POINT: './core/client/index.js'
};

gulp.task('build::css', function(done){

    runSequence('pre-build', 'purify', 'concatCss', 'header-css', done);

});

gulp.task('build-css-dev', function(done){

    runSequence('pre-build', 'purify', 'concatCss', done);

});

gulp.task('deploy', function(done){

    runSequence('bump', 'build::css', 'build::js', 'git', 's3-upload', 's3-crossdomain', done);

});

gulp.task('dist-remove', function(done){

    return rimraf('./content/dist', done);

});

gulp.task('git', function(done){

    runSequence('git-add', 'git-commit', 'git-push', done);

});

gulp.task('bump', function(){
  return gulp.src('./package.json')
        .pipe(bump())
        .pipe(gulp.dest('./'));
});

gulp.task('header-css', function(done) {

    var pkg = fs.readJsonSync('./package.json'),
        template = '/*! <%= pkg.title %> v<%= pkg.version %> | <%= pkg.homepage %> | (c) '+
        new Date().getFullYear() + ' <%= pkg.company %> | MIT License */\n';

    return gulp.src('./content/stylesheets/main-base.css')
    .pipe(header(template, { 'pkg' : pkg } ))
    .pipe(gulp.dest('./content/dist/css'));
});

gulp.task('header-js', function(done) {

    var pkg = fs.readJsonSync('./package.json'),
        template = '/*! <%= pkg.title %> v<%= pkg.version %> | <%= pkg.homepage %> | (c) '+
        new Date().getFullYear() + ' <%= pkg.company %> | MIT License */\n';

    return gulp.src('./content/dist/js/main-base.bundle.js')
    .pipe(header(template, { 'pkg' : pkg } ))
    .pipe(gulp.dest('./content/dist/js'));
});

gulp.task('concatCss', function(){

  return gulp.src([
        './content/stylesheets/**/*.css',
        './node_modules/ladda/dist/ladda-themeless.min.css',
        './node_modules/video.js/dist/video-js.css',
        '!./content/stylesheets/main-base.css',
        '!./content/stylesheets/pages.css',
        '!./content/stylesheets/bootstrap.css',
        '!./content/stylesheets/icons.css'
        ])
        .pipe(autoprefixer({
            browsers: ['last 8 versions'] 
        }))
        .pipe(concatCss('main-base.css'))
        .pipe(minifyCss({compatibility: 'ie7', keepSpecialComments : 0}))
        .pipe(gulp.dest('./content/stylesheets'));
});

gulp.task('purify', function() {
  return gulp.src('./content/stylesheets/pre-build.css')
    .pipe(purify(['./core/client/**/*.js']))
    .pipe(gulp.dest('./content/stylesheets/'));
});

gulp.task('pre-build', function(){
    return gulp.src(['./content/stylesheets/bootstrap.css', './content/stylesheets/pages.css', './content/stylesheets/icons.css'])
        .pipe(concatCss('pre-build.css'))
        .pipe(gulp.dest('./content/stylesheets'));
})

gulp.task('git-add', function(done){

    var cmd = 'git add . -A',
        cwd = process.cwd;

    exec(cmd, {cwd: cwd}, function(err, stdout, stderr){
        if (err) done(err);
        
        gutil.log(stdout, stderr);

        return done();
    });

});

gulp.task('git-commit', function(done){

    var pkg = fs.readJsonSync('./package.json'),
        commit = gutil.env.m || 'Commit ' + Date.now(),
        msg = commit + ' - v' + pkg.version;

    var cmd = 'git commit -m "' + msg + '"',
        cwd = process.cwd;

    exec(cmd, {cwd: cwd}, function(err, stdout, stderr){
        if (err) done(err);
        
        gutil.log(stdout, stderr);

        return done();
    });

});

gulp.task('git-push', function(done){

    var cmd = 'git push origin master',
        cwd = process.cwd;

    exec(cmd, {cwd: cwd}, function(err, stdout, stderr){
        if (err) done(err);
        
        gutil.log(stdout, stderr);

        return done();
    });

});

gulp.task('default', function() {
  return browserify(path.ENTRY_POINT)
    .bundle()
    .pipe(source(path.OUT))
    .pipe(gulp.dest(path.DEST_SRC))
});

gulp.task('s3-upload', function(){
    var aws = {
      'key': config.get('aws.s3.accessKeyId'),
      'secret': config.get('aws.s3.secretAccessKey'),
      'bucket': config.get('buckets.assets'),
      'region': config.get('aws.s3.region')
    };
    return gulp.src(['./content/dist/**'])
        .pipe(gzip())
        .pipe(s3(aws, {
            headers:{
                'Cache-Control': 'max-age=630720000, public'
            },
            gzippedOnly: true,
            uploadPath: '_a/'
        }));
});

gulp.task('s3-upload-js', function(){
    var aws = {
      'key': config.get('aws.s3.accessKeyId'),
      'secret': config.get('aws.s3.secretAccessKey'),
      'bucket': config.get('buckets.assets'),
      'region': config.get('aws.s3.region')
    };
    return gulp.src(['./content/dist/js/video-js.swf', './content/dist/js/main-base.bundle.js', './content/dist/js/main-base.bundle.js.map'])
        .pipe(gzip())
        .pipe(s3(aws, {
            headers:{
                'Cache-Control': 'max-age=630720000, public'
            },
            gzippedOnly: true,
            uploadPath: '_a/js/'
        }));
});

gulp.task('s3-upload-css', function(){
    var aws = {
      'key': config.get('aws.s3.accessKeyId'),
      'secret': config.get('aws.s3.secretAccessKey'),
      'bucket': config.get('buckets.assets'),
      'region': config.get('aws.s3.region')
    };
    return gulp.src(['./content/dist/css/main-base.css'])
        .pipe(gzip())
        .pipe(s3(aws, {
            headers:{
                'Cache-Control': 'max-age=630720000, public'
            },
            gzippedOnly: true,
            uploadPath: '_a/css/'
        }));
});

gulp.task('s3-crossdomain', function(){
    var aws = {
      'key': config.get('aws.s3.accessKeyId'),
      'secret': config.get('aws.s3.secretAccessKey'),
      'bucket': config.get('buckets.output'),
      'region': config.get('aws.s3.region')
    };
    return gulp.src('./content/crossdomain.xml')
        .pipe(gzip())
        .pipe(s3(aws, {
            headers:{
                'Cache-Control': 'max-age=630720000, public'
            },
            gzippedOnly: true
        }));
});

gulp.task('s3-default', function(){
    var aws = {
      'key': config.get('aws.s3.accessKeyId'),
      'secret': config.get('aws.s3.secretAccessKey'),
      'bucket': config.get('buckets.assets'),
      'region': config.get('aws.s3.region')
    };
    return gulp.src('./content/posters/default.jpg')
        .pipe(gzip())
        .pipe(s3(aws, {
            headers:{
                'Cache-Control': 'max-age=630720000, public'
            },
            gzippedOnly: true,
            uploadPath: 'posters/'
        }));
});

gulp.task('build::js', function(done){

    runSequence(
        'build-js',
        ['build-js-copy-plyr', 'build-copy-fonts', 'build-copy-images'],
        'header-js',
        done
    );

});

gulp.task('build-js-copy-plyr', function(done){
    return gulp.src('./content/javascripts/*.swf')
    .pipe(gulp.dest('./content/dist/js/'));
});

gulp.task('build-copy-fonts', function(done){
    return gulp.src('./content/fonts/*')
    .pipe(gulp.dest('./content/dist/fonts/'));
});

gulp.task('build-copy-images', function(done){
    return gulp.src('./content/images/*')
    .pipe(gulp.dest('./content/dist/images/'));
});

gulp.task('build-js', function(){
  return browserify({
    entries: [path.ENTRY_POINT]
  })
    .plugin(collapse)
    .bundle()
    .pipe(source(path.MINIFIED_OUT))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify({
        compress: {
            sequences: true,
            dead_code: true,
            conditionals: true,
            booleans: true,
            unused: true,
            if_return: true,
            join_vars: true,
            drop_console: true
        }
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(path.DEST_BUILD));
});

gulp.task('watch', ['default'], function () {
    gulp.watch(['./*.js', './core/client/**/*.js', '!./gulpfile.js'], ['default']);
});