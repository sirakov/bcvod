var AWS = require('aws-sdk'),
    async = require('async'),
    _ = require('lodash'),
    config = require('./config/production');

AWS.config.update(config.aws.default);

var ec2 = new AWS.EC2(),
    ssh = require('ssh2').Client,
    privateKey = require('fs-extra').readFileSync('./id_rsa'),
    logger = require('winston'),
    firstInstance;

var params = {Filters: [{Name: 'instance.group-name', Values: ['AutoScaling-Security-Group-1']}]};

async.waterfall([
    function(callback) {
        ec2.describeInstances(params, function(err, response) {

            if (err) return callback(err);

            var Instances = _.flatten(_.pluck(response.Reservations, 'Instances'));

            firstInstance = Instances[0].InstanceId;

            return callback(null, _.pluck(Instances, 'PublicIpAddress'));

        });
    },
    function(hosts, callback){

        var servers = _.map(hosts, function(host){
            
            return function(cb){
                
                var conn = new ssh();
                    conn.on('ready', function() {

                      console.log('Connected to host :'+ host);

                      conn.exec('cd /home/ubuntu/bcvod &&'+
                                'sudo git pull origin master &&'+
                                'sudo npm install --production &&'+
                                'sudo pm2 restart kukuotv', function(err, stream) {

                        if (err) return cb(err);

                        stream.on('close', function(code, signal) {
                          
                          conn.end();

                          return cb();
                        }).on('data', function(data) {

                          console.log(data.toString());

                        }).stderr.on('data', function(data) {

                          console.error(data.toString());

                        });
                      });
                    }).connect({
                      host: host,
                      port: 22,
                      username: 'ubuntu',
                      privateKey: privateKey
                    });

            };

        });

        async.series(servers, function(err, results){

            if (err) return callback(err);

            return callback();

        });
    }
], function (err) {
    
    if (err) return console.log(err);

    console.log(firstInstance);

});