_Last updated:_ January 10, 2016.

Welcome to KukuoTv! We are an ad supported video on demand streaming service that provides our users with premium African movies, television shows and music, and any other audio-visual entertainment streamed over the Internet to certain Internet-connected TV's, computers and other devices. Please take some time to keep yourself updated with our terms and conditions (“Terms of Use”)

#### **OVERVIEW**

This website is operated by Blue Canvas Technologies Limited. 
These Terms and Conditions govern your use of our service. As used in these Terms and Conditions, "KukuoTv service," "our service" or "the service" means the service provided as KukuoTv by Blue Canvas Technologies Limited for discovering and watching movies & TV shows, including all features and functionalities, website, and user interfaces, as well as all content and software associated with our service.

Throughout the site, the terms “we”, “us” and “our” refer to Blue Canvas Technologies Limited. Blue Canvas Technologies Limited offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.

By visiting our site, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Use”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Use apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.

#### **1. Acceptance of Terms of Use**

* These Terms of Use govern your use of the KukuoTv service. By using, visiting, or browsing the KukuoTv service, you accept and agree to these Terms of Use. If you do not agree to these Terms of Use, do not use the KukuoTv service.

* By agreeing to these Terms of Service, you represent that you are at least 18 years of age or at least the age of majority in your country of residence, or that you are the age of majority in your country of residence and you have given us your consent to allow any of your minor dependents to use this site.

#### **2. Changes to These Terms**

Blue Canvas Technologies Limited may, from time to time, change these Terms of Use, including the Privacy Policy. Such revisions shall be effective immediately; provided however, for existing members, such revisions shall, unless otherwise stated, be effective 30 days after posting.

#### **3. Privacy and Use of Personal Information**

Personally identifying information is subject to our Privacy Policy **[http://kukuotv.com/policy](http://kukuotv.com/policy)**, the terms of which are incorporated herein. Please review our Privacy Policy to understand our practices. By agreeing to these Terms, you agree that your presence on the KukuoTv Site and use of the Services through any other Access Point are governed by the KukuoTv Privacy Policy in effect at the time of your use.

#### **4. Use of KukuoTv Service**

* ##### **Age Limits:**
You must be 18 years of age, or the age of majority in your country, to become a member of the KukuoTv service. Individuals under the age of 18, or applicable age of majority, may utilize the service only with the involvement of a parent or legal guardian, under such person's account and otherwise subject to these Terms of Use as The Services are not intended to be used by children without involvement and approval of a parent or guardian.

* ##### **Your License:**
We grant you a non-exclusive limited license to use the Services, including accessing and viewing the Content on a streaming-only basis through the Video Player, for personal, non-commercial purposes as set forth in these Terms. You agree not to use the service for public performances.

* ##### **The Content:**
You agree to use the KukuoTv service, including all features and functionalities associated therewith, in accordance with all applicable laws, rules and regulations, or other restrictions on use of the service or content therein. You agree not to archive, download (other than through caching necessary for personal use), reproduce, distribute, modify, display, perform, publish, license, create derivative works from, offer for sale, or use (except as explicitly authorized in these Terms of Use) content and information contained on or obtained from or through the KukuoTv service without express written permission from the service and its licensors. You also agree not to: circumvent, remove, alter, deactivate, degrade or thwart any of the content protections in the KukuoTv service; use any robot, spider, scraper or other automated means to access the KukuoTv service; decompile, reverse engineer or disassemble any software or other products or processes accessible through the KukuoTv service; insert any code or product or manipulate the content of the KukuoTv service in any way; or, use any data mining, data gathering or extraction method. In addition, you agree not to upload, post, e-mail or otherwise send or transmit any material designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment associated with the KukuoTv service, including any software viruses or any other computer code, files or programs.

* ##### **Updates:**
We continually update the KukuoTv service, including the content library. In addition, we continually test various aspects of our service, including our website, user interfaces, availability of movies, TV shows and music, delivery and advertising. We reserve the right to, and by using our service you agree that we may, include you in or exclude you from these tests without notice. We reserve the right in our sole and absolute discretion to make changes from time to time and without notice in how we offer and operate our service.

* ##### **Ownership:**
You agree that Blue Canvas Technologies Limited owns and retains all rights to the Services. You further agree that the Content you access and view as part of the Services is owned or controlled by Blue Canvas Technologies Limited and KukuoTv licensors. The Services and the Content are protected by copyright, trademark, and other intellectual property laws.

* ##### **Your Responsibilities:**
In order for us to keep the Services safe and available for everyone to use, we all have to follow the same rules of the road. You and other users must use the Services for lawful, non-commercial, and appropriate purposes only. Your commitment to this principle is critical. You agree to observe the Services, Content, Video Player and embedding restrictions detailed above, and further agree that you will not use the Services in a way that:
  1. violates the rights of others, including patent, trademark, trade secret, copyright, privacy, publicity, or other proprietary rights;
  2. uses technology or other means to access, index, frame or link to the Services (including the Content) that is not authorized by Blue Canvas Technologies Limited (including by removing, disabling, bypassing, or circumventing any content protection or access control mechanisms intended to prevent the unauthorized download, stream capture, linking, framing, reproduction, access to, or distribution of the Services);
  3. involves accessing the Services (including the Content) through any automated means, including "robots," "spiders," or "offline readers" (other than by individually performed searches on publicly accessible search engines for the sole purpose of, and solely to the extent necessary for, creating publicly available search indices - but not caches or archives - of the Services and excluding those search engines or indices that host, promote, or link primarily to infringing or unauthorized content);
  4. introduces viruses or any other computer code, files, or programs that interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment;
  5. damages, disables, overburdens, impairs, or gains unauthorized access to the Services, including KukuoTv’s servers, computer network, or user accounts;
  6. removes, modifies, disables, blocks, obscures or otherwise impairs any advertising in connection with the Services (including the Content);
  7. uses the Services to advertise or promote services that are not expressly approved in advance in writing by Blue Canvas Technologies Limited;
  8. collects information in violation of KukuoTv’s Privacy Policy;
  9. encourages conduct that would constitute a criminal offense or give rise to civil liability;
  10. violates these Terms or any guidelines or policies posted by Blue Canvas Technologies Limited;
  11. interferes with any other party's use and enjoyment of the Services; or
  12. Attempts to do any of the foregoing.

  If Blue Canvas Technologies Limited determines in its sole discretion that you are violating any of these Terms, we may:

    1. notify you, and 
    2. Use technical measures to block or restrict your access or use of the Services.

  In either case, you agree to immediately stop accessing or using in any way (or attempting to access or use) the Services, and you agree not to circumvent, avoid, or bypass such restrictions, or otherwise restore or attempt to restore such access or use.

* ##### **No Spam/Unsolicited Communications.**
We know how annoying and upsetting it can be to receive unwanted email or instant messages from people you do not know. Therefore, you may not use the Services to harvest information about users for the purpose of sending, or to facilitate or encourage the sending of, unsolicited bulk or other communications. You understand that we may employ technical measures to prevent spam or unsolicited bulk or other communications from entering, utilizing, or remaining within our computer or communications networks. If you Post (as defined below in Section 7) or otherwise send spam, advertising, or other unsolicited communications of any kind through the Services, you acknowledge that you will have caused substantial harm to KukuoTv and Blue Canvas Technologies Limited and that the amount of such harm would be extremely difficult to measure. As a reasonable estimation of such harm, you agree to pay Blue Canvas Technologies Limited $50 for each such unsolicited communication you send through the Services.

#### **5. Billing**

Currently KukuoTv is an ad supported service that provides free content to users. Until changes in the service provided are made, neither KukuoTv or Blue Canvas Technologies Limited shall ask you to pay for any video content served, neither shall we request for access to your credit/debit card information via the service or emails.

#### **6. Account Access**

* The member who signed up for the KukuoTv account is referred to here as the Account Owner. The Account Owner has access and control over the KukuoTv account. The Account Owner's control is exercised through use of the Account Owner's password and therefore to maintain exclusive control, the Account Owner should not reveal the password to anyone. You are responsible for updating and maintaining the truth and accuracy of the information you provide to us relating to your account.

* In order to provide you with ease of access to your account and to help administer the KukuoTv service, Blue Canvas Technologies Limited implements technology that enables us to recognize you as the Account Owner and provide you with direct access to your account without requiring you to retype any password or other user identification when you revisit the KukuoTv service website.

* You should be mindful of any communication requesting that you submit credit card or other account information. Providing your information in response to these types of communications can result in identity theft. Always access your sensitive account information by going directly to the KukuoTv website and not through a hyperlink in an email or any other electronic communication, even if it looks official. Blue Canvas Technologies Limited reserves the right to place any account on hold anytime with or without notification to the member in order to protect itself and its partners from what it believes to be fraudulent activity.

#### **7. User Reviews, Comments, and Other Material**

* ##### **Your Posts:**
As part of the Services, users may have an opportunity to publish, transmit, submit, or otherwise post (collectively, "Post") reviews, comments, or other materials (collectively, "User Material"). In order to keep the Services enjoyable for all of our users, you must adhere to the rules below.
  1. Please choose carefully the User Material that you Post. Please limit yourself to User Material directly relevant to the Services. Moreover, you must not Post User Material that: 
    1. contains Unsuitable Material (as defined above in Section 3); or 
    2. Improperly claims the identity of another person. Please note that if you Post User Material on KukuoTv using a third party service, such as a social network or email provider, your first and last name or other user ID may appear to the public each time you Post. We advise that you do not, and you should also be careful if you decide to, Post additional personal information, such as your email address, telephone number, or street address.

  2. You must be, or have first obtained permission from, the rightful owner of any User Material you Post. By submitting User Material, you represent and warrant that you own the User Material or otherwise have the right to grant KukuoTv the license provided below. You also represent and warrant that the Posting of your User Material does not violate any right of any party, including privacy rights, publicity rights, and intellectual property rights. In addition, you agree to pay for all royalties, fees, and other payments owed to any party by reason of your Posting User Material. KukuoTv will remove all User Material if we are properly notified that such User Material infringes on another person's rights. You acknowledge that KukuoTv does not guarantee any confidentiality with respect to any User Material.

  3. By Posting User Material, you are not forfeiting any ownership rights in such material to KukuoTv. After posting your User Material, you continue to retain all of the same ownership rights you had prior to Posting. By Posting your User Material, you grant KukuoTv a limited license to use, display, reproduce, distribute, modify, delete from, add to, prepare derivative works of, publicly perform, and publish such User Material through the Services worldwide, including on or through any Property, in perpetuity, in any media formats and any media channels now known or hereinafter created. The license you grant to KukuoTv is non-exclusive (meaning you are not prohibited by us from licensing your User Material to anyone else in addition to KukuoTv), fully-paid, royalty-free (meaning that KukuoTv is not required to pay you for the use of your User Material), and sub licensable (so that KukuoTv is able to use its affiliates, subcontractors, and other partners, such as internet content delivery networks, to provide the Services). By posting your User Material, you also hereby grant each user of the Services a non-exclusive, limited license to access your User Material, and to use, display, reproduce, distribute, and perform such User Material as permitted through the functionality of the Services and under these Terms.

* ##### **Third Party Posts:**
Despite these restrictions, please be aware that some material provided by users may be objectionable, unlawful, inaccurate, or inappropriate. KukuoTv does not endorse any User Material, and User Material that is posted does not reflect the opinions or policies of KukuoTv or Blue Canvas Technologies Limited. We reserve the right, but have no obligation, to monitor User Material and to restrict or remove User Material that we determine, in our sole discretion, is inappropriate or for any other business reason. In no event does KukuoTv or Blue Canvas Technologies Limited assume any responsibility or liability whatsoever for any User Material, and you agree to waive any legal or equitable rights or remedies you may have against KukuoTv or Blue Canvas Technologies Limited with respect to such User Material. You can help us tremendously by notifying us of any inappropriate User Material you find by emailing **[contact@kukuotv.com](mailto:contact@kukuotv.com)** (subject line: "**Inappropriate User Material**").

#### **8. Linked Destinations and Advertising**

* ##### **Third Party Destinations:**
  1.If we provide links or pointers to other websites or destinations, you should not infer or assume that KukuoTv or Blue Canvas Technologies Limited operates, controls, or is otherwise connected with these other websites or destinations. When you click on a link within the Services, we will not warn you that you have left the Services and are subject to the terms and conditions (including privacy policies) of another website or destination. In some cases, it may be less obvious than others that you have left the Services and reached another website or destination. Please be careful to read the terms of use and privacy policy of any other website or destination before you provide any confidential information or engage in any transactions. You should not rely on these Terms to govern your use of another website or destination.
  2. KukuoTv or Blue Canvas Technologies Limited is not responsible for the content or practices of any website or destination other than the KukuoTv Site, even if it links to the KukuoTv Site and even if the website or destination is operated by a company affiliated or otherwise connected with KukuoTv or Blue Canvas Technologies Limited. By using the Services, you acknowledge and agree that KukuoTv or Blue Canvas Technologies Limited is not responsible or liable to you for any content or other materials hosted and served from any website or destination other than the KukuoTv Site.
* ##### **Advertisements:**
KukuoTv or Blue Canvas Technologies Limited takes no responsibility for advertisements or any third party material Posted on any Access Point where the Services are available, nor does it take any responsibility for the products or services provided by advertisers. Any dealings you have with advertisers while using the Services are between you and the advertiser, and you agree that KukuoTv or Blue Canvas Technologies Limited is not liable for any loss or claim that you may have against an advertiser.

#### **9. Intellectual Property**

* ##### **Copyright:**
The KukuoTv service, including all content provided on the KukuoTv service, is protected by copyright, trade secret or other intellectual property laws and treaties.
* ##### **Trademarks:**
KukuoTv is a registered trademark of Blue Canvas Technologies Limited.
* ##### **Claims of Copyright Infringement:**
Claims of Copyright Infringement. If you believe your work has been reproduced or distributed in a way that constitutes copyright infringement or are aware of any infringing material available through the KukuoTv service, please notify us on **[contact@kukuotv.com](mailto:contact@kukuotv.com)** and the Copyright Infringement Claims form will be sent to you via email.

#### **10. Use of Information Submitted.**

KukuoTv or Blue Canvas Technologies Limited is free to use any comments, information, ideas, concepts, reviews, or techniques or any other material contained in any communication you may send to us ("Feedback"), including responses to questionnaires or through postings to the KukuoTv service, including the KukuoTv website and user interfaces, worldwide and in perpetuity without further compensation, acknowledgement or payment to you for any purpose whatsoever including, but not limited to, developing, manufacturing and marketing products and creating, modifying or improving the KukuoTv service. In addition, you agree not to enforce any "moral rights" in and to the Feedback, to the extent permitted by applicable law. Please note It is Blue Canvas Technologies Limited's policy not to accept unsolicited submissions, including scripts, story lines, articles, fan fiction, characters, drawings, information, suggestions, ideas, or concepts., and is not responsible for the similarity of any of its content or programming in any media to materials or ideas transmitted to KukuoTv. Should you send any unsolicited materials or ideas, you do so with the understanding that no additional consideration of any sort will be provided to you, and you are waiving any claim against KukuoTv or Blue Canvas Technologies Limited and its affiliates regarding the use of such materials and ideas, even if material or an idea is used that is substantially similar to the material or idea you sent.

#### **11. Disclaimers of Warranties and Limitations on Liability**

* THE KUKUOTV SERVICE AND ALL CONTENT AND SOFTWARE ASSOCIATED THEREWITH, OR ANY OTHER FEATURES OR FUNCTIONALITIES ASSOCIATED WITH THE KUKUOTV, ARE PROVIDED "AS IS" AND "AS AVAILABLE" WITH ALL FAULTS AND WITHOUT WARRANTY OF ANY KIND. KUKUOTV DOES NOT GUARANTEE, REPRESENT, OR WARRANT THAT YOUR USE OF THE KUKUOTV SERVICE WILL BE UNINTERRUPTED OR ERROR-FREE. KUKUOTV SPECIFICALLY DISCLAIMS LIABILITY FOR THE USE OF APPLICATIONS, WEBSITE AND ANY OTHER KUKUOTV SOFTWARE (INCLUDING THEIR CONTINUING COMPATIBILITY WITH OUR SERVICE).

* TO THE EXTENT PERMISSIBLE UNDER APPLICABLE LAWS, IN NO EVENT SHALL BLUE CANVAS TECHNOLOGIES LIMITED, OR ITS SUBSIDIARIES OR ANY OF THEIR SHAREHOLDERS, DIRECTORS, OFFICERS, EMPLOYEES OR LICENSORS BE LIABLE (JOINTLY OR SEVERALLY) TO YOU FOR PERSONAL INJURY OR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER.

* SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR CERTAIN TYPES OF DAMAGES. THEREFORE, SOME OF THE ABOVE LIMITATIONS IN THIS SECTION MAY NOT APPLY TO YOU.

* NOTHING IN THESE TERMS OF USE SHALL AFFECT ANY NON-WAIVABLE STATUTORY RIGHTS THAT APPLY TO YOU. 

If any provision or provisions of these Terms of Use shall be held to be invalid, illegal, or unenforceable, the validity, legality and enforceability of the remaining provisions shall remain in full force and effect.

#### **12.	Notice and Procedure for Claims of Copyright Infringement**

* If you believe that any Content, User Material, or other material provided through the Services, including through a link, infringes your copyright, you should notify KukuoTv or Blue Canvas Technologies Limited of your infringement claim in accordance with the procedure set forth below.
We will process each notice of alleged infringement that KukuoTv or Blue Canvas Technologies Limited receives and take appropriate action in accordance with applicable intellectual property laws. A notification of claimed copyright infringement should be emailed to KukuoTv's Content Manager at **[contact@kukuotv.com](mailto:contact@kukuotv.com)** (subject line: "**DMCA Takedown Request**"). You may also contact us by mail at:

    Attention: Content Manager  
    KukuoTv, Blue Canvas Technologies Limited  
    BOX SE 398,  
    SUAME-KUMASI,  
    GHANA.

  To be effective, the notification must be in writing and contain the following information: 
  1. An electronic or physical signature of the person authorized to act on behalf of the owner of an exclusive copyright interest; 
  2. a description of the copyrighted work that you claim has been infringed; 
  3. a description of where the material that you claim is infringing is located on the Services that is reasonably sufficient to enable KukuoTv or Blue Canvas Technologies Limited to identify and locate the material; 
  4. how KukuoTv or Blue Canvas Technologies Limited can contact you, such as your address, telephone number, and email address; 
  5. a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and 
  6. A statement by you that the above information in your notice is accurate and under penalty of perjury that you are authorized to act on behalf of the copyright owner or the owner of an exclusive right in the material.

  KukuoTv or Blue Canvas Technologies Limited has a policy of terminating repeat infringers in appropriate circumstances.

#### **13.	Arbitration Agreement**

* Should you decide to seek legal action against KukuoTv or Blue Canvas Technologies Limited, we ask that you proceed with arbitration through our legal counsel.

* If you elect to seek arbitration or file a small claim court action, you must first send to KukuoTv or Blue Canvas Technologies Limited, by certified mail, a written Notice of your claim ("Notice").The Notice to KukuoTv or Blue Canvas Technologies must be addressed to:

Legal Counsel,  
KukuoTv or Blue Canvas Technologies Limited  
BOX SE 398,  
SUAME-KUMASI,  
GHANA.

* If KukuoTv or Blue Canvas Technologies Limited initiates arbitration, it will send a written Notice to the email address used for your membership account. A Notice, whether sent by you or by KukuoTv or Blue Canvas Technologies Limited, must: 
  1. describe the nature and basis of the claim or dispute; and 
  2. Set forth the specific relief sought ("Demand").

If KukuoTv or Blue Canvas Technologies Limited and you do not reach an agreement to resolve the claim within 30 days after the Notice is received, you or KukuoTv or Blue Canvas Technologies Limited may commence an arbitration proceeding or file a claim in court.

### **14. Survival.**

If any provision or provisions of these Terms of Use shall be held to be invalid, illegal, or unenforceable, the validity, legality and enforceability of the remaining provisions shall remain in full force and effect.

Thank you for taking the time to read these Terms. By understanding and agreeing to follow these Terms, the experience will be better for all users. It is our goal to provide you with a first class user experience, so if you have any questions or comments about these Terms, please contact us at: **[contact@kukuotv.com](mailto:contact@kukuotv.com)**. 

Enjoy KukuoTv!












