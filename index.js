var app = require('./core/server'),
    exec = require('child_process').exec,
    Admin = require('./core/server/models/Admin'),
    uuid = require('./core/server/utils/uuid'),
    moment = require('moment');

app.use('*', require('./core/server/routes/404'));

// Video.scan().exec(function(err, data){

//     var items = _.compact(_.pluck(_.pluck(data.Items, 'attrs'), 's3FileName'));

//     _.forEach(items, function(item){

//         var extname = Path.extname(item),
//             s3File = item.replace(extname, '') + '/';

//         console.log(s3File, item);

//         Upload.create('restore', {
//             key: item,
//             name: item,
//             prefix: s3File
//         }).priority('high').attempts(5).save();

//     });

// });

require('./core/server/connections/dynamo').createTables({
    'Admin':{
        readCapacity: 1,
        writeCapacity: 1
    },
    'Video': {
        readCapacity: 10,
        writeCapacity: 10
    },
    'User': {
        readCapacity: 5,
        writeCapacity: 5
    },
    'UserLikes':{
        readCapacity: 5,
        writeCapacity: 3
    }
}, function(err){
    if (err) return logger.error(err);

    Admin.query(config.get('master_email'))
        .limit(1)
        .usingIndex('EmailIndex')
        .exec(function(err, data){
            if (err) return logger.error(err);

            if (data.Items.length) {
                logger.log('Admin user exists');
            } else {

                Admin.create({
                    uid: uuid(12),
                    email: config.get('master_email'),
                    master: true,
                    savedIn: moment().format('[/]YYYY[/]MM'),
                    idt: Date.now()
                }, function(err, admin){
                    if (err) return logger.error(err);

                    if (admin) logger.log('Admin created!');
                })
            }
        });

});



app.listen(config.get('port'), function(){

    logger.info('listening on ' + config.get('port'));

});
