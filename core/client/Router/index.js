var Router = function(opts){

	var _this = this;

	_this._doc = opts.doc;
	_this._win = opts.win;

	_this._main = _this._doc.querySelector('.site-main');

	_this.init();

	return _this;
	
};

Router.prototype = {
	init : require('./init'),
	_clean: require('./clean'),
	_newScreen: require('./newScreen'),
	_render: require('./render'),
	_loadSprite: function(d, p){
		var a = new XMLHttpRequest(),
	        b = d.body;
	    a.open("GET", p, true);
	    a.send();
	    a.onload = function(){
	        var c = d.createElement("div");
	        c.style.display = "none";
	        c.innerHTML = a.responseText;
	        b.insertBefore(c, b.childNodes[0]);
	    };
	}
};

module.exports = Router;