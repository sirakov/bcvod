var forEach = require('fast.js/array/forEach');

var Base = function(router, ctx, head, state, next){

	var _this = this;

	_this.router = router;

	_this.router._render();

	_this.next = next;

	_this.ctx = ctx;

	_this.head = _this.router._head;

	_this.state = state;

	_this.children = [];

	_this._EventListeners = [];

	return _this.render(function(path){

		var propId = _this.state.get().toJS().googleTrackingId;

		ga('create', propId, 'auto');
		ga('send', 'pageview', path);

	});
	
};

Base.prototype.render = function(){};

Base.prototype.commitEvents = function(){

	var _this = this;

	forEach(_this.children, function(child){

		_this._EventListeners = _this._EventListeners.concat(child._EventListeners);

	});

};

Base.prototype._destroy = function(){

	var _this = this;

	forEach(_this._EventListeners, function(listener){

		return listener.destroy();
	});

};

module.exports = Base;