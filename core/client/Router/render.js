var Render = function(){

	var _this = this,
		_prerender = _this._main.querySelector('#prerender');

	if (_prerender) _this._prerender = true;
	else delete _this._prerender;
	
};

module.exports = Render;