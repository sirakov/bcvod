var NewScreen = function(){

	var _this = this,
		_prerender = _this._main.querySelector('#prerender'),
		_content;

	if (_prerender) {
		
		_screen = _prerender;
		_content = _screen.querySelector('.screenContent');


	} else {

		var _screen = _this._doc.createElement('div');
		
		_content = _this._doc.createElement('div');

		_content.className = 'screenContent bc-vod-position-relative';

		_screen.appendChild(_content);

	}

	_screen.classList.add('screen');

	_screen.id = 'screen_' + Date.now();

	_screen.classList.add('screen-in');

	if (_this._current) {

		_this._prev = _this._current;

		_this._current = _screen;

		_this._prev.style.visibility = 'hidden';

		_this._prev.style.display = 'none';

	} else {

		_this._current = _screen;

	}

	if (!_this._prerender) _this._main.appendChild(_screen);

	if (_this._currentState && _this._currentState._destroy) _this._currentState._destroy();

	_this._content = _content;

	return _screen;
	
};

module.exports = NewScreen;