var $ = require('sprint-js');

var Clean = function(path){
	
	var _this = this,
		prev = _this._prev;

	_this._currentRoute = path;

	if (document.body.scrollTop) document.body.scrollTop = 0;
	else if (document.documentElement.scrollTop) document.documentElement.scrollTop = 0;

	setTimeout(function(){

		_this._current.classList.remove('screen-in');

		setTimeout(function(){
			
			if (prev) {
				
				$(prev).remove();

				delete _this._prev;

			}

		}, 1500);

	}, 350);

};

module.exports = Clean;