var page = require('page.js'),
	Head = require('../components/head').client,
	contains = require('mout/array/contains'),
	Upload = require('./routes/upload'),
	Video = require('./routes/video'),
	Videos = require('./routes/videos'),
	Login = require('./routes/login'),
	userLogin = require('./routes/userLogin'),
	Public = require('./routes/public'),
	Home = require('./routes/homepage'),
	Single = require('./routes/single'),
	Search = require('./routes/search'),
	Category = require('./routes/category'),
	Profile = require('./routes/profile'),
	UserHistory = require('./routes/history'),
	Contact = require('./routes/contact'),
	NotFound = require('./routes/404'),
	Policy = require('./routes/policy'),
	Terms = require('./routes/terms'),
	Freezer = require('freezer-js');

var InitRouter = function(){
	var _this = this;

	_this._head = Head(_this._win.GLOBALS);

	_this.state = new Freezer(_this._win.GLOBALS);

	page('*', function(ctx, next){

		_this.state.get().set('path', ctx.path);

		if (_this._currentRoute && _this._currentRoute === ctx.pathname) return;
		else {

			return next();
		}
	});

	page('/admin/*', function(ctx, next){
		
		if (!contains(['/admin/login'], ctx.pathname)) {

			if (!_this.state.get()._currentUser || (_this.state.get()._currentUser && _this.state.get()._currentUser.frontend)) {

				_this._newScreen();

				return page.redirect('/admin/login?r=' + encodeURIComponent(ctx.pathname));

			} else {

				_this.state.get().set({
					admin: true
				});

				return next();

			}

		} else {

			return next();

		}

	});

	page('/admin', function(ctx, next){
		
		if (!contains(['/admin/login'], ctx.pathname)) {

			if (!_this.state.get()._currentUser) {

				_this._newScreen();

				return page.redirect('/admin/login?r=' + encodeURIComponent(ctx.pathname));

			} else {

				_this.state.get().set({
					admin: true
				});

				return next();

			}

		} else {

			return next();

		}

	});

	page('/admin/login', function(ctx, next){

		return new Login(_this, ctx, _this._head, _this.state, next);

	});

	page('/admin/upload', function(ctx, next){

		return new Upload(_this, ctx, _this._head, _this.state, next);

	});

	page.exit('/admin/upload', function(ctx, next){

		_this._currentState.children[1].socket.destroy();

		return next();

	});


	page('/admin', function(ctx, next){

		return new Videos(_this, ctx, _this._head, _this.state, next);

	});

	page('/admin/public', function(ctx, next){

		return new Public(_this, ctx, _this._head, _this.state, next);

	});

	page('/admin/v/:id/edit', function(ctx, next){

		return new Video(_this, ctx, _this._head, _this.state, next);

	});

	page('/', function(ctx, next){

		return new Home(_this, ctx, _this._head, _this.state, next);

	});

	page('/login', function(ctx, next){

		return new userLogin(_this, ctx, _this._head, _this.state, next);

	});

	page('/contact-us', function(ctx, next){

		return new Contact(_this, ctx, _this._head, _this.state, next);

	});

	page('/policy', function(ctx, next){

		return new Policy(_this, ctx, _this._head, _this.state, next);

	});

	page('/terms', function(ctx, next){

		return new Terms(_this, ctx, _this._head, _this.state, next);

	});

	page('/search', function(ctx, next){

		return new Search(_this, ctx, _this._head, _this.state, next);

	});

	page('/me/profile', function(ctx, next){

		return new Profile(_this, ctx, _this._head, _this.state, next);

	});

	page('/me/history', function(ctx, next){

		return new UserHistory(_this, ctx, _this._head, _this.state, next);

	});

	page('/v/:id/:episode?', function(ctx, next){

		return new Single(_this, ctx, _this._head, _this.state, next);

	});

	page.exit('/v/:id/:episode?', function(ctx, next){

		if (_this._currentState.children[1]._player) {

			_this._currentState.children[1]._player.dispose();

		}

		return next();

	});

	page('/category/:category', function(ctx, next){

		return new Category(_this, ctx, _this._head, _this.state, next);

	});

	page('*', function(ctx, next){
		
		return new NotFound(_this, ctx, _this._head, _this.state, next);

	});

	page();
    
};

module.exports = InitRouter;