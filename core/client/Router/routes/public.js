var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	VideosModule = require('../../components/videos'),
	http = require('../../components/http');

function Public(){
	Base.apply(this, arguments);
}

inherits(Public, Base);

Public.prototype.render = function(cb){

	var _this = this;


	// console.log(_this.ctx);

	if (_this.router._prerender) {

		var vids = _this.state.get().videos.toJS();

		_this.head.loop.update({
			title: 'Videos',
			path: _this.ctx.pathname
		});

		_this.router._newScreen();

		_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

		_this.children.push(new VideosModule({
			videos: vids,
			assetsUrl: _this.state.get().assetsUrl,
			last: _this.state.get().last,
			counts: _this.state.get().counts,
			public: _this.state.get().public
		}, _this.router._prerender, _this.router._content));

		_this.router._currentState = _this;

		_this.router._clean(_this.ctx.pathname);

		_this.commitEvents();

		_this.ctx.state.isOld = true;

		_this.ctx.save();

		return cb(_this.ctx.pathname);

	} else {

		_this.ctx.handled = false;

		return http({
			url: '/admin/public',
			method: 'get'
		}, function(err, res){

			if (err) return;

			var vids = res.data;

			vids.assetsUrl = _this.state.get().assetsUrl;

			// _this.ctx.state.data = res.data;
			_this.ctx.handled = true;

			_this.ctx.title = title('Videos', _this.state.get().brandName);

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			_this.head.loop.update({
				title: 'Videos',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

			_this.children.push(new VideosModule(vids, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});


	}

};

module.exports = Public;