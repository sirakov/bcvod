var inherits = require('inherits'),
	Base = require('../base'),
	title = require('../../components/head/title'),
	LoginModule = require('../../components/login');

function Login(){
	Base.apply(this, arguments);
}

inherits(Login, Base);

Login.prototype.render = function(cb){

	var _this = this;

	_this.ctx.title = title('Login', _this.state.get().brandName);

	_this.head.loop.update({
		title: 'Login',
		path: _this.ctx.pathname
	});

	_this.router._newScreen();

	_this.children.push(new LoginModule({
		disable: true,
		assetsUrl: _this.state.get().assetsUrl,
		_a: _this.state.get()._a.toJS(),
		build: _this.state.get().build
	}, _this.router._prerender, _this.router._content, _this.ctx.params));

	_this.router._currentState = _this;

	_this.router._clean(_this.ctx.pathname);

	_this.commitEvents();

	return cb(_this.ctx.pathname);

};

module.exports = Login;