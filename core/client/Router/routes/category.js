var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	Footer = require('../../components/footer'),
	CategoryModule = require('../../components/category'),
	capitalize = require('../../components/header/capitalize'),
	http = require('../../components/http');

function Category(){
	Base.apply(this, arguments);
}

inherits(Category, Base);

Category.prototype.render = function(cb){

	var _this = this;

	if (_this.router._prerender) {

		var dom = _this.state.get().toJS();

		dom.name = capitalize(_this.ctx.params.category);

		// if (!dom.videos.length) return _this.next();

		_this.head.loop.update({
			title: capitalize(_this.ctx.params.category),
			path: _this.ctx.pathname
		});

		_this.router._newScreen();

		_this.children.push(new Header(dom, _this.router._prerender, _this.router._content));

		_this.children.push(new CategoryModule(dom, _this.router._prerender, _this.router._content));

		_this.children.push(new Footer(dom, _this.router._prerender, _this.router._content));

		_this.router._currentState = _this;

		_this.router._clean(_this.ctx.pathname);

		_this.commitEvents();

		_this.ctx.state.isOld = true;

		_this.ctx.save();

		return cb(_this.ctx.pathname);

	} else {

		_this.ctx.handled = false;

		return http({
			url: _this.ctx.path,
			method: 'get'
		}, function(err, res){

			if (err) return;

			var dom = _this.state.get().toJS();

			dom.name = capitalize(_this.ctx.params.category);
			dom.videos = res.data.videos;
			dom.last = res.data.last;

			dom.filters = res.data.filters;

			_this.ctx.title = title(capitalize(_this.ctx.params.category), dom.brandName);

			_this.ctx.handled = true;

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			// if (!dom.videos.length) return _this.next();

			_this.head.loop.update({
				title: capitalize(_this.ctx.params.category),
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(dom, _this.router._prerender, _this.router._content));

			_this.children.push(new CategoryModule(dom, _this.router._prerender, _this.router._content));

			_this.children.push(new Footer(dom, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});

	}

};

module.exports = Category;