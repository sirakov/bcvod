var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	http = require('../../components/http'),
	title = require('../../components/head/title'),
	Footer = require('../../components/footer'),
	VideoModule = require('../../components/single'),
	loadingBar = require('../../components/loading'),
	getImage = require('../../components/single/getImage'),
	supportsVideo = require('../../supports_video'),
	initPlayer = require('../../components/single/initPlayer'),
	queryString = require('mout/queryString/decode');

	// require('../../vast/es5-shim');
	// require('../../vast/ie8fix');
	// require('../../vast/swfobject');
	// require('../../vast/videojs_5.vast.vpaid.min');


function Video(){
	Base.apply(this, arguments);
}

function getIEVersion(){
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

inherits(Video, Base);

Video.prototype.render = function(cb){

	var _this = this;

	if (_this.router._prerender) {

		if (_this.state.get().video) {

			var	vid = {
					video: _this.state.get().video.toJS(),
					assetsUrl: _this.state.get().assetsUrl,
					thumbnailsUrl: _this.state.get().thumbnailsUrl,
					imgsUrl: _this.state.get().imgsUrl,
					baseUrl: _this.state.get().baseUrl,
					embed: queryString('?' + _this.ctx.querystring).embed,
					brandName: _this.state.get().brandName,
					streamUrl: _this.state.get().streamUrl,
					defaultPoster: _this.state.get().defaultPoster.toJS(),
					env: _this.state.get().env,
					_a: _this.state.get()._a.toJS(),
					playlist: [],
					build: _this.state.get().build
				},
				head = _this.state.get().toJS();


			vid._currentUser = head._currentUser;

			head.backgroundMix = true;

			_this.head.loop.update({
				title: (vid.video.title) ? vid.video.title: '',
				description: (vid.video.description) ? vid.video.description: '',
				image: getImage(vid.video.poster),
				path: _this.ctx.pathname,
				video: true
			});

			_this.router._newScreen();

			if (!vid.embed) _this.children.push(new Header(head, _this.router._prerender, _this.router._content));

			_this.children.push(new VideoModule(vid, _this.router._prerender, _this.router._content));

			if (!vid.embed) _this.children.push(new Footer(head, _this.router._prerender, _this.router._content));

			if (vid.embed) {

				initPlayer(vid, _this.children[0]);

			} else {

				initPlayer(vid, _this.children[1]);

			}

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			_this.ctx.state.isOld = true;

			_this.ctx.save();

			return cb(_this.ctx.pathname);

		} else {

			return _this.next();

		}

	} else {

		_this.ctx.handled = false;

		return http({
			url: _this.ctx.path,
			method: 'get'
		}, function(err, res){

			if (err) {

				if (err.status === 404) {

					_this.ctx.handled = true;

					if (_this.ctx.state.isOld) {

						_this.ctx.save();

					} else {

						_this.ctx.state.isOld = true;

						_this.ctx.pushState();

					}

					if (loadingBar.isLoading()) loadingBar.stop();

					return _this.next();

				}

				return;
			}

			var vid = {
					video: res.data.video,
					assetsUrl: _this.state.get().assetsUrl,
					streamUrl: _this.state.get().streamUrl,
					thumbnailsUrl: _this.state.get().thumbnailsUrl,
					embed: queryString('?' + _this.ctx.querystring).embed,
					imgsUrl: _this.state.get().imgsUrl,
					baseUrl: _this.state.get().baseUrl,
					brandName: _this.state.get().brandName,
					defaultPoster: _this.state.get().defaultPoster.toJS(),
					env: _this.state.get().env,
					_a: _this.state.get()._a.toJS(),
					playlist: [],
					build: _this.state.get().build
				},
				head = _this.state.get().toJS();

				vid._currentUser = head._currentUser;

			// _this.ctx.state.data = res.data;
			_this.ctx.handled = true;

			_this.ctx.title = title((vid.video.title) ? vid.video.title: '', head.brandName);

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			_this.head.loop.update({
				title: (vid.video.title) ? vid.video.title: '',
				description: (vid.video.description) ? vid.video.description: '',
				image: getImage(vid.video.poster),
				path: _this.ctx.pathname,
				video: true
			});

			head.backgroundMix = true;

			_this.router._newScreen();

			if (!vid.embed) _this.children.push(new Header(head, _this.router._prerender, _this.router._content));

			_this.children.push(new VideoModule(vid, _this.router._prerender, _this.router._content));

			if (!vid.embed) _this.children.push(new Footer(head, _this.router._prerender, _this.router._content));

			if (vid.embed) {

				initPlayer(vid, _this.children[0]);

			} else {

				initPlayer(vid, _this.children[1]);

			}
			
			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});


	}

};

module.exports = Video;