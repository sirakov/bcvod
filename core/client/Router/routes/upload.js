var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	UploadModule = require('../../components/upload');

function Upload(){
	Base.apply(this, arguments);
}

inherits(Upload, Base);

Upload.prototype.render = function(cb){

	var _this = this;

	_this.ctx.title = title('Upload', _this.state.get().brandName);

	_this.head.loop.update({
		title: 'Upload',
		path: _this.ctx.pathname
	});

	_this.router._newScreen();

	_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.children.push(new UploadModule(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.router._currentState = _this;

	_this.router._clean(_this.ctx.pathname);

	_this.commitEvents();

	return cb(_this.ctx.pathname);

};

module.exports = Upload;