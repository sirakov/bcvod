var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	Footer = require('../../components/footer'),
	NotFoundModule = require('../../components/errors');

function NotFound(){
	Base.apply(this, arguments);
}

inherits(NotFound, Base);

NotFound.prototype.render = function(cb){

	var _this = this;

	_this.ctx.title = title('', _this.state.get().brandName);

	_this.head.loop.update({
		title: '',
		path: _this.ctx.pathname
	});

	_this.router._newScreen();

	_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.children.push(new NotFoundModule('404', _this.router._prerender, _this.router._content));

	_this.children.push(new Footer(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.router._currentState = _this;

	_this.router._clean(_this.ctx.pathname);

	_this.commitEvents();

	return cb(_this.ctx.pathname);

};

module.exports = NotFound;