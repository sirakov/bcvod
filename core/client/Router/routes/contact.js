var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	Footer = require('../../components/footer'),
	ContactModule = require('../../components/contact');

function Contact(){
	Base.apply(this, arguments);
}

inherits(Contact, Base);

Contact.prototype.render = function(cb){

	var _this = this;

	_this.router._newScreen();

	_this.ctx.title = title('Contact us', _this.state.get().brandName);

	_this.head.loop.update({
		title: 'Contact us',
		path: _this.ctx.pathname
	});

	_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.children.push(new ContactModule(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.children.push(new Footer(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.router._currentState = _this;

	_this.router._clean(_this.ctx.pathname);

	_this.commitEvents();

	return cb(_this.ctx.pathname);

};

module.exports = Contact;