var inherits = require('inherits'),
	Base = require('../base'),
	title = require('../../components/head/title'),
	LoginModule = require('../../components/userLogin'),
	qs = require('mout/queryString/decode');

function UserLogin(){
	Base.apply(this, arguments);
}

inherits(UserLogin, Base);

UserLogin.prototype.render = function(cb){

	var _this = this;

	_this.ctx.title = title('Login', _this.state.get().brandName);

	_this.head.loop.update({
		title: 'Login',
		path: _this.ctx.pathname
	});

	_this.router._newScreen();

	var _dom = _this.state.get().toJS(),
		query = qs(_this.ctx.querystring);

	_this.children.push(new LoginModule({
		path: query.r,
		assetsUrl: _dom.assetsUrl,
		build: _dom.build,
		_a: _dom._a
	}, _this.router._prerender, _this.router._content));

	_this.router._currentState = _this;

	_this.router._clean(_this.ctx.pathname);

	_this.commitEvents();

	return cb(_this.ctx.pathname);

};

module.exports = UserLogin;