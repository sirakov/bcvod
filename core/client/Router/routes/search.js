var inherits = require('inherits'),
	Base = require('../base'),
	title = require('../../components/head/title'),
	Header = require('../../components/header'),
	SearchModule = require('../../components/search'),
	qs = require('mout/queryString/decode'),
	http = require('../../components/http');

function Search(){
	Base.apply(this, arguments);
}

inherits(Search, Base);

Search.prototype.render = function(cb){

	var _this = this;

	var _dom = _this.state.get().toJS(),
		query = qs(_this.ctx.querystring),
		filters = {};

	if (query.q) filters.q = query.q;

	if (_this.router._prerender) {

		_this.ctx.title = title('Search', _this.state.get().brandName);

		_this.head.loop.update({
			title: 'Search',
			path: _this.ctx.pathname
		});

		_this.router._newScreen();

		_this.children.push(new Header(_dom, _this.router._prerender, _this.router._content));

		_this.children.push(new SearchModule({
			brandName: _dom.brandName,
			assetsUrl: _dom.assetsUrl,
			imgsUrl: _dom.imgsUrl,
			defaultPoster: _dom.defaultPoster,
			filters: filters,
			results: _dom.results,
			last: _dom.last
		}, _this.router._prerender, _this.router._content));

		_this.router._currentState = _this;

		_this.router._clean(_this.ctx.pathname);

		_this.commitEvents();

		return cb(_this.ctx.pathname);

	} else {

		if (filters.q) {

			_this.ctx.handled = false;

			return http({
				url: _this.ctx.path,
				method: 'get'
			}, function(err, res){

				if (err) return;

				_this.ctx.handled = true;

				if (_this.ctx.state.isOld) {

					_this.ctx.save();

				} else {

					_this.ctx.state.isOld = true;

					_this.ctx.pushState();

				}

				_this.ctx.title = title('Search', _this.state.get().brandName);

				_this.head.loop.update({
					title: 'Search',
					path: _this.ctx.pathname
				});

				_this.router._newScreen();

				_this.children.push(new Header(_dom, _this.router._prerender, _this.router._content));

				_this.children.push(new SearchModule({
					brandName: _dom.brandName,
					assetsUrl: _dom.assetsUrl,
					imgsUrl: _dom.imgsUrl,
					defaultPoster: _dom.defaultPoster,
					filters: filters,
					results: res.data.results,
					last: res.data.last
				}, _this.router._prerender, _this.router._content));

				_this.router._currentState = _this;

				_this.router._clean(_this.ctx.pathname);

				_this.commitEvents();

				return cb(_this.ctx.pathname);


			});



		} else {

			_this.ctx.title = title('Search', _this.state.get().brandName);

			_this.head.loop.update({
				title: 'Search',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(_dom, _this.router._prerender, _this.router._content));

			_this.children.push(new SearchModule({
				brandName: _dom.brandName,
				assetsUrl: _dom.assetsUrl,
				defaultPoster: _dom.defaultPoster,
				filters: filters,
				results: []
			}, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		}

	}

};

module.exports = Search;