var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	Footer = require('../../components/footer'),
	HistoryModule = require('../../components/history'),
	capitalize = require('../../components/header/capitalize'),
	http = require('../../components/http');

function UserHistory(){
	Base.apply(this, arguments);
}

inherits(UserHistory, Base);

UserHistory.prototype.render = function(cb){

	var _this = this;

	if (_this.router._prerender) {

		var dom = _this.state.get().toJS();

		_this.head.loop.update({
			title: 'History',
			path: _this.ctx.pathname
		});

		_this.router._newScreen();

		_this.children.push(new Header(dom, _this.router._prerender, _this.router._content));

		_this.children.push(new HistoryModule(dom, _this.router._prerender, _this.router._content));

		_this.children.push(new Footer(dom, _this.router._prerender, _this.router._content));

		_this.router._currentState = _this;

		_this.router._clean(_this.ctx.pathname);

		_this.commitEvents();

		_this.ctx.state.isOld = true;

		_this.ctx.save();

		return cb(_this.ctx.pathname);

	} else {

		_this.ctx.handled = false;

		return http({
			url: _this.ctx.path,
			method: 'get'
		}, function(err, res){

			if (err) return;

			var dom = _this.state.get().toJS();

			dom.videos = res.data.videos;
			dom.last = res.data.last;

			_this.ctx.title = title('History', dom.brandName);

			_this.ctx.handled = true;

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			// if (!dom.videos.length) return _this.next();

			_this.head.loop.update({
				title: 'History',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(dom, _this.router._prerender, _this.router._content));

			_this.children.push(new HistoryModule(dom, _this.router._prerender, _this.router._content));

			_this.children.push(new Footer(dom, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});

	}

};

module.exports = UserHistory;