var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	Footer = require('../../components/footer'),
	HomePageModule = require('../../components/homepage'),
	http = require('../../components/http');

function HomePage(){
	Base.apply(this, arguments);
}

inherits(HomePage, Base);

HomePage.prototype.render = function(cb){

	var _this = this;

	if (_this.router._prerender) {

		_this.head.loop.update({
			title: '',
			path: _this.ctx.pathname
		});

		_this.router._newScreen();

		var dom = _this.state.get().toJS();

		dom.backgroundMix = true;

		_this.children.push(new Header(dom, _this.router._prerender, _this.router._content));

		_this.children.push(new HomePageModule(dom, _this.router._prerender, _this.router._content));

		_this.children.push(new Footer(dom, _this.router._prerender, _this.router._content));

		_this.router._currentState = _this;

		_this.router._clean(_this.ctx.pathname);

		_this.commitEvents();

		_this.ctx.state.isOld = true;

		_this.ctx.save();

		return cb(_this.ctx.pathname);

	} else {

		_this.ctx.handled = false;

		return http({
			url: '/',
			method: 'get'
		}, function(err, res){

			if (err) return;

			var dom = _this.state.get().toJS();

			dom.backgroundMix = true;
			dom.videos = res.data.videos;
			dom.last = res.data.last;

			// dom.releases = res.data.releases;
			dom.popular = res.data.popular;
			dom.genres = res.data.genres;

			_this.ctx.handled = true;

			_this.ctx.title = title('', dom.brandName);

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			_this.head.loop.update({
				title: '',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(dom, _this.router._prerender, _this.router._content));

			_this.children.push(new HomePageModule(dom, _this.router._prerender, _this.router._content));

			_this.children.push(new Footer(dom, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});

	}

};

module.exports = HomePage;