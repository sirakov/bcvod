var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	VideoModule = require('../../components/video'),
	loadingBar = require('../../components/loading'),
	http = require('../../components/http');

function Video(){
	Base.apply(this, arguments);
}

inherits(Video, Base);

Video.prototype.render = function(cb){

	var _this = this;


	// console.log(_this.ctx);

	if (_this.router._prerender) {

		if (_this.state.get().video) {

			var vid = {
				video: _this.state.get().video.toJS(),
				categories: _this.state.get().categories.toJS(),
				assetsUrl: _this.state.get().assetsUrl
			};

			_this.head.loop.update({
				title: (vid.video.title) ? vid.video.title: '',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

			_this.children.push(new VideoModule(vid, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			_this.ctx.state.isOld = true;

			_this.ctx.save();

			return cb(_this.ctx.pathname);

		} else {

			return _this.next();

		}

	} else {

		_this.ctx.handled = false;

		return http({
			url: _this.ctx.path,
			method: 'get'
		}, function(err, res){

			if (err) {

				if (err.status === 404) {

					_this.ctx.handled = true;
					
					if (_this.ctx.state.isOld) {

						_this.ctx.save();

					} else {

						_this.ctx.state.isOld = true;

						_this.ctx.pushState();

					}

					if (loadingBar.isLoading()) loadingBar.stop();

					return _this.next();

				}

				return;
			}

			var vid = {
				video: res.data,
				categories: _this.state.get().categories.toJS(),
				assetsUrl: _this.state.get().assetsUrl
			};
			
			_this.ctx.handled = true;

			_this.ctx.title = title((vid.video.title) ? vid.video.title: '', _this.state.get().brandName);

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			_this.head.loop.update({
				title: (vid.video.title) ? vid.video.title: '',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

			_this.children.push(new VideoModule(vid, _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});


	}

};

module.exports = Video;