var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	TermsModule = require('../../components/terms'),
	Footer = require('../../components/footer'),
	http = require('../../components/http');

function Terms(){
	Base.apply(this, arguments);
}

inherits(Terms, Base);

Terms.prototype.render = function(cb){

	var _this = this;

	if (_this.router._prerender) {

		_this.ctx.title = title('Terms of use', _this.state.get().brandName);

		_this.head.loop.update({
			title: 'Terms of use',
			path: _this.ctx.pathname
		});

		_this.router._newScreen();

		_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

		_this.children.push(new TermsModule({
			state: ''
		}, _this.router._prerender, _this.router._content));

		_this.children.push(new Footer(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

		_this.router._currentState = _this;

		_this.router._clean(_this.ctx.pathname);

		_this.commitEvents();

		return cb(_this.ctx.pathname);

	} else {

		_this.ctx.handled = false;

		return http({
			url: '/terms',
			method: 'get'
		}, function(err, res){

			if (err) return;

			_this.ctx.title = title('Terms of use', _this.state.get().brandName);

			_this.ctx.handled = true;

			if (_this.ctx.state.isOld) {

				_this.ctx.save();

			} else {

				_this.ctx.state.isOld = true;

				_this.ctx.pushState();

			}

			_this.head.loop.update({
				title: 'Terms of use',
				path: _this.ctx.pathname
			});

			_this.router._newScreen();

			_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

			_this.children.push(new TermsModule({
				html: res.data.html
			}, _this.router._prerender, _this.router._content));

			_this.children.push(new Footer(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

			_this.router._currentState = _this;

			_this.router._clean(_this.ctx.pathname);

			_this.commitEvents();

			return cb(_this.ctx.pathname);

		});

	}

};

module.exports = Terms;