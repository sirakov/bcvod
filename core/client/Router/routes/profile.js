var inherits = require('inherits'),
	Base = require('../base'),
	Header = require('../../components/header'),
	title = require('../../components/head/title'),
	ProfileModule = require('../../components/profile'),
	Footer = require('../../components/footer');

function Profile(){
	Base.apply(this, arguments);
}

inherits(Profile, Base);

Profile.prototype.render = function(cb){

	var _this = this,
		user = _this.state.get()._currentUser.toJS();

	_this.ctx.title = title(user.fullname || 'Profile', _this.state.get().brandName);

	_this.head.loop.update({
		title: user.fullname || 'Profile',
		path: _this.ctx.pathname
	});

	_this.router._newScreen();

	_this.children.push(new Header(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.children.push(new ProfileModule(user, _this.router._prerender, _this.router._content));

	_this.children.push(new Footer(_this.state.get().toJS(), _this.router._prerender, _this.router._content));

	_this.router._currentState = _this;

	_this.router._clean(_this.ctx.pathname);

	_this.commitEvents();

	return cb(_this.ctx.pathname);

};

module.exports = Profile;