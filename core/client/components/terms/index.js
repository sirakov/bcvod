var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render');

var Terms = function(dom, prerender, content) {

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-terms-page');

		obj.target = target;

	}

	_this.loop = mainLoop(dom, Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	_this._EventListeners = [];
	
};

module.exports = Terms;