var _h = require('virtual-dom/h');

var Render = function(state){

	return _h('div.bc-vod-terms-page.bc-vod-container.bc-vod-container-center', [
		_h('div.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-4-5.bc-vod-width-large-4-5', [
			_h('h1.title.bold.bc-vod-margin-large-top.text-center', [String('TERMS OF USE')]),
			_h('div.section-divider', [
				_h('hr')
			]),
			_h('div.terms.bc-vod-margin-large-bottom', {
				innerHTML: state.html
			})
		])
	]);
	
};

module.exports = Render;