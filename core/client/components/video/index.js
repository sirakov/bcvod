var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js');

var Upload = function(dom, prerender, content) {

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-media-div');

		obj.target = target;

	}

	if (!dom.video.episodes) dom.video.episodes = [];

	_this._dom = new Freezer(dom);

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	_this.$ = require('sprint-js');

	_this._initEvents(document.body);
	
};

Upload.prototype = {
	_initEvents: require('./events')
};

module.exports = Upload;