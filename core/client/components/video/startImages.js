var Vibrant = require('node-vibrant');

var StartImages = function(file, index){

	if (/image/.test(file.type)) {

		var _this = this,
			img = new Image(),
			url = (URL || webkitURL).createObjectURL(file);

		img.onload = function(){

			var _that = this,
				video = _this._dom.get().video,
				poster;

			console.log(video);

			if (video.previewUrl) {

				(URL || webkitURL).revokeObjectURL(video.previewUrl);

			}

			Vibrant.from(url).getPalette(function(err, palette){

				if (_that.width >= 275 && _that.height >= 407) {

					_this._dom.get().video.set({
						posterUploaded: false,
						previewUrl: url,
						poster: JSON.stringify({
							width: _that.width,
							height: _that.height,
							color: (palette['Vibrant'] || palette['Muted']).getHex() || '#000'
						})
					});

					_this.poster = file;

				} else {

					_this._dom.get().video.set({
						poster: false,
						previewUrl: false,
						posterUploaded: false
					});

				}

				_this.loop.update(_this._dom.get().toJS());

			});

		};

		img.src = url;

		return;

	}

};

module.exports = StartImages;
