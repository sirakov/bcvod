var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	genresO = require('../category/genres'),
    yearsO = require('../category/years'),
    originsO = require('../category/origins'),
    musicGenres = require('../category/musicGenres'),
    musicYears = require('../category/musicYears'),
    musicOrigins = require('../category/musicOrigin'),
    languagesO = require('../category/languages');

var Render = function(obj){

	var Preview,
		re = new RegExp('music', 'g'),
		Mo = new RegExp('movies', 'g'),
		Se = new RegExp('series', 'g'),
		De = new RegExp('draft', 'g'),
		isDraft = De.test(obj.video.category || obj.video.state),
		isSeries = Se.test(obj.video.category),
		isMovie = Mo.test(obj.video.category);

	if (re.test(obj.video.category)) {

		var genres = musicGenres;
		var years = musicYears;
		var origins	= musicOrigins;

		var languages = [];

	} else {

		var genres = genresO;
		var years = yearsO;
		var origins	= originsO;

		var languages = languagesO;


	}

	if (obj.video.poster || obj.video.previewUrl) {

		if (obj.video.previewUrl) {

			src = obj.video.previewUrl;

		} else if (obj.video.poster) {

			var poster = JSON.parse(obj.video.poster);

			if (poster.src) src = obj.imgsUrl + '/' +  poster.src;

		}

		Preview = _h('div', [
			_h('img.preview-canvas.bc-vod-container-center',{
				style:{
					display: 'block',
					width: '275px'
				},
				src: src
			}),
			_h('input.poster-upload', {
				attributes:{
					type: 'file',
					accept: 'image/*'
				},
				style:{
					display: 'none'
				}
			})
		]);

	} else {

		var De = String('Drop file to upload'),
			Sp = _h('span', {
				style:{
					display: 'block'
				}
			}, [String('or click here')]);

		if (typeof obj.video.poster === 'boolean') {

			De = undefined;

			Sp = _h('span', {
				style:{
					display: 'block',
					color: 'red'
				}
			}, [String('Error: file must be at least 275px x 407px in dimensions')]);

		}

		Preview = _h('div.preview-canvas.bc-vod-placeholder.bc-vod-placeholder-large.bc-vod-text-center', [
			De,
			Sp,
			_h('input.poster-upload', {
				attributes:{
					type: 'file',
					accept: 'image/*'
				},
				style:{
					display: 'none'
				}
			})
		]);

	}

	var Status,
		PublishButton,
		UnPublishButton,
		DeleteButton;

	if (!isSeries) {

		Status = _h('div.fileName.bc-vod-margin-top.bc-vod-text-center', [String(obj.video.oFile)]);

	}

	if ((isSeries && isDraft) || (!isSeries && isDraft)) {

		PublishButton = _h('button.publish-video.btn-info.btn', {
			attributes:{
				'data-action': 'publish'
			}
		}, [String('Publish')]);

	}

	if (!isDraft) {

		UnPublishButton = _h('button.publish-video.btn-info.btn', {
			attributes:{
				'data-action': 'unpublish'
			}
		}, [String('Unpublish')]);

	}

	if ((isSeries && isDraft && !obj.video.episodes.length) || (!isSeries && isDraft)) {

		DeleteButton = _h('button.delete-video.btn-danger.btn', [String('Delete')]);

	}

	return _h('div.bc-vod-media-div.bc-vod-container-center.bc-vod-container.bc-vod-margin-large-bottom.bc-vod-main-div', [
			_h('div.bc-vod-grid.bc-vod-grid-large.bc-vod-margin-large-top', [
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-5.bc-vod-margin-top', [
					_h('div.preview-pane', [
						Preview
					]),
					Status
				]),
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-3-5.bc-vod-margin-top', [
					_h('form', {
						attributes:{
							role: 'form'
						}
					}, [
						_h('div.form-group', [
							_h('label', [String('VideoId')]),
							_h('input.form-control.input-lg', {
								attributes:{
									disabled: true,
									type: 'text'
								},
								value: obj.video.vid
							})
						]),
						_h('div.form-group', [
							_h('label', [String('Title')]),
							_h('input.form-control.input-lg', {
								attributes:{
									placeholder: 'Enter Title',
									type: 'text',
									id: 'title'
								},
								value: obj.video.title
							})
						]),
						_h('div.form-group', [
							_h('label', [String('Description')]),
							_h('textarea.form-control', {
								attributes:{
									placeholder: 'No description yet',
									rows: 5
								}
							}, [String((obj.video.description) ? obj.video.description : '')])
						]),
						_h('div.form-group', {
							style:{
								display: (isSeries) ? undefined : 'none'
							}
						}, [
							_h('label', [String('Episodes')]),
							_h('div', map(obj.video.episodes, function(episode){
								return _h('div.btn-group.bc-vod-margin-small-right.bc-vod-margin-small-bottom', [
									_h('div.btn-rounded.btn', [
										String(episode.episodeId)
									]),
									_h('button.btn-rounded.btn.btn-danger', {
										attributes:{
											'data-action': 'remove-episode',
											'data-episode-id': episode.episodeId
										}
									}, [String('x')])
								]);
							})),
							_h('input', {
								className: [
									'form-control',
									'bc-vod-margin-small-top'
								].join(' '),
								style:{
									display: (obj.isAddEpisode) ? 'block': 'none'
								},
								attributes:{
									type: 'number',
									name: 'episodeId',
									placeholder: 'episodeId'
								}
							}),
							_h('input', {
								className: [
									(obj.isAddEpisodeError) ? 'error': '',
									'form-control',
									'bc-vod-margin-small-top'
								].join(' '),
								style:{
									display: (obj.isAddEpisode) ? 'block': 'none'
								},
								attributes:{
									type: 'text',
									name: 'videoId',
									placeholder: 'videoId eg(2016-04-4e9bbd69fe6d)'
								}
							}),
							_h('a.btn-rounded.bc-vod-margin-small-top.btn.bc-vod-margin-small-right.bc-vod-margin-small-bottom', {
								attributes:{
									'data-action': (obj.isAddEpisode) ? 'add-episode' :'new-episode'
								}
							}, ['+ Add'])
						]),
						_h('div.form-group', {
							style:{
								display: (obj.video.status === 'live' && !isSeries) ? 'block': 'none'
							}
						}, [
							_h('label', [String('Category')]),
							_h('select.form-control', {
								attributes:{
									placeholder: 'Select Category'
								}
							}, map(obj.categories, function(category){
								var attrs = {},
									re = new RegExp(category.name, 'g');

								if (re.test(obj.video.category)) attrs.selected = 'selected';

								return _h('option', {
									attributes: attrs
								}, [String(category.name)])
							}))
						]),
						_h('div.form-group', {
							style:{
								display: (isMovie || isSeries) ? undefined : 'none'
							}
						}, [
							_h('label', [String('Sequel')]),
							_h('input.form-control', {
								attributes: {
									placeholder: 'Sequel videoId eg(2016-04-4e9bbd69fe6d)',
									type: 'text',
									id: 'sequel'
								},
								value: obj.video.sequel
							})
						]),
						_h('div.form-group', {
							style:{
								display: (obj.video.status === 'live' || isSeries) ? 'block': 'none'
							}
						}, [
							_h('label', [String('Genres')]),
							_h('div', map(genres, function(genre){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (typeof obj.video.genres === 'object' && (obj.video.genres.indexOf(genre) > -1)) {

									classList.push('btn-info');

								}

								if (typeof obj.video.genres === 'string' && obj.video.genres === genre) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'genres',
										'data-value': genre
									}
								}, [String(genre)]);
							}))
							
						]),
						_h('div.form-group', {
							style:{
								display: (obj.video.status === 'live' || isSeries) ? 'block': 'none'
							}
						}, [
							_h('label', [String('Origin')]),
							_h('div', map(origins, function(origin){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (obj.video.origin === origin) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'origin',
										'data-value': origin
									}
								}, [String(origin)]);
							}))
							
						]),
						_h('div.form-group', {
							style:{
								display: (obj.video.status === 'live' || isSeries) ? 'block': 'none'
							}
						}, [
							_h('label', [String('Production year')]),
							_h('div', map(years, function(year){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (obj.video.year === year) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'year',
										'data-value': year
									}
								}, [String(year)]);
							}))
							
						]),
						_h('div.form-group', {
							style:{
								display: (languages.length && obj.video.status === 'live' || isSeries) ? undefined: 'none'
							}
						}, [
							_h('label', [String('Language')]),
							_h('div', map(languages, function(language){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (obj.video.language === language) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'language',
										'data-value': language
									}
								}, [String(language)]);
							}))
							
						]),
						_h('div.bc-vod-form-row.bc-vod-clearfix', [
							_h('div.btn-group.bc-vod-margin-large-top', [
								_h('button.save-video.btn-primary.btn', [String('Save')]),
								PublishButton,
								UnPublishButton,
								DeleteButton
							])
						])
					])
				])
			])
		]);
	
};

module.exports = Render;