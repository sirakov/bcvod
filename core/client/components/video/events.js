var Delegator = require('dom-delegate'),
	http = require('../http'),
	Overlay = require('../overlay'),
	qs = require('mout/queryString/encode'),
	StartImages = require('./startImages'),
	findIndex = require('mout/array/findIndex'),
	page = require('page.js');

var Events = function(body){

	var _this = this,
		_BodyDelegator = new Delegator(body);

	_BodyDelegator.on('dragenter', '.site-main', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('dragover', '.site-main', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('dragstart', '.site-main', function(e){

		e.dataTransfer.effectAllowed = 'copyMove';
		e.dataTransfer.dropEffect = 'copy';

	});

	_BodyDelegator.on('dragleave', '.site-main', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('drop', '.site-main', function(e){

		e.preventDefault();

		StartImages.call(_this, e.dataTransfer.files[0]);

		return;

	});

	_BodyDelegator.on('dragend', '.site-main', function(e){

		// console.log(e);

		e.preventDefault();

	});

	_BodyDelegator.on('input', '.bc-vod-media-div form input#title', function(e){

		var vid = _this._dom.get().video.toJS();

		vid.title = e.target.value;

		_this.loop.update(_this._dom.get().set({
			video: vid
		}).toJS());

	});

	_BodyDelegator.on('input', '.bc-vod-media-div form input#sequel', function(e){

		var vid = _this._dom.get().video.toJS();

		vid.sequel = e.target.value;

		_this.loop.update(_this._dom.get().set({
			video: vid
		}).toJS());

	});

	_BodyDelegator.on('input', '.bc-vod-media-div form textarea', function(e){

		var vid = _this._dom.get().video.toJS();

		vid.description = e.target.value;

		_this.loop.update(_this._dom.get().set({
			video: vid
		}).toJS());

	});

	_BodyDelegator.on('input', '.bc-vod-media-div form select', function(e){

		var vid = _this._dom.get().video.toJS(),
			name = _this._dom.get().categories[e.target.selectedIndex].name,
			toUpdate;

		vid.category = '/' + name + vid.savedIn.replace('/draft', '');

		toUpdate = {
			video: vid
		};


		if (name === 'music') {

			toUpdate.video.genres = '';
			toUpdate.video.year = '';
			toUpdate.video.origin = '';

			delete toUpdate.video.language;

		} else {

			toUpdate.video.genres = [];

			toUpdate.video.year = '';
			toUpdate.video.origin = '';

		}

		_this.loop.update(_this._dom.get().set(toUpdate).toJS());

	});

	_BodyDelegator.on('click', 'a[data-action="new-episode"]', function(e){

		var dom = _this._dom.get().toJS();

		if (dom.isAddEpisode) _this._dom.get().remove('isAddEpisode');
		else _this._dom.get().set('isAddEpisode', true);

		return _this.loop.update(_this._dom.get().toJS());

	});

	_BodyDelegator.on('click', 'a[data-action="add-episode"]', function(e){

		var dom = _this._dom.get().toJS(),
			videoId = document.querySelector('input[name="videoId"]'),
			episodeId = document.querySelector('input[name="episodeId"]');

		if (!videoId.value || !episodeId.value) {

			_this._dom.get().set('isAddEpisodeError', true);
			_this.loop.update(_this._dom.get().toJS());

			return false;

		}

		if (dom.isAddEpisodeError) {

			_this._dom.get().remove('isAddEpisodeError');
			_this.loop.update(_this._dom.get().toJS());

		}

		http({
			method: 'post',
			url: '/admin/v/' + dom.video.vid + '/eps',
			data:{
				videoId: videoId.value,
				episodeId: episodeId.value
			}
		}, function(err, res){

			if (err) {

				_this._dom.get().set('isAddEpisodeError', true);

				_this.loop.update(_this._dom.get().toJS());

				return;
			}

			_this._dom.get().video.episodes.push({
				episodeId: parseInt(res.data.episodeId),
				vid: '/episode/' + res.data.episodeId
			});

			_this._dom.get().remove('isAddEpisode');

			videoId.value = '';
			episodeId.value = '';

			return _this.loop.update(_this._dom.get().toJS());


		});

	});

	_BodyDelegator.on('click', 'button[data-action="remove-episode"]', function(e){

		e.preventDefault();

		var dom = _this._dom.get().toJS(),
			data = e.target.dataset;

		http({
			method: 'put',
			url: '/admin/v/' + dom.video.vid + '/eps',
			data:{
				title: dom.video.title,
				episodeId: data.episodeId
			}
		}, function(err, res){

			if (err) return;

			var index = findIndex(dom.video.episodes, {
				episodeId: parseInt(data.episodeId)
			});

			if (index > -1) _this._dom.get().video.episodes.splice(index, 1);

			return _this.loop.update(_this._dom.get().toJS());


		});

	});

	_BodyDelegator.on('click', 'button[data-action="publish"]', function(e){

		e.preventDefault();

		var dom = _this._dom.get().toJS();

		http({
			method: 'post',
			url: '/admin/v/' + dom.video.vid + '/publish'
		}, function(err, res){

			if (err) return;

			_this._dom.get().video.set({
				state: res.data.video.state,
				status: res.data.video.status,
				savedIn: res.data.video.savedIn,
				category: res.data.video.category,
				live: true
			});

			var ctx = new page.Context('/admin/v/' + dom.video.vid + '/edit?public=true', {
				isOld: true
			});

			ctx.save();

			return _this.loop.update(_this._dom.get().toJS());


		});

		return false;

	});

	_BodyDelegator.on('click', 'button[data-action="unpublish"]', function(e){

		e.preventDefault();

		var dom = _this._dom.get().toJS();

		http({
			method: 'delete',
			url: '/admin/v/' + dom.video.vid + '/publish'
		}, function(err, res){

			if (err) return;

			_this._dom.get().video.set({
				state: res.data.video.state,
				status: res.data.video.status,
				savedIn: res.data.video.savedIn,
				category: res.data.video.category,
				live: true
			});

			var ctx = new page.Context('/admin/v/' + dom.video.vid + '/edit', {
				isOld: true
			});

			ctx.save();

			return _this.loop.update(_this._dom.get().toJS());


		});

		return false;

	});

	_BodyDelegator.on('click', '.bc-vod-media-div form button.save-video', function(e){

		e.preventDefault();

		var vid = _this._dom.get().video.toJS(),
			data,
			obj = {};

		if (vid.title) obj.title = vid.title;
		if (vid.description) obj.description = vid.description;
		if (vid.category) obj.category = vid.category;
		if (vid.sequel) obj.sequel = vid.sequel;

		if (vid.genres && typeof vid.genres === 'object') obj.genres = vid.genres;
		else if (vid.genres && typeof vid.genres === 'string') obj.genres = [vid.genres];

		if (vid.origin) obj.origin = vid.origin;
		if (vid.year) obj.year = vid.year;
		if (vid.language) obj.language = vid.language;

		if (vid.status === 'live') obj.live = true;

		if (vid.poster) {

			var poster = JSON.parse(vid.poster);

			obj.imgMeta = JSON.stringify(poster);

		}

		if (_this.poster) {

			data = require('../utils/formData')(_this.poster, obj);

		} else {

			data = obj;

		}

		return http({
			url: '/admin/v/' + vid.vid + '/edit',
			method: 'post',
			data: data
		}, function(err, res){

			if (err) return;



			if (vid.previewUrl) {

				window.URL.revokeObjectURL(vid.previewUrl);

			}

			if (vid.previewUrl) {

				_this._dom.get().video.set({
					poster: res.data.poster,
					posterUploaded: true,
					previewUrl: false
				});

			}

			delete _this.poster;

			_this.loop.update(_this._dom.get().toJS());

			// console.log(res);

		});

		return;

	});

	_BodyDelegator.on('click', 'a[data-action="filter"]', function(e){

		e.preventDefault();

		var el = (e.target.tagName.toLowerCase() === 'a') ? e.target: _this.$(e.target).parents('a[data-action="filter"]').dom[0],
			data = el.dataset,
			type = data.type,
			value = data.value,
			dom = _this._dom.get();

		if (type === 'genres' && typeof dom.video[type] === 'object') {

			if (contains(dom.video[type], value)) {

				var index = dom.video[type].indexOf(value);

				if (index > -1) {

					_this._dom.get().video[type].splice(index, 1);

				}

			} else {

				_this._dom.get().video[type].push(value);

			}

		} else {

			if (dom.video[type] === value) _this._dom.get().video.remove(type);
			else _this._dom.get().video.set(type, value);

		}

		_this.loop.update(_this._dom.get().toJS());

		return false;

	});

	_BodyDelegator.on('click', '.preview-pane', function(e){

		var inputFile,
			isButton = e.target.tagName.toLowerCase() === 'button',
			isIcon = e.target.tagName.toLowerCase() === 'i';

		if (e.target.classList.contains('preview-pane') && isButton && !isIcon) {

			inputFile = e.target.querySelector('input.poster-upload')

		} else if (!isButton && !isIcon) {

			var pane = _this.$(e.target).parents('.preview-pane').dom[0];

			inputFile = pane.querySelector('input.poster-upload');

		}

		if (inputFile) return inputFile.click();

		return;

	});

	_BodyDelegator.on('click', 'button.delete-video', function(e){

		e.preventDefault();

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button.delete-video').dom[0],
			dom = _this._dom.get(),
			vid = dom.video.vid,
			video = dom.video,
			live = (video.status === 'live') ? true: false;

		var _overlay = Overlay.add({
				type: 'delete'
			}, {
			delete: function(){

				_overlay._destroy();

				http({
					url: '/admin/v/'+ vid + '/edit?live=' + live,
					method: 'delete'
				}, function(err, res){

					if (err) return;

					if (live) page.redirect('/admin/public');
					else page.redirect('/admin');

				});
			}
		});

		_overlay.loop.target.focus();

	});

	_BodyDelegator.on('change', 'input.poster-upload', function(e){

		// min-width: 275px
		// min-height: 400px

		var file = e.target.files[0];

		// console.log(e);

		StartImages.call(_this, file);

		e.target.value = '';

		return false;

	});

	_this._EventListeners = [_BodyDelegator];
};

module.exports = Events;
