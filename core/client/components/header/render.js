var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	capitalize = require('./capitalize');

var RenderHeader = function(dom) {
	var actions = {
			userMenu: 'user-menu'
		},
		Menus = [],
		Logo,
		menu,
		SearchSmall,
		Search,
		Se = new RegExp('/search', 'g'),
		isSearch = Se.test(dom.path);

	// console.log(dom.active);
	// 
	if (dom._currentUser) dom._currentUser.path = dom.path;

	if (dom.active === actions.userMenu) {

		menu = require('./menus/userMenu')(dom._currentUser);

	} else {
		menu = undefined;
	}

	Logo = _h('a.bc-vod-clearfix.bc-vod-navbar-brand', {
		href: '/',
	}, [
		_h('img', {
			src: dom.assetsUrl + dom._a['/images/logo-small.png'] + '?' + dom.build,
			style:{
				width: '39px',
				margin: '7px 0 0 0'
			}
		})
	]);

	if (!isSearch) {

		SearchSmall = _h('div.bc-vod-visible-small.p-l-10', [
 			_h('form.m-t-10.m-b-10.bc-vod-display-inline-block', {
 				style:{
 					width: '100%',
 					'float': 'left' 
 				},
				attributes:{
					'action': '/search'
				}
			}, [
				_h('input.form-control.bc-vod-display-inline-block', {
					style:{
						width: '80%'
					},
					attributes:{
						type: 'search',
						name: 'q',
						placeholder: 'Search Kukuotv'
					}
				}),
				_h('button.bc-vod-button.bc-vod-button-primary.bc-vod-margin-small-left', {
					style:{
						width: '15%'
					},
					attributes:{
						type: 'submit'
					}
				}, [_h('i.ti-search')])
			])
 		]);

 		Search = _h('div.bc-vod-navbar-flip.bc-vod-navbar-content.bc-vod-hidden-small', [
 			_h('form.bc-vod-margin-remove.bc-vod-display-inline-block', {
 				style:{
 					width: '270px'
 				},
				attributes:{
					'action': '/search'
				}
			}, [
				_h('input.form-control.bc-vod-display-inline-block', {
					style:{
						width: '85%'
					},
					attributes:{
						type: 'search',
						name: 'q',
						placeholder: 'Search Kukuotv'
					}
				}),
				_h('button.bc-vod-button.bc-vod-button-primary.bc-vod-margin-small-left', {
					style:{
						width: '11%',
						background: 0,
					    border: 0,
					    outline: 0,
				        boxShadow: 'none'
					},
					attributes:{
						type: 'submit'
					}
				}, [_h('i.ti-search')])
			])
 		]);

	}

	Menus.push(
		_h('li.bc-vod-parent.bc-vod-visible-small', [
			_h('button', {
				attributes:{
					'data-action': 'show-menu'
				},
				style:{
					padding: '13px 13px 0 0'
				}
			}, [
				_h('svg', {
					namespace: 'http://www.w3.org/2000/svg',
					attributes:{
						version: '1.1',
						xmlns: 'http://www.w3.org/2000/svg',
						'xmlns:xlink': 'http://www.w3.org/1999/xlink',
						width: '17',
						height: '17',
						viewBox:'0 0 17 17'
					},
					innerHTML: '<g></g><path d="M16 3v2h-15v-2h15zM1 10h15v-2h-15v2zM1 15h15v-2h-15v2z" fill="#fff" />'
				})
			])
		])
	);

	if (dom._currentUser) {
		
		if (!dom._currentUser.frontend) {

			Menus.push(
				_h('li.dropdown.bc-vod-parent', {
						key: actions.userMenu
				}, [
					_h('button', {
							attributes:{
								'data-action': actions.userMenu,
								'data-dropdown':''
							}
						}, [
							_h('div.avatar.m-t-10', [
							_h('div.bc-vod-margin-small-top', [
								_h('i.ti-user')
							])
						])
					])
				])
			);

		} else {

			Menus.push(
				_h('li.dropdown.dath--parent', {
						key: actions.userMenu
				}, [
					_h('button.click-me', {
							attributes:{
								'data-action': actions.userMenu,
								'data-dropdown':''
							}
						}, [
							_h('div.avatar.m-t-10', [
							_h('img', {
								src: dom._currentUser.avatar
							})
						])
					])
				])
			);

		}

	} else {

		Menus.push(
			_h('li.dropdown.bc-vod-parent', {
					key: actions.userMenu
			}, [
				_h('a', {
					href: '/login?r='+ dom.path,
					attributes:{
						'data-action': 'login'
					}
				}, [String('Sign In')])
			])
		);

	}

	if (menu && dom._currentUser) {

		var dropDown = _h('div.bc-vod-dropdown.bc-vod-dropdown-flip', {
			style: {
				left: (dom.dropOffsets.left - 152) + 'px',
				display: 'block'
			}
		}, [
			_h('div.arrow'),
			_h('ul.bc-vod-nav.bc-vod-nav-navbar.bc-vod-nav-dropdown.bc-vod-dropdown-small',[
				menu
			])
		]);
 
	}

 	var Container = _h('div.p-r-5.p-l-15.bc-vod-container.bc-vod-container-center', [
 		Logo,
 		_h('ul.bc-vod-navbar-nav.bc-vod-hidden-small', map(dom.menus, function(menu){
 			return _h('li', [
 				_h('a', {
 					href: menu.slug,
 					rel: (menu.contact) ? 'external' : undefined
 				}, [String(capitalize(menu.name))])
 			]);
 		})),
		_h('div.bc-vod-navbar-flip.bc-vod-navbar-content', [
			_h('ul.bc-vod-navbar-nav', [
				Menus
			]),
			dropDown
		]),
		Search
	]);

	var ContainerSmall = _h('div.p-r-5.p-l-5.bc-vod-container-small.bc-vod-container.bc-vod-container-center.bc-vod-visible-small', {
		style:{
			display: (dom.showSmMenu) ? 'block' : 'none'
		}
	}, [
		_h('ul.bc-vod-navbar-nav.sub-container.p-l-10.bc-vod-margin-top', map(dom.menus, function(menu){
 			return _h('li.no-padding', [
 				_h('a.p-r-10', {
 					href: menu.slug,
 					rel: (menu.contact) ? 'external' : undefined
 				}, [String(capitalize(menu.name))])
 			]);
 		})),
 		SearchSmall
	]);

	var navClass = [
			'bc-vod-navbar-attached',
			'bc-vod-navbar'
		],
		headClass = [
			'bc-vod-header',
			'bc-vod-clearfix'
		];

	if (dom.backgroundMix) {

		navClass.push('bc-vod-background-mix');
		headClass.push('bc-vod-background-mix');
		
	}



	return _h('header', {
		className: headClass.join(' ')
	}, [
		_h('nav', {
			className: navClass.join(' ')
		}, [
			Container,
			ContainerSmall
		])
	]);
	
};

module.exports = RenderHeader;