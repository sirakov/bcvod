module.exports = [
	{name: 'movies', slug: '/category/movies'},
	{name: 'music', slug: '/category/music'},
	{name: 'series', slug: '/category/series'},
	{name: 'contact us', slug: '/contact-us', contact: true}
];