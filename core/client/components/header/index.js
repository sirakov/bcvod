var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	pick = require('mout/object/pick'),
	Freezer = require('freezer-js'),
	$ = require('sprint-js');

var Header = function(dom, prerender, content) {

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('header.bc-vod-header');

		obj.target = target;

	}

	_this._headOpts = new Freezer(pick(dom, ['_currentUser', 'assetsUrl', 'build', '_a', 'path', 'admin', 'menus', 'backgroundMix']));

	_this.loop = mainLoop(_this._headOpts.get().toJS(), Render, obj);

	_this.$ = $;

	if (!prerender) content.appendChild(_this.loop.target);

	_this._initEvents(document.body);
	
};

Header.prototype = {
	_initEvents: require('./events')
};

module.exports = Header;