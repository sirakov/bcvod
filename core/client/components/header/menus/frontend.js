var _h = require('virtual-dom/h');

var FrontEnd = function(user) {

	return [
		_h('li.wide', [
			_h('a',{
				href: '/me/profile' 
			}, [String('Profile')])
		]),
		_h('li.wide', [
			_h('a',{
				href: '/me/history' 
			}, [String('History')])
		]),
		_h('li',{
			className: 'divider'
		}),
		_h('li.wide', [
			_h('a',{
				href: '/me/logout?r=' + user.path,
				rel: 'external'
			}, [String('Logout')])
		])
	];
	
};

module.exports = FrontEnd;