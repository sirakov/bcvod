var _h = require('virtual-dom/h'),
	frontend = require('./frontend');

module.exports = function(user){

	if (user.frontend) return frontend(user);

	return [
		_h('li.wide', [
			_h('a',{
				href: '/admin/upload'
			}, [String('Upload Videos')])
		]),
		_h('li.wide', [
			_h('a',{
				href: '/admin/public'
			}, [String('Public videos')])
		]),
		_h('li.wide', [
			_h('a',{
				href: '/admin'
			}, [String('Videos')])
		]),
		_h('li',{
			className: 'divider'
		}),
		_h('li.wide', [
			_h('a',{
				href: '/me/logout?r=' + user.path,
				rel: 'external'
			}, [String('Logout')])
		])
	];

};