var Delegator = require('dom-delegate'),
	Overlay = require('../overlay');

var Events = function(body){
	
	var _this = this,
		_BodyDelegator = new Delegator(body);

	_BodyDelegator.on('click', 'a[data-action="login"]', function(e){

		e.preventDefault();

		var _overlay = Overlay.add({
					type: 'login',
					path: document.location.pathname,
					assetsUrl: GLOBALS.assetsUrl,
					build: GLOBALS.build,
					_a: GLOBALS._a
				}, {
				delete: function(){

					_overlay._destroy();

					
				}
			});

		return _overlay.loop.target.focus();
		

	});

	_BodyDelegator.on('click', 'button[data-action="login"]', function(e){

		e.preventDefault();

		var _overlay = Overlay.add({
					type: 'login',
					path: document.location.pathname,
					assetsUrl: GLOBALS.assetsUrl,
					build: GLOBALS.build,
					_a: GLOBALS._a
				}, {
				delete: function(){

					_overlay._destroy();

					
				}
			});

		return _overlay.loop.target.focus();
		

	});

	_BodyDelegator.on('click', '[data-action="show-menu"]', function(e){

		var _headOpts = _this._headOpts.get();

		if (_headOpts.showSmMenu) {

			_this.loop.update(_headOpts.remove('showSmMenu').toJS());

		} else {

			var set = _headOpts.set({
				'showSmMenu': true
			});

			_this.loop.update(set.toJS());

		}

	});

	_BodyDelegator.on('click', 'button[data-dropdown]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-dropdown]').dom[0],
			data = el.dataset,
			offsets = el.getBoundingClientRect(),
			_headOpts = _this._headOpts.get();

		if (data.action === _headOpts.active) {

			_this.loop.update(_headOpts.remove('active').toJS());

		} else {

			var set = _headOpts.set({
				'dropOffsets': offsets,
				'active': data.action
			});

			_this.loop.update(set.toJS());

		}


	});

	_BodyDelegator.on('click', function(e){

		var target = e.target,
			_headOpts = _this._headOpts.get(),
			selector = '[data-action="' + _headOpts.active + '"]';

		if (_headOpts.active && !/button/i.test(target.nodeName.toLowerCase()) && !_this.$(target).parents(selector).length) {

			_this.loop.update(_headOpts.remove('active').toJS());

		}

	});

	_this._EventListeners = [_BodyDelegator];
};

module.exports = Events;