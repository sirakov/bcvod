var Delegator = require('dom-delegate'),
	http = require('../http');

var Events = function(){
	
	var _this = this;

	var _BodyDelegator = new Delegator(document.body);

	_BodyDelegator.on('click', '.avatar-upload-img', function(e){

		var inputFile = document.querySelector('.avatar-upload');

		if (inputFile) return inputFile.click();

		return;

	});

	_BodyDelegator.on('change', 'input[name="avatar"]', function(e){

		var file = e.target.files[0];

		if (/image/.test(file.type)) {

			var img = new Image(),
				url = (URL || webkitURL).createObjectURL(file);
			
			img.onload = function(){

				var _that = this;

				var profile = _this._dom.get(),
					poster;

				if (profile.avatar) {

					(URL || webkitURL).revokeObjectURL(profile.avatar);

				}

				_this._dom.get().set({
					avatar: url
				});

				_this.avatar = file;

				_this.loop.update(_this._dom.get().toJS());

			};

			img.src = url;

		}

		e.target.value = '';

		return false;

	});

	_BodyDelegator.on('input', 'input[type="text"]', function(e){

		var el = e.target,
			data = el.dataset;

		_this._dom.get().set(data.type, el.value);

		_this.loop.update(_this._dom.get().toJS());

		el.value = '';

		return;


	});

	_BodyDelegator.on('click', 'button[data-action="save-profile"]', function(e){

		e.preventDefault();

		var profile = _this._dom.get().toJS(),
			data,
			obj = {};

		if (profile.fullname) obj.fullname = profile.fullname;
		if (profile.location) obj.location = profile.location;

		if (_this.avatar) {

			data = require('../utils/formData')(_this.avatar, obj);

		} else {

			data = obj;

		}

		return http({
			url: '/me',
			method: 'post',
			data: data
		}, function(err, res){

			if (err) return;

			

			if (profile.avatar) {

				window.URL.revokeObjectURL(profile.avatar);

			}

			if (profile.avatar) {

				_this._dom.get().set({
					avatar: res.data.avatar
				});

			}

			delete _this.avatar;

			_this.loop.update(_this._dom.get().toJS());

			// console.log(res);

		});

		return;		
		
	});
	

	_this._EventListeners = [_BodyDelegator];
	
};

module.exports = Events;