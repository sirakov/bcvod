var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	pick = require('mout/object/pick'),
	Freezer = require('freezer-js'),
	$ = require('sprint-js');


var Profile = function(dom, prerender, content){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('div.bc-vod-user-profile');

		obj.target = target;

	}

	_this._dom = new Freezer(pick(dom, ['fullname', 'avatar', 'location']));

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);
	
	_this.$ = $;

	if (!prerender) content.appendChild(_this.loop.target);
	
	_this._EventListeners = [];

	_this._initEvents();
	
};

Profile.prototype = {
	_initEvents: require('./events')
};

module.exports = Profile;