var _h = require('virtual-dom/h');

var Render = function(state){

	return _h('div.bc-vod-user-profile.bc-vod-container-center.bc-vod-container', [
		_h('div.bc-vod-grid.bc-vod-margin-large-top', [
			_h('div.bc-vod-width-2-4.bc-vod-container-center', [
				_h('h1.title.bold.text-center', [String('Edit Profile')]),
				_h('div.section-divider', [
					_h('hr')
				]),
				_h('div.bc-vod-grid.bc-vod-margin-large-top', [
					_h('div.bc-vod-width-2-4', [
						_h('form', [
							_h('div.form-group', [
								_h('label', [String('Fullname')]),
								_h('input.form-control', {
									value: state.fullname,
									attributes:{
										placeholder: 'Fullname',
										type: 'text',
										'data-type': 'fullname'
									}
								})
							]),
							_h('div.form-group', [
								_h('label', [String('Location')]),
								_h('input.form-control', {
									value: state.location,
									attributes:{
										'data-type': 'location',
										type: 'text',
										placeholder: 'Location eg(GH, NG, etc)'
									}
								})
							]),
							_h('div.form-group', [
								_h('button.btn.btn-primary.btn-rounded', {
									attributes:{
										'data-action': 'save-profile'
									}
								}, [String('Save')])
							])
						])
					]),
					_h('div.bc-vod-width-2-4', [
						_h('img.avatar-upload-img.bc-vod-float-right', {
							src: state.avatar,
							style:{
							    width: '100px',
							    height: '100px',
							    borderRadius: '100%',
							    cursor: 'pointer'
							}
						}),
						_h('span.bold', [String('Click to change your avatar')]),
						_h('input.avatar-upload', {
							style: {
								display: 'none'
							},
							attributes:{
								type: 'file',
								name: 'avatar'
							}
						})
					])
				])
			])
		])
	]);
		
};

module.exports = Render;