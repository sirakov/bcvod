var Delegator = require('dom-delegate'),
	http = require('../http')
	qs = require('mout/queryString/encode'),
	contains = require('mout/array/contains'),
	merge = require('mout/object/merge'),
	ladda = require('ladda'),
	page = require('page.js');

var Events = function(){
	
	var _this = this;

	var _BodyDelegator = new Delegator(document.body);
	
	_BodyDelegator.on('click', 'button[data-action="load-more"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="load-more"]').dom[0],
			l = ladda.create(el),
			dom = _this._dom.get().toJS(),
			path = '/me/history',
			last = dom.last;

		l.start();

		http({
			url: path + qs(last),
			method: 'get'
		}, function(err, res){

			if (err) {

				l.stop();
				l.remove();

				_this.loop.update(_this._dom.get().toJS());

				return;

			}
			
			var currentVideos = _this._dom.get().videos.toJS(),
				videos = res.data.videos;

			if (res.data.last) _this._dom.get().last.set(res.data.last);
			else _this._dom.get().remove('last');

			_this._dom.get().videos.set(currentVideos.concat(videos));

			l.stop();
			l.remove();

			_this.loop.update(_this._dom.get().toJS());

			setTimeout(function(){

				_this.echo.render();

			}, 100);


		});

	});

	_this._EventListeners = [_BodyDelegator];
	
};

module.exports = Events;