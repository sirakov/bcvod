var _h = require('virtual-dom/h'),
	gridify = require('../homepage/gridify'),
	map = require('fast.js/array/map'),
	chunk = function(arr, chunkSize) {
	    var groups = [], i;
	    for (i = 0; i < arr.length; i += chunkSize) {
	        groups.push(arr.slice(i, i + chunkSize));
	    }
	    return groups;
	};

var Render = function(state){

	var LoadMore,
		Videos,
		chunks = chunk(state.videos, 4),
		layouts;

	layouts = map(chunks, function(chunk){

		var videos = gridify(chunk, state);

		return _h('section.bc-vod-video-section.clearfix', {
			attributes:{
				'data-video-count': chunk.length
			}
		}, videos);

	});



	if (state.last) {

		LoadMore = _h('button.ladda-button.btn.btn-primary.bc-vod-container-center.bc-vod-display-block.bc-vod-margin-large', {
			attributes:{
				'data-action': 'load-more',
				'data-style': 'expand-left',
				'data-spinner-color': '#fff'
			}
		}, [
			_h('span.ladda-label', [String('Load More')]),
			_h('span.ladda-spinner'),
			_h('div.ladda-progress', {
				style:{
					width: '0px'
				}
			})
		]);

	}

	if (state.videos.length) {

		Videos = _h('div.bc-vod-width-1-1.bc-vod-container-center', layouts);

	} else {

		Videos = _h('div.bc-vod-width-1-1.bc-vod-container-center', [
			_h('div.bc-vod-grid', [
				_h('div.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-4', [
					_h('h5.bold.text-center.bc-vod-margin-large-top.bc-vod-margin-large-bottom', [String('No Videos Found')])
				])
			])
		]);

	}

	return _h('div.bc-vod-history-page.bc-vod-container-center.bc-vod-container', [
		_h('h1.title.bold.text-center.font-montserrat.bc-vod-margin-large-top', [String('History')]),
		_h('div.section-divider', [
			_h('hr')
		]),
		_h('div.bc-vod-grid.bc-vod-margin-large-top', {
			style:{
				opacity: (state.isLoading) ? 0.3: undefined
			}
		}, [
			Videos
		]),
		LoadMore
	])
	
};

module.exports = Render;