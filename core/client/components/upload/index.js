var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js'),
	pick = require('mout/object/pick'),
	filesize = require('filesize'),
	$ = require('sprint-js'),
	io = require('socket.io-client');

var Upload = function(dom, prerender, content) {

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-upload-div');

		obj.target = target;

	}

	dom.files = [];
	dom.series = {};

	_this._dom = new Freezer(pick(dom, ['utilsUrl', 'baseUrl', 'categories', 'supportsFileApi', 'files', 'assetsUrl', 'series']));

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);

	_this.inputFile = _this.loop.target.querySelector('#fileInput');

	_this.message = 'Your uploads haven\'t finished yet!';

	_this.deleteMessage = 'Are you sure you wish to cancel this upload?';

	_this.sockets = [];

	_this.posters = {};

	_this.sockUrl = _this._dom.get().utilsUrl;

	_this.socket = io.connect(_this.sockUrl, {
        'force new connection': true
    });

	_this.selectedFiles = [];

	_this.head = require('../head').client(window.GLOBALS);

	_this.$ = $;

	if (!prerender) content.appendChild(_this.loop.target);

	_this._initEvents(document.body);
	
};

Upload.prototype = {
	_initEvents: require('./events'),
	_startUpload: require('./startUpload'),
	_moreData: require('./moreData'),
	getDuration: require('./getDuration')
};

module.exports = Upload;