var duration = require('../utils/duration'),
	bfu = require('../utils/bfu');

var Duration = function(index){

	var _this = this;

	var file = _this.selectedFiles[index],
		mime = file.type,
		rd = new FileReader();

	rd.onload = function(e) {
		
		var blob = new Blob([e.target.result], {type: mime}),
			url = (URL || webkitURL).createObjectURL(blob),
			video = document.createElement("video");

		var canvas = document.createElement('canvas');
		var context = canvas.getContext('2d');

		canvas.width = 400;
		canvas.height = 296;
		
		video.preload = 'metadata';

		video.addEventListener('loadedmetadata', function() {

			video.currentTime = 5;

		}, false);

		video.addEventListener('seeked', function() {
			
			context.fillRect(0, 0, 400, 296);
			context.drawImage(video, 0, 0, 400, 296);

			_this._dom.get().files[index].set({
				previewUrl: (URL || webkitURL).createObjectURL(bfu(canvas.toDataURL('image/jpeg')))
			});

			_this.loop.update(_this._dom.get().toJS());

			(URL || webkitURL).revokeObjectURL(url);

		}, false);

		video.addEventListener('error', function() {

			(URL || webkitURL).revokeObjectURL(url);

		}, false);

		video.src = url;

	};

	var splice = (file.size > (1024 * 22000)) ? (2/100) * file.size: (6/100) * file.size;

	rd.readAsArrayBuffer(file.slice(0, splice)); 
};

module.exports = Duration;