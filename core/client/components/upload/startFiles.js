var forEach = require('fast.js/array/forEach');

var StartFiles = function(files){

	var _this = this;

	forEach(files, function(file){

		if (/video/.test(file.type)) {

			_this.selectedFiles.push(file);

			var index = _this.selectedFiles.length - 1;

			_this._dom.get().files.push({
				fileName: file.name,
				progress: true
			});

			_this.loop.update(_this._dom.get().toJS());

			// _this.getDuration(index);

			_this._startUpload(index);

		}

	});
	
};

module.exports = StartFiles;