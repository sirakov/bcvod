var Delegator = require('dom-delegate'),
	forEach = require('fast.js/array/forEach'),
	where = require('fast.js/array/filter'),
	findIndex = require('mout/array/findIndex'),
	http = require('../http'),
	params = require('../utils/params'),
	StartFiles = require('./startFiles'),
	StartImages = require('./startImages'),
	page = require('page.js');

var Events = function(body){
	
	var _this = this,
		_BodyDelegator = new Delegator(body);

	_BodyDelegator.on('change', 'input#fileInput', function(e){

		StartFiles.call(_this, e.target.files);

		_this.inputFile.value = '';

		return;

	});

	_BodyDelegator.on('dragenter', '.site-main', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('dragover', '.site-main', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('dragstart', '.site-main', function(e){

		e.dataTransfer.effectAllowed = 'copyMove';
		e.dataTransfer.dropEffect = 'copy';

	});

	_BodyDelegator.on('dragleave', '.site-main', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('drop', '.site-main', function(e){

		e.preventDefault();

		StartFiles.call(_this, e.dataTransfer.files);

		return;

	});

	_BodyDelegator.on('dragend', '.site-main', function(e){

		// console.log(e);

		e.preventDefault();

	});

	_BodyDelegator.on('click', 'button[data-action="series"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="series"]').dom[0],
			dom = _this._dom.get().toJS();

		if (!dom.isSeries) _this._dom.get().set('isSeries', true);
		else _this._dom.get().remove('isSeries');

		_this.loop.update(_this._dom.get().toJS());

		return false;

	});

	_BodyDelegator.on('click', '.preview-pane', function(e){

		var inputFile,
			isButton = e.target.tagName.toLowerCase() === 'button',
			isIcon = e.target.tagName.toLowerCase() === 'i';

		if (e.target.classList.contains('preview-pane') && isButton && !isIcon) {

			inputFile = e.target.querySelector('input.poster-upload')

		} else if (!isButton && !isIcon) {

			var pane = _this.$(e.target).parents('.preview-pane').dom[0];

			inputFile = pane.querySelector('input.poster-upload');

		}

		if (inputFile) return inputFile.click();

		return;

	});

	_BodyDelegator.on('change', 'input.poster-upload', function(e){

		// min-width: 275px
		// min-height: 400px
		// 
		var file = e.target.files[0];

		var el = (e.target.tagName.toLowerCase() === 'input') ? e.target: _this.$(e.target).parents('input[data-index]').dom[0],
			index = el.dataset.index;

		StartImages.call(_this, file, index);

		e.target.value = '';

		return false;

	});

	_BodyDelegator.on('dragenter', '.preview-pane', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('dragover', '.preview-pane', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('dragstart', '.preview-pane', function(e){

		e.dataTransfer.effectAllowed = 'copyMove';
		e.dataTransfer.dropEffect = 'copy';

	});

	_BodyDelegator.on('dragleave', '.preview-pane', function(e){

		e.preventDefault();

	});

	_BodyDelegator.on('drop', '.preview-pane', function(e){

		e.preventDefault();

		var file = e.dataTransfer.files[0];

		var el = (e.target.classList.contains('preview-pane')) ? e.target: _this.$(e.target).parents('.preview-pane[data-index]').dom[0],
			index = el.dataset.index;

		StartImages.call(_this, file, index);

		return;

	});

	_BodyDelegator.on('dragend', '.preview-pane', function(e){

		// console.log(e);

		e.preventDefault();

	});

	_BodyDelegator.on('click', '.dropzone .message', function(e){

		return _this.inputFile.click();

	});

	_BodyDelegator.on('click', 'a[data-action="pauseAll"]', function(e){

		e.preventDefault();

		var files = where(_this._dom.get().files, function(val){
				return !val.finished && val.uploading && !val.paused;
			});

		forEach(files, function(file){

			var index = findIndex(_this._dom.get().files, {
				fileName: file.fileName
			});

			_this._dom.get().files[index].set({
				paused: true
			});

		});

		_this.loop.update(_this._dom.get().toJS());

		return;

	});

	_BodyDelegator.on('click', 'a[data-action="removeAll"]', function(e){

		e.preventDefault();

		_this._dom.get().files.reset([]);

		_this.posters = {};

		_this.head.loop.update({
            title: 'Upload'
        });

		_this.loop.update(_this._dom.get().toJS());

		return;

	});

	_BodyDelegator.on('click', 'a[data-action="resumeAll"]', function(e){

		e.preventDefault();

		var doc = _this._dom.get(),
			files = where(_this._dom.get().files, function(val){
				return !val.finished && val.uploading && val.paused;
			});

		forEach(files, function(file, index){

			if (_this.sockets[index].connected) {

				_this._dom.get().files[index].remove('paused');

				if (doc.error) _this._dom.get().remove('error');

				_this.sockets[index].emit('start', {
					'name' : _this.selectedFiles[index].name,
					'size' : _this.selectedFiles[index].size
				});

			} else {

				_this._dom.get().files[index].set({
	                paused: true,
	                error: true
	            });

			}

		});

		_this.loop.update(_this._dom.get().toJS());

		return;

	});

	_BodyDelegator.on('click', 'a[data-action="cancelAll"]', function(e){

		e.preventDefault();

		var doc = _this._dom.get(),
			files = where(_this._dom.get().files, function(val){
				return !val.finished && val.uploading && !val.cancelled;
			});

		forEach(files, function(file){

			var index = findIndex(_this._dom.get().files, {
					fileName: file.fileName
				});

			var SocketIndex = findIndex(_this.sockets, {
		            id: file.socketId
		        }),
				FileIndex = findIndex(_this.selectedFiles, {
	                name: file.fileName
	            });

			_this._dom.get().files[index].set({
				cancelled: true
			});

			_this.loop.update(_this._dom.get().toJS());
			
			if (_this.sockets[SocketIndex]) {

				_this.sockets[SocketIndex].emit('start', {
		            'name' : _this.selectedFiles[FileIndex].name,
		            'size' : _this.selectedFiles[FileIndex].size
		        });

		    }

		});

		_this.head.loop.update({
            title: 'Upload'
        });

		return;

	});

	_BodyDelegator.on('click', 'button.cancel-upload', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-index]').dom[0],
			index = el.dataset.index,
			file = _this._dom.get().files[index];

		var SocketIndex = findIndex(_this.sockets, {
	            id: file.socketId
	        }),
			FileIndex = findIndex(_this.selectedFiles, {
                name: file.fileName
            });

		_this._dom.get().files[index].set({
			cancelled: true
		});

		_this.loop.update(_this._dom.get().toJS());

		if (_this.sockets[SocketIndex]) {
		
			_this.sockets[SocketIndex].emit('start', {
	            'name' : _this.selectedFiles[FileIndex].name,
	            'size' : _this.selectedFiles[FileIndex].size
	        });

	    }

	    if (_this._dom.get().files.length === 1) {

	    	_this.head.loop.update({
                title: 'Upload'
            });

	    }

		return;

	});

	_BodyDelegator.on('click', 'button.pause-upload', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-index]').dom[0],
			index = el.dataset.index;

		_this._dom.get().files[index].set({
			paused: true
		});

		_this.loop.update(_this._dom.get().toJS());

		return;
		
	});

	_BodyDelegator.on('click', 'button[data-action="remove"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-index]').dom[0],
			index = el.dataset.index,
			file = _this._dom.get().files[index];

		if (_this.posters[file.vid]) delete _this.posters[file.vid];

		_this._dom.get().files.splice(index, 1);

		_this.head.loop.update({
            title: 'Upload'
        });

		_this.loop.update(_this._dom.get().toJS());

		return;
		
	});

	_BodyDelegator.on('click', 'button.resume-upload', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-index]').dom[0],
			index = el.dataset.index;

		if (_this.sockets[index].connected) {

			var doc = _this._dom.get();

			_this._dom.get().files[index].remove('paused');

			if (doc.error) _this._dom.get().remove('error');

			_this.loop.update(_this._dom.get().toJS());

			_this.sockets[index].emit('start', {
				'name' : _this.selectedFiles[index].name,
				'size' : _this.selectedFiles[index].size
			});

		} else {

			_this._dom.get().files[index].set({
                paused: true,
                error: true
            });

			_this.loop.update(_this._dom.get().toJS());

		}

		return;
		
	});

	_BodyDelegator.on('input', 'form input[type="text"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'input') ? e.target: _this.$(e.target).parents('input[data-index]').dom[0],
			index = el.dataset.index,
			dom = _this._dom.get();

		if (dom.isSeries) {

			_this._dom.get().series.set({
				title: e.target.value
			});

		} else {

			_this._dom.get().files[index].set({
				title: e.target.value,
				dirty: true
			});

		}

		_this.loop.update(_this._dom.get().toJS());

	});

	_BodyDelegator.on('input', 'form textarea', function(e){

		var el = (e.target.tagName.toLowerCase() === 'textarea') ? e.target: _this.$(e.target).parents('textarea[data-index]').dom[0],
			index = el.dataset.index,
			dom = _this._dom.get();


		if (dom.isSeries) {

			_this._dom.get().series.set({
				description: e.target.value
			});

		} else {

			_this._dom.get().files[index].set({
				description: e.target.value,
				dirty: true
			});

		}

		_this.loop.update(_this._dom.get().toJS());
		
	});

	_BodyDelegator.on('input', 'form select', function(e){

		var el = (e.target.tagName.toLowerCase() === 'select') ? e.target: _this.$(e.target).parents('select[data-index]').dom[0],
			index = el.dataset.index,
			video = _this._dom.get().files[index].toJS();

		_this._dom.get().files[index].set({
			category: '/' + _this._dom.get().categories[e.target.selectedIndex].name + video.savedIn.replace('/draft', ''),
			dirty: true
		});

		_this.loop.update(_this._dom.get().toJS());
		
	});

	_BodyDelegator.on('click', 'form button.save-series', function(e){

		e.preventDefault();

		var dom = _this._dom.get().toJS();

		http({
			url: '/admin/upload',
			method: 'post',
			data:{
				title: dom.series.title,
				description: dom.series.description
			}
		}, function(err, res){

			if (err) return;

			return page('/admin/v/' + res.data.vid + '/edit');

		});

	});

	_BodyDelegator.on('click', 'form button.save-video', function(e){

		var el = (e.target.tagName.toLowerCase() === 'form') ? e.target: _this.$(e.target).parents('form[data-index]').dom[0],
			index = el.dataset.index;

		e.preventDefault();

		var video = _this._dom.get().files[index].toJS(),
			vid = params.get({
				vid: video.vid,
				savedIn: video.savedIn
			}),
			data,
			obj = {};

		if (video.title) obj.title = video.title;
		if (video.description) obj.description = video.description;
		if (video.category) obj.category = video.category;
		if (video.live) obj.live = video.live;

		if (video.imgMeta) obj.imgMeta = JSON.stringify(video.imgMeta);

		if (_this.posters[video.vid]) {

			data = require('../utils/formData')(_this.posters[video.vid], obj);

		} else {

			data = obj;

		}

		return http({
			url: '/admin/v/' + vid + '/edit',
			method: 'post',
			data: data
		}, function(err, res){

			if (err) return;

			if (video.previewUrl) window.URL.revokeObjectURL(video.previewUrl);

			_this._dom.get().files[index].set({
				dirty: false,
				previewUrl: JSON.parse(res.data.poster).src,
				posterUploaded: true
			});

			delete _this.posters[video.vid];

			_this.loop.update(_this._dom.get().toJS());

		});

	});


	_this._EventListeners = [_BodyDelegator];
};

module.exports = Events;