var Convert = function(data){
	
	var obj = {
        title: data.meta.title,
        vid: data.meta.vid,
        savedIn: data.meta.savedIn,
        state: data.meta.status
    };

    if (obj.state === 'live') obj.live = true;

    if (data.meta.description) obj.description = data.meta.description;

    if (data.meta.category) obj.category = data.meta.category;

    if (data.meta.poster) {

        var source = JSON.parse(data.meta.poster);

        obj.previewUrl = source.src;
        obj.posterUploaded = true;

        obj.imgMeta = {
            width: source.width,
            height: source.height
        };

    }

    obj.dirty = false;

    return obj;	

};

module.exports = Convert;