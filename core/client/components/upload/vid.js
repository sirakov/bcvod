module.exports = function(id){
	var Id = id.savedIn.split('/');

    Id.shift();

    Id.push(id.vid || id.uid);

    return Id.join('-').replace('draft-', '');
};