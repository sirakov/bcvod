var _h = require('virtual-dom/h'),
	categories = require('../header/categories'),
	map = require('fast.js/array/map'),
	every = require('fast.js/array/every'),
	where = require('fast.js/array/filter'),
	getVid = require('./vid');

var Render = function(obj){

	var Status,
		Progress,
		Uploaded,
		Input,
		UploadButton,
		Pause,
		Preview,
		Message,
		Files;

	Input = _h('input#fileInput', {
		attributes:{
			type: 'file',
			multiple: 'multiple',
			accept: 'video/*'
		},
		style: {
			display: 'none'
		}
	});

	Message = _h('div.message');

	// console.log(obj);

	if (obj.files.length) {

		var Control;

		if (obj.error) {

			Control = _h('div.bc-vod-grid.bc-vod-grid-large.bc-vod-margin-large-top', [
				_h('div.bc-vod-width-2-3.bc-vod-container-center', [
					_h('div.bc-vod-alert.bc-vod-alert-danger.bc-vod-text-center', [
						String('Oops! You’re no longer connected to the Internet. Once you reconnect, your upload will resume.')
					])
				])
			]);

		} else if (obj.files.length > 1) {

			var ControlText,
				ControlAction,
				Controls,
				files = where(obj.files, function(file){
					return !file.finished && file.uploading;
				}),
				allFinished = every(files, function(file){
					return file.finished;
				}),
				allPaused = every(files, function(file){
					return file.paused;
				});


			if (allPaused) {

				ControlText = 'Resume All';

				ControlAction = 'resumeAll';

			} else {

				ControlText = 'Pause All';

				ControlAction = 'pauseAll';

			}

			if (allFinished) {

				Controls = Controls = _h('div.bc-vod-float-right', [
					_h('a', {
						attributes:{
							'data-action': 'removeAll'
						}
					}, [String('Remove all')])
				]);

			} else {

				Controls = _h('div.bc-vod-float-right', [
					_h('a', {
						attributes:{
							'data-action': ControlAction
						}
					}, [String(ControlText)]),
					String(' / '),
					_h('a', {
						attributes:{
							'data-action': 'cancelAll'
						}
					}, [String('Cancel all')])
				]);

			}

			Control = _h('div.bc-vod-grid.bc-vod-grid-large.bc-vod-margin-large-top', [
				_h('div.bc-vod-width-1-1', [
					_h('div.bc-vod-alert.bc-vod-alert-info', [
						String(obj.files.length + ' Uploads'),
						Controls
					])
				])
			]);
		}

		Files = _h('div.file-uploads', map(obj.files, function(file, index){

			var RemoveButton;

			if (file.previewUrl && typeof file.previewUrl !== 'boolean') {

				Preview = _h('div', [
					_h('img.preview-canvas.bc-vod-container-center',{
						style:{
							display: 'block',
							width: '275px'
						},
						src: (file.posterUploaded) ? obj.assetsUrl + '/posters/' +  file.previewUrl: file.previewUrl
					}),
					_h('input.poster-upload', {
						attributes:{
							type: 'file',
							accept: 'image/*',
							'data-index': index
						},
						style:{
							display: 'none'
						}
					})
				]);

			} else {

				var De = String('Drop file to upload'),
					Sp = _h('span', {
						style:{
							display: 'block'
						}
					}, [String('or click here')]);

				if (typeof file.previewUrl === 'boolean') {

					De = undefined;

					Sp = _h('span', {
						style:{
							display: 'block',
							color: 'red'
						}
					}, [String('Error: file must be at least 275px x 407px in dimensions')]);

				}

				Preview = _h('div.preview-canvas.bc-vod-placeholder.bc-vod-placeholder-large.bc-vod-text-center', [
					De,
					Sp,
					_h('input.poster-upload', {
						attributes:{
							type: 'file',
							accept: 'image/*',
							'data-index': index
						},
						style:{
							display: 'none'
						}
					})
				]);

			}

			if (file.finished) {

			 	Status = _h('div.uploading', [String(file.fileName)]);

			 	UploadButton = undefined;

			 	Pause = undefined;

			} else if (file.uploading && file.fileName) {

				Status = _h('div.uploading', [String(file.fileName)]);

				UploadButton = _h('button.cancel-upload.btn.btn-sm.btn-danger', {
					attributes:{
						'data-index': index,
						disabled: (obj.error || file.cancelled) ? true : undefined
					}
				}, [
					_h('i.ti-close')
				]);

				if (file.paused) {

					Pause = _h('button.resume-upload.btn.btn-sm.btn-primary', {
						attributes:{
							'data-index': index,
							disabled: (obj.error || file.cancelled) ? true : undefined
						}
					}, [
						_h('i.ti-control-play')
					]);

				} else {

					Pause = _h('button.pause-upload.btn.btn-sm.btn-primary', {
						attributes:{
							'data-index': index,
							disabled: (obj.error || file.cancelled) ? true : undefined
						}
					}, [
						_h('i.ti-control-pause')
					]);

				}

			} else if (file.fileName) {

				Status = _h('div.fileName', [String(file.fileName)]);

				UploadButton = undefined;


			}

			if ('progress' in file) {

				var clas;

				if (obj.error) clas = 'bc-vod-progress-danger';
				else clas = 'bc-vod-progress-success';

				var Progress;

				if (typeof file.progress === 'boolean') {

					Progress = _h('div.bc-vod-progress.bc-vod-progress-striped.bc-vod-active', [
						_h('div.bc-vod-progress-bar', {
							style:{
								width: 100 + '%'
							}
						}, ['Starting Upload'])
					]);

				} else {

					var text,
						length;

					length = file.progress;

					if (file.progress === 100 && file.finished && file.state === 'local') text = 'Uploading to S3!';
					else if (file.progress === 100 && file.finished && file.state === 's3') text = 'Transcoding!';
					else if (file.progress === 100 && file.finished && file.state === 'live') text = 'Upload Complete!';
					else text = file.progress + '%';

					if (file.cancelled) {

						text = 'Cancelled';
						length = 100;
						clas = 'bc-vod-progress-danger';

					}

					Progress = _h('div.bc-vod-progress.bc-vod-progress-striped.bc-vod-active.' + clas, [
						_h('div.bc-vod-progress-bar', {
							style:{
								width: length + '%'
							}
						}, [text])
					]);

				}

			}

			if (file.fileSize && 'uploaded' in file) {

				Uploaded = _h('div#uploaded', [String(file.uploaded + '/' + file.fileSize)]);
			}

			if (file.finished) {

				RemoveButton = _h('button.btn.btn-default.btn-sm.pull-right.bc-vod-margin-small-bottom', {
					attributes:{
						'data-action': 'remove',
						'data-index': index
					}
				}, [
					_h('i.ti-close')
				]);

			} else {

				RemoveButton = undefined;
				
			}

			var newVid;

			if (file.vid && file.savedIn) {

				var newVid = getVid({
					vid: file.vid,
					savedIn: file.savedIn
				});

			}

			return _h('div.bc-vod-grid.bc-vod-grid-large.bc-vod-margin-large-top', [
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-5.bc-vod-margin-top', [
					_h('div.preview-pane', {
						attributes:{
							'data-index': index
						}
					}, [
						Preview,
						_h('div.action-buttons', [
							Pause,
							UploadButton
						])
					]),
					Progress,
					Status,
					Uploaded
				]),
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-3-5.bc-vod-margin-top', [
					RemoveButton,
					_h('form', {
						attributes:{
							role: 'form',
							'data-index': index
						}
					}, [
						_h('div.form-group', {
							style:{
								display: (file.state === 'live') ? 'block' : 'none'
							}
						}, [
							_h('label', [String('VideoId')]),
							_h('input.form-control.input-lg', {
								attributes:{
									placeholder: (file.vid)? newVid: 'videoId',
									'data-index': index,
									type: 'text'
								},
								value: (file.vid)? newVid: undefined,
								disabled: true
							})
						]),
						_h('div.form-group', [
							_h('label', [String('Title')]),
							_h('input.form-control.input-lg', {
								attributes:{
									placeholder: file.fileName,
									'data-index': index,
									type: 'text'
								},
								value: (file.title)? file.title: undefined,
								disabled: (file.cancelled)? true : undefined
							})
						]),
						_h('div.form-group', [
							_h('label', [String('Description')]),
							_h('textarea.form-control', {
								attributes:{
									placeholder: 'No description yet',
									'data-index': index,
									rows: 7
								},
								disabled: (file.cancelled)? true : undefined
							}, [String((file.description)? file.description: '')])
						]),
						_h('div.form-group', {
							style:{
								display: (file.state === 'live') ? 'block' : 'none'
							}
						}, [
							_h('label', [String('Category')]),
							_h('select.form-control', {
								attributes:{
									placeholder: 'Select Category',
									'data-index': index,
									disabled: (file.cancelled)? true : undefined
								}
							}, map(obj.categories, function(category){
								var attrs = {},
									re = new RegExp(category.name);

								if (re.test(file.category)) attrs.selected = 'selected';

								return _h('option', {
									attributes: attrs
								}, [String(category.name)])
							}))
						]),
						_h('div.bc-vod-form-row.bc-vod-clearfix', [
							_h('button.save-video.btn-primary.btn', {
								attributes:{
									type: 'submit',
									'data-index': index,
									disabled: (obj.error || !file.dirty || file.cancelled)? true : undefined
								}
							}, [String('Save')])
						])
					])
				])
			])
		}))

	} else {

		Files = undefined;

	}

	var ZoneAttrs = {},
		Series;

	if (obj.files.length || obj.isSeries) {

		ZoneAttrs.style = {
			'display': 'none'
		};

	}

	if (obj.isSeries) {

		Series = _h('div.bc-vod-series.bc-vod-width-small-1-1.bc-vod-width-medium-3-4.bc-vod-width-large-2-4.bc-vod-container-center', [

			_h('form', {
				attributes:{
					role: 'form'
				}
			}, [
				_h('div.form-group', [
					_h('label', [String('Title')]),
					_h('input.form-control.input-lg', {
						attributes:{
							placeholder: 'Series Title',
							'data-action': 'series',
							type: 'text'
						},
						value: (obj.series.title)? obj.series.title: undefined
					})
				]),
				_h('div.form-group', [
					_h('label', [String('Description')]),
					_h('textarea.form-control', {
						attributes:{
							placeholder: 'No description yet',
							'data-action': 'series',
							rows: 7
						}
					}, [String((obj.series.description)? obj.series.description: '')])
				]),
				_h('div.bc-vod-form-row.bc-vod-clearfix', [
					_h('button.save-series.btn-primary.btn', {
						attributes:{
							type: 'submit'
						}
					}, [String('Create')])
				])
			])

		]);

	}

	var toggleClassList = [
		'btn',
		'bc-vod-margin-large-top'
	];

	if (obj.isSeries) {

		toggleClassList.push('btn-info');

	} else {

		toggleClassList.push('btn-default');

	}


	return _h('div.bc-vod-upload-div.bc-vod-container-center.bc-vod-container.bc-vod-margin-large-bottom.bc-vod-main-div', [
		_h('button', {
			className: toggleClassList.join(' '),
			style:{
				margin: '0 auto',
				display: (obj.files.length) ? 'none' : 'block'
			},
			attributes:{
				'data-action': 'series'
			}
		}, ['Series']),
		_h('div.bc-vod-grid.bc-vod-grid-small.bc-vod-margin-large-top', ZoneAttrs, [
			_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-3-4.bc-vod-width-large-3-5.bc-vod-container-center', [
				_h('div.panel.panel-default', [
					_h('div.panel-heading', [
						_h('div.panel-title', [String('Upload Video')])
					]),
					_h('div.panel-body.no-scroll.no-padding', [
						_h('div.dropzone', [
							Message,
							Input
						])
					])
				])
			])
		]),
		Control,
		Files,
		Series
	]);

	
};

module.exports = Render;