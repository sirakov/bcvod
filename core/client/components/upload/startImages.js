var Vibrant = require('node-vibrant');

var StartImages = function(file, index){

	if (/image/.test(file.type)) {

		var _this = this,
			img = new Image(),
			url = (URL || webkitURL).createObjectURL(file);
		
		img.onload = function(){

			var _that = this;

			Vibrant.from(url).getPalette(function(err, palette){

				var video = _this._dom.get().files[index];

				if (video.previewUrl && !video.posterUploaded) {

					(URL || webkitURL).revokeObjectURL(video.previewUrl);

				}

				if (_that.width >= 275 && _that.height >= 407) {

					_this.posters[video.vid] = file;

					_this._dom.get().files[index].set({
						previewUrl: url,
						posterUploaded: false,
						dirty: true,
						imgMeta: {
							width: _that.width,
							height: _that.height,
							color: palette['Vibrant'].getHex()
						}
					});

				} else {

					_this._dom.get().files[index].set({
						previewUrl: false,
						posterUploaded: false,
						dirty: true,
						imgMeta: undefined
					});

				}

				_this.loop.update(_this._dom.get().toJS());

			});

		};

		img.src = url;

	}
	
};

module.exports = StartImages;