var filesize = require('filesize'),
    reduce = require('mout/array/reduce'),
    findIndex = require('mout/array/findIndex');


module.exports = function (data, index){

    var _this = this,
        FileIndex = findIndex(_this.selectedFiles, {
            name: data.name
        }),
    	_source = _this.selectedFiles[FileIndex],
    	place = data.uploaded,
    	NewFile,
        _start,
        _end,
        totalFileLengths = _this._dom.get().files.length,
        totalFileProgress = reduce(_this._dom.get().files, function(a, b){

            return a + b.progress;

        }, 0);

    _this.head.loop.update({
        title: '('+ Math.round(totalFileProgress / totalFileLengths) +'%) Upload'
    });

    var uploadsize = filesize(data.uploaded, {
            output: 'string'
        }),
        obj = {
            progress: Math.round(data.percent * 100)/100,
            uploaded: uploadsize
        },
        file = _this._dom.get().files[index],
        SocketIndex = findIndex(_this.sockets, {
            id: file.socketId
        });

    file.set(obj);

    _this.loop.update(_this._dom.get().toJS());

    if (_this._dom.get().files[index].cancelled) return _this.sockets[SocketIndex].emit('cancel', {
        'name' : _source.name,
        vid: data.vid,
        savedIn: file.savedIn,
        poster: (file.posterUploaded) ? file.previewUrl : null
    });

    if (_this._dom.get().files[index].paused) return _this.sockets[SocketIndex].emit('pause', {
        'name' : _source.name,
        vid: data.vid
    });

    _start = place;

    _end = place + 424288;

    NewFile = (_source.slice || _source.webkitSlice || _source.mozSlice).call(_source, _start, _end);
        
    _this.sockets[SocketIndex].emit('upload', {
        'name' : _source.name,
        buffer : NewFile,
        vid: data.vid
    });

};