var filesize = require('filesize'),
    io = require('socket.io-client'),
    findIndex = require('mout/array/findIndex'),
    Convert = require('./convert');

var StartUpload = function(index){

    var _this = this;

    var size = filesize(_this.selectedFiles[index].size, {
        output: 'string'
    });

    _this._dom.get().files[index].set({
        uploading: true,
        progress: true,
        uploaded: 0,
        state: 'local',
        fileSize: size
    });

    _this.loop.update(_this._dom.get().toJS());

    _this.sockets[index] = io.connect(_this.sockUrl, {
        'force new connection': true
    });

    // console.log(_this.socket);

    _this.sockets[index].on('disconnect', function(){

        // console.log('disconnect');
        
        var doc = _this._dom.get(),
            obj = {};


        if (doc.files[index].paused) {

            obj = {
                disconnect: true,
                error: true
            };

            _this.loop.update(_this._dom.get().set(obj).toJS());

        } else if (!doc.files[index].paused && !doc.files[index].finished) {

            _this._dom.get().set({
                error: true
            });

            _this._dom.get().files[index].set({
                paused: true
            });

            _this.loop.update(_this._dom.get().toJS());

        }

    });

    _this.sockets[index].on('connect', function(){
        
        // console.log('connect');

        var doc = _this._dom.get();

        if (doc.files[index].paused && doc.error && !doc.files[index].finished && !doc.disconnect) {

            _this._dom.get().files[index].remove('paused');

            _this._dom.get().files[index].set({
                socketId: this.id
            });

            _this._dom.get().remove('error');

            _this.loop.update(_this._dom.get().toJS());
            // console.log('conn')
            // 
            var SocketIndex = findIndex(_this.sockets, {
                id: this.id
            });

            _this.sockets[SocketIndex].emit('start', {
                'name' : _this.selectedFiles[index].name,
                'size' : _this.selectedFiles[index].size
            });

        } else {

            _this._dom.get().files[index].set({
                socketId: this.id
            });

            if (doc.error && doc.disconnect) {

                _this._dom.get().remove('error');

                _this._dom.get().remove('disconnect');

                _this.loop.update(_this._dom.get().toJS());
                // console.log('conn 2');

                return;

            } else if (!doc.files[index].paused && !doc.files[index].finished) {
                // console.log('conn 3')
                // 
                _this._dom.get().files[index].set({
                    socketId: this.id
                });

                var SocketIndex = findIndex(_this.sockets, {
                    id: this.id
                });

                _this.sockets[SocketIndex].emit('start', {
                    'name' : _this.selectedFiles[index].name,
                    'size' : _this.selectedFiles[index].size
                });

            }

        }

    });

    _this.sockets[index].on('error', function(){
        console.log('error');
    });

    _this.sockets[index].on('moreData', function(data){

        return _this._moreData.call(_this, data, index);

    });

    _this.sockets[index].on('setMeta', function(data){

        var obj = Convert(data);

        _this._dom.get().files[index].set(obj);

        _this.loop.update(_this._dom.get().toJS());

    });

    _this.sockets[index].on('cancelDone', function (data){

        index = findIndex(_this._dom.get().files, {
            vid: data.vid
        });
        
        var file = _this._dom.get().files[index],
            FileIndex = findIndex(_this.selectedFiles, {
                name: file.fileName
            }),
            SocketIndex = findIndex(_this.sockets, {
                id: file.socketId
            });

        _this._dom.get().files[index].remove('socketId');

        _this.selectedFiles.splice(FileIndex, 1);

        _this.sockets[SocketIndex].destroy();

        _this.sockets.splice(SocketIndex, 1);

        _this._dom.get().files.splice(index, 1);

        if (_this.posters[data.vid]) delete _this.posters[data.vid];

        _this.loop.update(_this._dom.get().toJS());

        if (!_this._dom.get().files.length) {

            _this.head.loop.update({
                title: 'Upload'
            });

        }

    });

    _this.sockets[index].on('done', function (data){

        var uploadsize = filesize(data['uploaded'], {
                output: 'string'
            }),
            file = _this._dom.get().files[index];

        _this._dom.get().files[index].remove('uploading');

        _this._dom.get().files[index].set({
            progress: Math.round(data['percent']*100)/100,
            uploaded: uploadsize,
            finished: true
        });

        if (data.meta) {

            var obj = Convert(data);

            _this._dom.get().files[index].set(obj);

        }

        _this.loop.update( _this._dom.get().toJS());

        _this.socket.on(data.vid + ':s3', function(){

            _this._dom.get().files[index].set({
                state: 's3'
            });

            _this.loop.update( _this._dom.get().toJS());

        })

        _this.socket.on(data.vid + ':transcode', function(data){

            _this._dom.get().files[index].set({
                state: 'live',
                live: true,
                savedIn: data.savedIn
            });

            _this.loop.update( _this._dom.get().toJS());
            
        });

        var FileIndex = findIndex(_this.selectedFiles, {
            name: file.fileName
        });

        var SocketIndex = findIndex(_this.sockets, {
            id: file.socketId
        });

        _this._dom.get().files[index].remove('socketId');

        _this.selectedFiles.splice(FileIndex, 1);

        _this.sockets[SocketIndex].destroy();

        _this.sockets.splice(SocketIndex, 1);

        if (_this.posters[data.vid]) delete _this.posters[data.vid];

        _this.head.loop.update({
            title: '(100%) Upload'
        });

        if (!_this._dom.get().files.length) {

            _this.head.loop.update({
                title: 'Upload'
            });

        }
        
    });
	
};

module.exports = StartUpload;