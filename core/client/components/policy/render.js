var _h = require('virtual-dom/h');

var Render = function(){

	var link = _h('a.bold', {href: '/'}, [String('kukuotv.com')]),
		contact = _h('a.bold', {
			href: 'mailto:contact@kukuotv.com'
		}, [String('contact@kukuotv.com')]);

	return _h('div.bc-vod-policy-div', [
		_h('div.bc-vod-container.bc-vod-container-center.bc-vod-margin-large-bottom', [
			_h('h1.text-center.bold.font-montserrat.bc-vod-margin-large-top', [String('Privacy Policy')]),
			_h('div.section-divider', [
				_h('hr')
			]),
			_h('div.bc-vod-grid.bc-vod-margin-top.bc-vod-margin-large-bottom', [
				_h('div.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-3-5.bc-vod-width-large-3-5', [
					_h('ol', [
						_h('li', [
							String('All User information supplied by Users of '),
							link,
							String(' as defined under this Terms & Conditions is covered by the Data Protection Act 2012, Ghana.')
						]),
						_h('li', [
							String('Users can amend information provided at any time by contacting '),
							contact,
							String('.')
						]),
						_h('li', [
							String('Users can access the data held on them by '),
							link,
							String(' by contacting us at '),
							contact,
							String('.')
						]),
						_h('li', [
							String('User information will not be disclosed to a third party without Users’ permission. User details will be used only to provide service(s) specifically requested by the User.')
						]),
						_h('li', [
							link,
							String(' prohibits registration by, and does not knowingly collect personal information from anyone under 18 years of age. Parental guidance is advised for persons under 18years.')
						]),
						_h('li', [
							String('Users’ details may be transferred to another company if ownership of Blue Canvas Technologies Limited or '),
							link,
							String(' changes.')
						]),
						_h('li', [
							String('The '),
							link,
							String(' website uses cookies. Users may choose not to accept cookies but this will prevent proper functioning of the services of the website.')
						]),
						_h('li', [
							String('Whenever a User requests a page, username, IP address, browser and version of operating system are stored in a log file. This information is only used for statistical purposes to help improve this site and provide accurate statistics to advertisers in the form of aggregated data.')
						]),
						_h('li', [
							String('Blue Canvas Technologies Limited cannot guarantee that third party services accessible through '), 
							link,
							String(' services operate the same Privacy Policy.')
						]),
						_h('li', [
							String('Blue Canvas Technologies Limited reserves the right to update and change this Privacy Policy at any time. However, Blue Canvas Technologies Limited will not use information submitted by a User under this current notice in a way not currently described herein without also notifying the User of such change, and giving the User an opportunity to opt-out, or otherwise prevent that new use of your information.')
						]),
						_h('li', [
							String('11.	If you have any questions concerning this Privacy Policy or would like us to amend or do away with your details please contact us at '),
							contact,
							String('.')
						])
					])
				])
			])
		])
	]);
	
};

module.exports = Render;