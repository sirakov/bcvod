var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	errors = {
		'404': require('./notFound')
	};

var HttpErrors = function(errorCode, prerender, content){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.error-page');

		obj.target = target;

	}

	_this.loop = mainLoop({}, errors[errorCode], obj);

	if (!prerender) content.appendChild(_this.loop.target);

	_this._EventListeners = [];
	
};

HttpErrors.prototype = {};

module.exports = HttpErrors;