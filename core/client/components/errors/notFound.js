var _h = require('virtual-dom/h');

var NotFound = function(){
	return _h('div.container-xs-height.full-height.error-page', [
		_h('div.row-xs-height', [
			_h('div.col-xs-height.col-middle', [
				_h('div.error-container.text-center.bc-vod-margin-large-top', [
					_h('h1.error-number',{
						style:{
							marginTop: '125px'
						}
					}, [String('404')]),
					_h('h2.semi-bold', [String('Sorry but we couldn\'t find this page')])
				])
			])
		])
	]);
};

module.exports = NotFound;