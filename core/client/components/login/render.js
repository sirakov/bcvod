var _h = require('virtual-dom/h');

var RenderLogin = function(obj){

	var body;

	function text(val){
		return 'An email has been sent to <strong>' + val +
				'</strong> with a link to login. The link expires after 4 minutes.';
	}

	if (obj.success) {

		body = [
			_h('p.success', {
				innerHTML: text(obj.success)
			})
		];

	} else {

		var error;

		if (obj.error) {

			error = _h('label.error', [String(obj.error)]);

		}

		body = [
			_h('a', {
				href: '/'
			}, [
				_h('img.bc-vod-margin-large-bottom', {
					src: obj.assetsUrl + obj._a['/images/logo-alt.png'] + '?' + obj.build,
					style:{
						    width: '100px',
						    margin: '0 auto',
						    display: 'block'
					}
				})
			]),
			_h('div.form-group', [,
				_h('label', [String('Email')]),
				_h('input.form-control.bc-vod-width-1-1', {
					attributes:{
						type: 'text',
						name: 'user',
						id: 'user',
						autocomplete: 'off',
						placeholder:'email',
						required: true
					}
				}),
				error
			]),
			_h('button.btn.btn-primary.bc-vod-width-1-1', {
				attributes:{
					disabled: (obj.disable) ? true : undefined
				}
			}, [
				String('Login')	
			])
		];

	}

	return _h('div.bc-vod-container-center.bc-vod-container.bc-vod-login', [
		_h('section.bc-vod-grid.bc-vod-grid-collapse', {
			style:{
				margin: '15% auto 0'
			}
		}, [
			_h('form.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-1-4.bc-vod-container-center', {
				attributes:{
					role: 'form',
					name: 'loginForm',
					action: '/admin/login',
					method: 'post'
				}
			}, body)
		])
	]);
};

module.exports = RenderLogin;