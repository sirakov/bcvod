var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Delegator = require('dom-delegate'),
	http = require('../http'),
	email = require('./email'),
	Render = require('./render');

var Login = function(dom, prerender, content, query){
	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-login');

		obj.target = target;

	}

	_this.loop = mainLoop(dom, Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	_this._fields = {};

	_this._fields.password = '';

	var input = _this.loop.target.querySelector('#user');

	setTimeout(function(){

		input.focus();

	}, 50);

	if (query.r) _this._fields.r = query.r;
	else _this._fields.r = '';

	var _BodyDelegator = new Delegator(document.body);

	_BodyDelegator.on('submit', '.bc-vod-login', function(e){
		
		e.preventDefault();

		dom.disable = true;

		_this.loop.update(dom);

		http({
			url: '/admin/login',
			method: 'post',
			data: _this._fields
		}).then(function(res){

			if (res.data.success) {

				dom.success = _this._fields.user;

				_this.loop.update(dom);

			}

		}).catch(function(err){

			if (err.status === 401) {

				dom.error = 'Invalid user';
				
				_this.loop.update(dom);

			} else {

				dom.error = 'An error occurred while trying to validate user.';

				_this.loop.update(dom);

			}

		});

		return false;
	});

	_BodyDelegator.on('input', '.bc-vod-login input', function(e){
		
		e.preventDefault();

		_this._fields[e.target.id] = e.target.value;

		if (!email.test(_this._fields.user)) {

			dom.disable = true;

			_this.loop.update(dom);

		} else {

			dom.disable = false;

			_this.loop.update(dom);			

		}

		return false;
	});

	_this._EventListeners = [_BodyDelegator];
	
};

Login.prototype = {};

module.exports = Login;