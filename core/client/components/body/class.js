var slug = require('mout/string/slugify');

var BodyClass = function(ua){

	var classes = [
		slug((ua && ua.name) ? ua.name.split(' ')[0].toLowerCase() : ''),
		slug((ua && ua.os && ua.os.family) ? ua.os.family.split(' ')[0].toLowerCase() : '')
	];

	return classes.join(' ');
};

module.exports = BodyClass;