var _h = require('virtual-dom/h'),
	toString = require('vdom-to-html');

var Javascript = function(src, attrs){

	if (typeof src === 'object') {

		return toString(_h('script', {
			attributes: {
				'type': 'text/javascript'
			},
			innerHTML: 'var GLOBALS = '+ JSON.stringify(src) +';'
		}));

	}

	var dom = GLOBALS.get();

	if (attrs) {

		return toString(_h('script', {
			attributes: attrs
		}));

	}

	return toString(_h('script', {
		attributes: {
			type: 'text/javascript',
			async: true,
			src: dom.assetsUrl + dom._a[src] + '?' + dom.build
		}
	}));
};

module.exports = Javascript;