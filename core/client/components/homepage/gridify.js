var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	forEach = require('fast.js/array/forEach'),
	reduce = require('fast.js/array/reduce'),
    aspectRatio = require('../utils/aspectRatio'),
    _subtractWidth = require('../utils/subtractWidth'),
    increaseAspect = require('../utils/increaseAspect'),
    decreaseAspect = require('../utils/decreaseAspect'),
    freeze = require('freezer-js');

var Gridify = function(videos, state){

	if (!videos.length) {

		return [
			_h('div.bc-vod-no-videos-found',[
				_h('h2.bold.text-center', [String('No Videos found')])
			])
		];

	}

	state.defaultPoster = new freeze(state.defaultPoster);

	var sum = reduce(videos, function(a, b){

		var poster;
	
		if (b.poster) poster = JSON.parse(b.poster);
		else poster = state.defaultPoster.get().toJS();

        var ar = aspectRatio(poster.width, poster.height);

        return a + ar.width;

    }, 0),
    ratio = reduce(videos, function(a, b){

    	var poster;

		if (b.poster) poster = JSON.parse(b.poster);
		else poster = state.defaultPoster.get().toJS();

        var ar = aspectRatio(poster.width, poster.height);

        return a + (ar.width / ar.height);

    }, 0),
    minH = sum / ratio,
    ImgSumW = 0;

    forEach(videos, function(video){

    	if (video.poster) video.poster = JSON.parse(video.poster);
    	else video.poster = state.defaultPoster.get().toJS();
        
        var ar = aspectRatio(video.poster.width, video.poster.height),
            width = ar.width,
            height = ar.height,
            minP = ((minH / ar.height) * 100);

        if (minP > 100) {
            
            var widthW = increaseAspect(width, height, (minP - 100));
            
            ImgSumW += widthW.width;
            
            video.poster.height = widthW.height;
            video.poster.width = widthW.width;

        } else if (minP < 100) {

            var widthW = decreaseAspect(width, height, (100 - minP));
            
            ImgSumW += widthW.width;
            
            video.poster.height = widthW.height;
            video.poster.width = widthW.width;

        } else {

            ImgSumW += width;

            video.poster.width = width;

        }

        video.poster = JSON.stringify(video.poster);

    });

	state.defaultPoster = state.defaultPoster.get().toJS();

	return map(videos, function(video){

		if (!video.title) video.title = 'Untitled';

		var image;

		if (video.poster) {

			var poster = JSON.parse(video.poster);

			if (videos.length > 1) {

				var width = (poster.width / ImgSumW) * 100,
                    ra = aspectRatio(((width - _subtractWidth()) / 100) * ImgSumW, minH);

            } else if (videos.length === 1) {

            	var width = 20,
                    ra = aspectRatio(((width - _subtractWidth()) / 100) * ImgSumW, minH / 5);

            }

			image = _h('img.bc-vod-overlay-scale.bc-vod-video-poster', {
				attributes:{
					alt: video.title,
					'data-echo': state.imgsUrl + '/' + poster.src
				}
			});

		}

		var figcap,
			fig,
			FigCaption;

		if (poster.src !== 'default.jpg' && !video.type) {

			figcap = '.bc-vod-overlay-slide-bottom';
			fig = '.bc-vod-overlay-hover';

		} else {

			figcap = '';
			fig = '';

		}

		if (!video.type) {

			FigCaption = _h('figcaption.bold.bc-vod-text-truncate.bc-vod-overlay-panel.bc-vod-overlay-background.bc-vod-overlay-bottom' + figcap, [
				String((video.type) ? video.type : video.title)
			]);

		} else {

			FigCaption = _h('figcaption.bc-vod-text-truncate.bc-vod-overlay-panel.bc-vod-overlay-background.bc-vod-flex.bc-vod-flex-center.bc-vod-flex-middle.bc-vod-text-center', [
				_h('div', [
					_h('h2.bold', [String((video.type) ? video.type : video.title)])
				])
			]);

		}

		var category = (video.category) ? video.category.replace(video.savedIn, '').replace('/', '') : null;

		return _h('figure.bc-vod-video-figure.bc-vod-overlay' + fig, {
				attributes:{
					'data-width': poster.width,
					'data-height': poster.height
				},
			    style:{
				    width: (width - _subtractWidth()) + '%'
			    }
		}, [
			_h('div.bc-vod-video-cover', [
				_h('div.ratioHolder', {
					style: {
						padding: '0px 0px ' + ra.ratio +  '% 0px'
					}
				}),
				image
			]),
			FigCaption,
			_h('a.bc-vod-position-cover',{
				href: (category && video.type) ? '/category/' + category + '?genres=' + video.type :'/v/' + video.vid,
				attributes:{
					title: video.title
				}
			})
		]);


	});
	
};

module.exports = Gridify;