var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	warna = require('warna'),
	gridify = require('./gridify'),
	capitalize = require('../header/capitalize'),
	freeze = require('freezer-js'),
	chunk = function(arr, chunkSize) {
	    var groups = [], i;
	    for (i = 0; i < arr.length; i += chunkSize) {
	        groups.push(arr.slice(i, i + chunkSize));
	    }
	    return groups;
	};

var RenderHomePage = function(state){

	var vids = new freeze(state.videos),
		grid = state.videos.slice(3, state.videos.length),
		slider = state.videos.slice(0, 3),
		layouts = [
			{
				type: 'recently',
				videos: vids.get().toJS(),
				hidden: true
			},{
				type: 'recently',
				videos: grid
			},{
				type: 'popular',
				videos: state.popular
			}, {
				type: 'genres',
				videos: state.genres
			}
		];

	layouts = map(layouts, function(layout){

		var chunks = chunk(layout.videos, 5);

		var section = map(chunks, function(chuk){

			var videos = gridify(chuk, state);

			return _h('section.bc-vod-video-section.clearfix', {
				attributes:{
					'data-video-count': chuk.length
				}
			}, videos);

		});

		var title = (layout.type === 'recently') ? 'Recently Added': capitalize(layout.type),
			List = [
				'bc-vod-section-' + layout.type
			];

		if (layout.hidden && layout.type === 'recently') List.push('bc-vod-visible-small');
		else if (layout.type === 'recently') List.push('bc-vod-hidden-small');

		return _h('div', {
			className: List.join(' '),
			style:{
				display: (layout.videos.length) ? 'block': 'none'
			}
		}, [
			_h('h2.title.text-center.bold.bc-vod-margin-large-top.font-montserrat', [String(title)]),
			_h('div.section-divider', [
				_h('hr')
			]),
			_h('div.videos', section)
		]);

	});

	var LoadMore;

	if (state.last && state.videos.length >= 17) {

		LoadMore = _h('button.ladda-button.btn.btn-primary.bc-vod-container-center.bc-vod-display-block.bc-vod-margin-large', {
			attributes:{
				'data-action': 'load-more',
				'data-style': 'expand-left',
				'data-spinner-color': '#fff'
			}
		}, [
			_h('span.ladda-label', [String('Load More')]),
			_h('span.ladda-spinner'),
			_h('div.ladda-progress', {
				style:{
					width: '0px'
				}
			})
		]);

	}

	return _h('div.bc-vod-homepage-slider.bc-vod-margin-remove', [
		_h('div.slider.js_slider.bc-vod-hidden-small', [
			_h('div.frame.js_frame', [
				_h('ul.slides.js_slides.clearfix', map(slider, function(video){

					if (!video.poster) {

						video.poster = JSON.stringify(state.defaultPoster);

						video.defaultPoster = true;

					}

					var poster = JSON.parse(video.poster);

					var Poster = _h('img.bc-vod-video-poster-img', {
							attributes:{
								alt: video.title,
								'data-echo': state.imgsUrl + '/' + poster.src + '?h=407&auto=compress'
							}
						});

					var BG;

					if (poster && poster.src && !video.defaultPoster) {

						BG = _h('div.bc-vod-video-card', {
							attributes:{
								'data-echo-background': state.imgsUrl + '/' + poster.src + '?auto=compress&blur=60'
							}
						});

					}

					var description = video.description || 'No description yet.';

					return _h('li.js_slide.clearfix', [
						_h('div.bc-vod-video-color', {
							style:{
								backgroundColor: (poster && poster.color) ? warna.darken(poster.color, 0.5).hex : '#000'
							}
						}),
						BG,
						_h('div.bc-vod-container.bc-vod-container-center.bc-vod-margin-large-top', [
							_h('div.bc-vod-grid.bc-vod-grid-small.bc-vod-margin-large-top', [
								_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-3-6.bc-vod-width-large-2-5', [
									_h('div.bc-vod-video-poster-home', [
										_h('a', {
											href: '/v/' + video.vid
										}, [
											Poster
										])
									])
								]),
								_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-3-6.bc-vod-width-large-2-4.bc-vod-hidden-small.bc-vod-margin-large-top', [
									_h('h1.bold', [
										_h('a', {
											href: '/v/' + video.vid
										}, [
											String(video.title)
										])
									]),
									_h('p.bc-vod-video-description-home', [String(description)]),
									_h('a.btn.btn-primary.btn-rounded.btn-lg.bc-vod-margin-top', {
										href: '/v/' + video.vid
									}, [
										String('Watch')
									])
								])
							])
						])
					]);
				})),
				_h('div.slider-navs.bc-vod-visible-large', [
					_h('a.prev_arrow.slider_arrow.js_prev', {
						href: 'javascript:void(0)'
					}, [
						_h('span')
					]),
					_h('a.next_arrow.slider_arrow.js_next', {
						href: 'javascript:void(0)'
					}, [
						_h('span')
					])
				])
			])
		]),
		_h('div.bc-vod-container.bc-vod-container-center.bc-vod-margin-large-bottom.bc-vod-margin-large-top', layouts),
		LoadMore
	]);

	
};

module.exports = RenderHomePage;