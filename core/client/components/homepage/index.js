var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	pick = require('mout/object/pick'),
	Freezer = require('freezer-js'),
	Lory = require('lory.js').lory,
	$ = require('sprint-js'),
	echo = require('echo-js');

var HomePage = function(dom, prerender, content){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('div.bc-vod-homepage-slider');

		obj.target = target;

	}

	_this._dom = new Freezer(pick(dom, ['videos', 'assetsUrl', 'imgsUrl', 'defaultPoster', 'last', 'popular', 'genres']));

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);
	
	_this.$ = $;

	if (!prerender) content.appendChild(_this.loop.target);

	_this.echo = echo(window);

	_this.echo.init({
		offset: 100,
		throttle: 250,
		unload: false
	});

	_this.loop.target.addEventListener('before.lory.slide', function(c, n){

		clearInterval(_this._Timer);

		setTimeout(function(){

			_this.echo.render();

		}, 150);

	});

    _this.loop.target.addEventListener('after.lory.slide', function(c){

    	_this._Timer = setInterval(function () {
		
			_this._slider.next();

			setTimeout(function(){

				_this.echo.render();

			}, 150);

		}, 10000 + 300);

    });

	_this._slider = Lory(_this.loop.target, {
		enableMouseEvents: true,
		infinite: 1
	});
	
	_this._Timer = setInterval(function () {
		
		_this._slider.next();

		setTimeout(function(){

			_this.echo.render();

		}, 150);


	}, 10000 + 300);

	_this.echo.render();

	_this._EventListeners = [];

	_this._initEvents();
};

HomePage.prototype = {
	_initEvents: require('./events')
};

module.exports = HomePage;