module.exports = function(file, obj) {
    var formData;
    
    formData = new FormData();
    
    if (obj) {
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                var value = obj[key];
                formData.append(key, value);
            }
        }
    }

    formData.append('file', file);
    
    return formData;
};
