module.exports = {
    set: function(id){
        
        var split = id.split('-');

        return {
            savedIn: (split.length === 3) ? '/' + split[0] + '/' + split[1] : null,
            vid: (split.length === 3) ? split[2] : null
        };

    },
    get: function(id){
        
        var Id = id.savedIn.split('/');

        Id.shift();

        Id.push(id.vid || id.uid);

        return Id.join('-').replace('draft-', '');

    }
};