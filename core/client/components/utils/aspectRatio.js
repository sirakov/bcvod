module.exports = function(w, h, vary) {
    var fill_ratio, height, maxHeight, maxWidth, ratio, result, width;
    maxWidth = 1240;

    if (vary) maxWidth = 650;

    maxHeight = 400;
    ratio = 0;
    width = w;
    height = h;
    if (width > maxWidth) {
        ratio = maxWidth / width;
        height = height * ratio;
        width = width * ratio;
    } else if (height > maxHeight) {
        ratio = maxHeight / height;
        width = width * ratio;
        height = height * ratio;
    }
    fill_ratio = (height / width) * 100;
    result = {
        width: width,
        height: height,
        ratio: fill_ratio
    };
    // utils.log(result);
    return result;
};
