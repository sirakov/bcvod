module.exports = function(w, h, p){
	var height, width, ratio, result;
	width = ((p + 100) / 100) * w;
	height = ((p + 100) / 100) * h;
	// console.log(p);
	result = {
		width: width,
		height: height,
		ratio: (height / width) * 100
	};
	return result;
};