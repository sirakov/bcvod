module.exports = function(w, h, p){
	var height, width, ratio, result;
	width = ((100 - p) / 100) * w;
	height = ((100 - p) / 100) * h;
	// console.log(p);
	result = {
		width: width,
		height: height,
		ratio: (height / width) * 100
	};
	return result;
};