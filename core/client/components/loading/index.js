var Loading = function(){

	var _this = this;

	_this._body = document.body;

	_this._loadingClass = 'isLoading';
	
};

Loading.prototype = {
	isLoading: function(){

		var _this = this;

		return _this._body.classList.contains(_this._loadingClass);

	},
	stop: function(){

		var _this = this;


		if (!_this._body.classList.contains(_this._loadingClass)) return true;
		else _this._body.classList.remove(_this._loadingClass);

		return true;
	},
	start: function(){

		var _this = this;


		if (_this._body.classList.contains(_this._loadingClass)) return true;
		else _this._body.classList.add(_this._loadingClass);

		return true;

	}
	
};


module.exports = new Loading();