var _h = require('virtual-dom/h'),
	overlayers = {
		'delete' : require('./overlays/delete'),
		'noconn': require('./overlays/noconn'),
		'login': require('./overlays/login')
	};

var RenderOverlay = function(data){

	var content = overlayers[data.type](data),
		List;

	if (content) List = _h('div.modal-content', content);

	return _h('div.modal.fill-in.fade.in', {
		attributes:{
			tabindex: -1
		},
		style: {
			display: 'block'
		}
	}, [
		_h('button.close', {
			attributes:{
				'data-action': 'close',
				type: 'button'
			}
		}, [
			_h('i.ti-close')
		]),
		_h('div.modal-dialog', [
			List
		])
	]);
	
};

module.exports = RenderOverlay;