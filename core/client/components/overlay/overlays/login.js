var _h = require('virtual-dom/h'),
	footer = require('../../footer/render');

var Login = function(data){

	function text(val){
		return 'An email has been sent to <strong>' + val +
				'</strong> with a link to login. The link expires after 4 minutes.';
	}

	var Email;

	if (data.success) {

		Email = _h('p.success', {
			style:{
			    width: '230px',
			    margin: '0 auto'
			},
			innerHTML: text(data.user)
		});

	} else if (data.isEmail) {

		var error;

		if (data.error) {

			error = _h('label.error', [String(data.error)]);

		}

		Email = _h('form.login-form', {
			style:{
			    width: '230px',
			    margin: '0 auto'
			}
		}, [
			_h('div.form-group', [,
				_h('label', [String('Email')]),
				_h('input.form-control.bc-vod-width-1-1', {
					attributes:{
						type: 'text',
						name: 'user',
						id: 'user',
						autocomplete: 'off',
						placeholder:'email',
						required: true
					}
				}),
				error
			]),
			_h('button.bc-vod-margin-top.btn.btn-rounded.btn-primary', {
				attributes:{
					disabled: data.disabled,
					'data-action': 'email-login'
				}
			}, [String('Login')])
		]);

	} else {

		Email = _h('ul.bc-vod-list.login-list', [
			_h('li', [
				_h('a.btn', {
					href: '/login/facebook?r=' + data.path,
					attributes:{
						'data-action': 'sign-in-facebook',
						rel: 'external'
					}
				}, [
					_h('i.ti-facebook.bc-vod-vertical-align-middle'),
					String('Sign in with facebook')
				])
			]),
			_h('li', [
				_h('a.btn', {
					href: '/login/twitter?r=' + data.path,
					attributes:{
						'data-action': 'sign-in-twitter',
						rel: 'external'
					}
				}, [
					_h('i.ti-twitter-alt.bc-vod-vertical-align-middle'),
					String('Sign in with twitter')
				])
			]),
			_h('li.bc-vod-margin-top', [
				_h('a.bold', {
					attributes:{
						'data-action': 'email'
					}
				}, [
					String('Sign in with email')
				])
			])
		]);

	}
	
	return _h('div.modal-body.bc-vod-login-view', [
		_h('div.logo-alt', [
			_h('a', {
				href: '/'
			}, [
				_h('img', {
					src: data.assetsUrl + data._a['/images/logo-alt.png'] + '?' + data.build
				})
			])
		]),
		_h('p.text-center.p-b-4.description', [
			String('Sign in or create an account')
		]),
		Email,
		footer(data)
	]);

};


module.exports = Login;