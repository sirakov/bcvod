var _h = require('virtual-dom/h');

var DeleteOverlay = function(){

	return _h('div.modal-body.text-center', [
		_h('h3.text-center.p-b-10', [String('Deleted videos can\'t be restored.')]),
		_h('button.btn.btn-rounded.btn-success.bc-vod-margin-small-right', {
			attributes:{
				'data-action': 'delete-post'
			}
		}, [String('Delete')]),
		_h('button.btn.btn-default.btn-rounded', {
			attributes:{
				'data-action': 'close'
			}
		}, [String('Cancel')])
	]);
	
};

module.exports = DeleteOverlay;