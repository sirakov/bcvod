var _h = require('virtual-dom/h');

var NoConn = function(){
	
	return _h('div.modal-header', [
		_h('h4.text-center.p-b-4', [
			String('Oops! there is a problem with your connection please try again')
		])
	]);

};


module.exports = NoConn;