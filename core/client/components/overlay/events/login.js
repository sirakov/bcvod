var email = require('../../login/email'),
	http = require('axios');

var Login = function(evts){

	var _this = this;

	_this._eventHandler.on('click', 'a[data-action="email"]', function(e){

		_this._dom.get().set('isEmail', true);
		_this._dom.get().set('disabled', true);

		_this.loop.update(_this._dom.get().toJS());

		setTimeout(function(){

			var input = _this.loop.target.querySelector('#user');

			setTimeout(function(){

				input.focus();

			}, 50);

		}, 50);

		return;

	});

	_this._eventHandler.on('input', 'input#user', function(e){
		
		e.preventDefault();

		_this._dom.get().set('user', e.target.value);

		if (!email.test(_this._dom.get().user)) {

			_this._dom.get().set('disabled', true);

			_this.loop.update(_this._dom.get().toJS());

		} else {

			_this._dom.get().remove('disabled');

			_this.loop.update(_this._dom.get().toJS());		

		}

		return false;
	});


	_this._eventHandler.on('submit', 'form.login-form', function(e){
		
		e.preventDefault();

		_this._dom.get().set('disabled', true);

		_this.loop.update(_this._dom.get().toJS());

		http({
			url: '/login',
			method: 'post',
			data: {
				user: _this._dom.get().user,
				r: _this._dom.get().path
			},
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			},
			xsrfCookieName: 'xsrf',
			xsrfHeaderName: 'X-XSRF-TOKEN'
		}).then(function(res){

			if (res.data.success) {

				_this._dom.get().remove('disabled');

				_this._dom.get().set('success', true);

				_this.loop.update(_this._dom.get().toJS());

			}

		}).catch(function(err){

			if (err.status === 401) {

				_this._dom.get().remove('disabled');

				_this._dom.get().set(error, 'Invalid user');
				
				_this.loop.update(_this._dom.get().toJS());

			} else {

				_this._dom.get().set(error, 'An error occurred while trying to validate user.');

				_this.loop.update(_this._dom.get().toJS());

			}

		});

		return false;
		
	});


	return _this._eventHandler;
	
};

module.exports = Login;