var Defaults = function(){

	var _this = this;

	_this._eventHandler.on('click', 'button[data-action="close"]', function(e){

		return _this._destroy();

	}, true);


	_this._eventHandler.on('keydown', function(e){

		if (e.which === 27) return _this._destroy();

	});

	_this._eventHandler.on('click', function(e){

		var target = e.target,
			selector = '.modal-content';

		if (!target.classList.contains('modal-content') && !_this.$(target).parents(selector).length) {

			return _this._destroy();

		}

	});


	return _this._eventHandler;
};

module.exports = Defaults;