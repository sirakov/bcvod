var OverlayEvent = function(evts) {

	var _this = this;

	_this._eventHandler.on('click', '[data-action]', function(e){

		var data = e.target.dataset;

		if (data.action === 'delete-post') return evts.delete();

	});


	return _this._eventHandler;

};

module.exports = OverlayEvent;