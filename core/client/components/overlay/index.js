var Delegator = require('dom-delegate');

var Overlay = function(){

	var _this = this;

	_this.$ = require('sprint-js');

	_this._eventHandler = new Delegator();

	_this.events = {
		'defaults': require('./events/defaults'),
		'delete': require('./events/delete'),
		'noconn': require('./events/noconn'),
		'login': require('./events/login')
	};

	return _this;
	
};

Overlay.prototype = {
	add: require('./add'),
	_destroy: require('./destroy')
};



// change view/virtual-dom
// apply events

module.exports = new Overlay();