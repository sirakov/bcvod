var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js');


var AddOverlay = function(data, evts){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (_this._current) _this._destroy();

	_this.loop = mainLoop(data, Render, obj);

	_this.events['defaults'].call(_this);

	_this._eventHandler.root(_this.loop.target);

	_this._current = data.type;

	_this._dom = new Freezer(data);

	document.body.appendChild(_this.loop.target);

	_this.events[data.type].call(_this, evts);

	return _this;
	
};

module.exports = AddOverlay;