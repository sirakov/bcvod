var Destroy = function(){
	
	var _this = this;

	_this._eventHandler.destroy();

	_this.$(_this.loop.target).remove();

	delete _this.loop;
	delete _this._current;

};

module.exports = Destroy;