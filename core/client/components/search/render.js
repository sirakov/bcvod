var _h = require('virtual-dom/h'),
	gridify = require('../homepage/gridify'),
	footer = require('../footer/render'),
	map = require('fast.js/array/map'),
	chunk = function(arr, chunkSize) {
	    var groups = [], i;
	    for (i = 0; i < arr.length; i += chunkSize) {
	        groups.push(arr.slice(i, i + chunkSize));
	    }
	    return groups;
	};

var Render = function(state){

	var LoadMore,
		Videos,
		chunks = chunk(state.results || [], 4),
		layouts;

	layouts = map(chunks, function(chunk){

		var videos = gridify(chunk, state);

		return _h('section.bc-vod-video-section.clearfix', {
			attributes:{
				'data-video-count': chunk.length
			}
		}, videos);

	});



	if (state.last) {

		LoadMore = _h('button.ladda-button.btn.btn-primary.bc-vod-container-center.bc-vod-display-block.bc-vod-margin-large', {
			attributes:{
				'data-action': 'load-more',
				'data-style': 'expand-left',
				'data-spinner-color': '#fff'
			}
		}, [
			_h('span.ladda-label', [String('Load More')]),
			_h('span.ladda-spinner'),
			_h('div.ladda-progress', {
				style:{
					width: '0px'
				}
			})
		]);

	}

	if (state.results && state.results.length) {

		Videos = _h('div.bc-vod-width-1-1.bc-vod-container-center', [
			_h('h1.title.bold.text-center', [String('Results')]),
			_h('div.section-divider', [
				_h('hr')
			]),
			_h('div.videos', layouts),
			footer(state)
		]);

	} else if (state.filters.q) {

		Videos = _h('div.bc-vod-width-1-1.bc-vod-container-center', [
			_h('div.bc-vod-grid', [
				_h('div.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-4', [
					_h('h5.bold.text-center.bc-vod-margin-large-top.bc-vod-margin-large-bottom', [String('No Videos Found')])
				])
			])
		]);

	}

	return _h('div.bc-vod-search-page.bc-vod-container.bc-vod-container-center', [
		_h('div.bc-vod-grid', [
			_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-1-1.bc-vod-width-large-1-1.bc-vod-container-center', [
				_h('form.search-form.bc-vod-hidden-small', {
					style:{
						width: '100%',
						marginTop: '75px'
					},
					attributes:{
						'action': '/search'
					}
				}, [
					_h('input.form-control.bc-vod-display-inline-block', {
						style:{
						    width: '85%',
						    fontSize: '35px',
					        height: '70px',
						    lineHeight: 1.3,
						    padding: '0 10px'
						},
						onfocus: function(e){
							this.value = state.filters.q || '';
						},
						value: state.filters.q || '',
						attributes:{
							type: 'search',
							name: 'q',
							placeholder: 'Search '+ state.brandName
						}
					}),
					_h('button.btn.btn-primary.bc-vod-margin-left', {
						style:{
							width: '11%',
							height: '70px'
						},
						attributes:{
							type: 'submit'
						}
					}, [_h('i.ti-search')])
				]),
				_h('form.bc-vod-visible-small', {
					attributes:{
						'action': '/search'
					}
				}, [
					_h('input.form-control', {
						style:{
						    width: '100%',
						    marginTop: '75px',
						    fontSize: '20px',
					        height: '50px',
						    lineHeight: 1,
						    padding: '0 10px'
						},
						onfocus: function(e){
							this.value = state.filters.q || '';
						},
						value: state.filters.q || '',
						attributes:{
							type: 'search',
							name: 'q',
							placeholder: 'Search '+ state.brandName
						}
					}),
					_h('button.btn.btn-primary.bc-vod-margin-top.bc-vod-visible-small', {
						attributes:{
							type: 'submit'
						}
					}, ['Search'])
				])
			])
		]),
		_h('div.bc-vod-grid.bc-vod-margin-large-top', {
			style:{
				opacity: (state.isLoading) ? 0.3: undefined
			}
		}, [
			Videos
		]),
		LoadMore
	]);
};

module.exports = Render;