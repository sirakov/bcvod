var Delegator = require('dom-delegate'),
	http = require('../http'),
	qs = require('mout/queryString/encode'),
	merge = require('mout/object/merge'),
	page = require('page.js'),
	ladda = require('ladda'),
	$ = require('sprint-js'),
	serialize = require('form-serialize');

var Events = function(){
	
	var _this = this;

	var _BodyDelegator = new Delegator(document.body);

	_BodyDelegator.on('click', 'button[data-action="load-more"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: $(e.target).parents('button[data-action="load-more"]').dom[0],
			l = ladda.create(el),
			dom = _this._dom.get().toJS(),
			lastAndFilters = merge(dom.last || {}, dom.filters);

		_this._dom.get().set('isLoading', true);

		_this.loop.update(_this._dom.get().toJS());

		l.start();

		http({
			url: '/search' + qs(lastAndFilters),
			method: 'get'
		}, function(err, res){

			if (err) {

				l.stop();
				l.remove();

				_this._dom.get().remove('isLoading');

				_this.loop.update(_this._dom.get().toJS());

				return;

			}
			
			var currentVideos = _this._dom.get().results.toJS(),
				results = res.data.results;

			if (res.data.last) _this._dom.get().last.set(res.data.last);
			else _this._dom.get().remove('last');

			_this._dom.get().results.set(currentVideos.concat(results));

			l.stop();
			l.remove();

			_this._dom.get().remove('isLoading');

			_this.loop.update(_this._dom.get().toJS());

			setTimeout(function(){

				_this.echo.render();

			}, 100);

		});

	});
	
	_BodyDelegator.on('submit', '.search-form', function(e){

		e.preventDefault();

		var hash = serialize(e.target, {hash: true});

		_this._dom.get().set('isLoading', true);

		_this._dom.get().filters.set('q', hash.q);

		_this.loop.update(_this._dom.get().toJS());

		var dom = _this._dom.get().toJS();

		http({
			url: '/search' + qs(dom.filters),
			method: 'get'
		}, function(err, res){

			if (err) {

				_this._dom.get().remove('isLoading');

				_this.loop.update(_this._dom.get().toJS());

				return;

			}

			var ctx = new page.Context('/search' + qs(dom.filters), {
				isOld: true
			});

			ctx.save();
			
			var results = res.data.results;

			if (res.data.last) _this._dom.get().last.set(res.data.last);
			else _this._dom.get().remove('last');

			_this._dom.get().set('results', results);

			_this._dom.get().remove('isLoading');

			_this.loop.update(_this._dom.get().toJS());

			setTimeout(function(){

				_this.echo.render();

			}, 100);

		});


	});

	_this._EventListeners = [_BodyDelegator];
	
};

module.exports = Events;