var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js'),
	echo = require('echo-js');

var Search = function(dom, prerender, content){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-search-page');

		obj.target = target;

	}

	_this._dom = new Freezer(dom);

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	setTimeout(function(){

		var search = _this.loop.target.querySelector('input[type="search"]');

		if (search) {

			search.onfocus = function(e){

				this.value = _this._dom.get().toJS().filters.q || '';

			};

			setTimeout(function(){

				search.focus();

			}, 50);

		}

	}, 50);

	_this.echo = echo(window);

	_this.echo.init({
		offset: 100,
		throttle: 250,
		unload: false
	});

	_this._EventListeners = [];

	_this._initEvents();
	
};

Search.prototype = {
	_initEvents: require('./events')
};

module.exports = Search;