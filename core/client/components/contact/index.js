var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js'),
	$ = require('sprint-js');

var Contact = function(dom, prerender, content){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('div.bc-vod-contact-page');

		obj.target = target;

	}

	_this._dom = dom;

	_this.loop = mainLoop(_this._dom, Render, obj);
	
	_this.$ = $;

	if (!prerender) content.appendChild(_this.loop.target);
	
	_this._EventListeners = [];
	
};

Contact.prototype = {};

module.exports = Contact;