var _h = require('virtual-dom/h');

var Render = function(state){

	var Content;

	if (state.success) {

		Content = _h('div.bc-vod-grid.bc-vod-grid-large.bc-vod-margin-large-top', [
			_h('div.bc-vod-width-2-5.bc-vod-container-center', [
				_h('div.bc-vod-alert.bc-vod-alert-success', [
					String('Thank you for contacting us. We will be in touch with you very soon.'),
				])
			])
		]);

	} else if (state.error) {

		Content = _h('div.bc-vod-grid.bc-vod-grid-large.bc-vod-margin-large-top', [
			_h('div.bc-vod-width-2-5.bc-vod-container-center', [
				_h('div.bc-vod-alert.bc-vod-alert-danger', [
					String('Your email address is invalid'),
				])
			])
		]);

	}

	return _h('div.bc-vod-contact-page', [
		_h('div.bc-vod-container.bc-vod-container-center', [
			Content,
			_h('div.bc-vod-grid', {
				style:{
					display: (state.success) ? 'none' : undefined
				}
			}, [
				_h('div.bc-vod-width-1-1', [
					_h('h1.bold.bc-vod-margin-large-top.text-center.font-montserrat', [
						String('Send us an email')
					]),
					_h('div.section-divider', [
						_h('hr')
					])
				])
			]),
			_h('div.bc-vod-grid', {
				style:{
					display: (state.success) ? 'none' : undefined
				}
			}, [
				_h('div.bc-vod-margin-large-bottom.bc-vod-width-small-1-1.bc-vod-width-medium-1-4.bc-vod-width-large-1-4', [
					_h('p', [
						String('KukuoTv is an ad supported free view video on demand streaming service that seeks to promote and deliver primarily premium African entertainment.')
					]),
					_h('p', [
						String('KukuoTv Service is a product of Blue Canvas Technologies Limited. For enquires and advertising, you can find us at:')
					]),
					_h('p.bold', [
						String('Blue Canvas Technologies Ltd. '),
						_h('br'),
						String('M23 African Bungalow, Mbrom, '),
						_h('br'),
						String('Kumasi-Ghana.')
					]),
					_h('p.text-center', [String('OR')]),
					_h('p.bold', [
						String('Blue Canvas Technologies Ltd.'),
						_h('br'),
						String('P.O. Box S.E. 398'),
						_h('br'),
						String('Suame-Kumasi'),
						_h('br'),
						String('Ghana.')
					])
				]),
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-3-4.bc-vod-width-large-3-4', [
					_h('form.contact-form', {
						attributes:{
							'role': 'form',
							'method': 'post'
						}
					}, [
						_h('input.form-control', {
							attributes:{
								type: 'hidden',
								name: 'xsrf'
							},
							value: state.xsrf
						}),
						_h('div.row.clearfix', [
							_h('div.col-sm-6', [
								_h('div.form-group.form-group-default.required', [
									_h('label', [String('Name')]),
									_h('input.form-control', {
										attributes:{
											type: 'text',
											name: 'name',
											required: true
										},
										value: (state.name) ? state.name:  ''
									})
								])
							]),
							_h('div.col-sm-6', [
								_h('div.form-group.form-group-default.required', [
									_h('label', [String('Email')]),
									_h('input.form-control', {
										attributes:{
											type: 'text',
											name: 'email'
										},
										value: (state.email) ? state.email:  ''
									})
								])
							])
						]),
						_h('div.row', [
							_h('div.col-sm-12', [
								_h('div.form-group.form-group-default.required', [
									_h('label', [String('Subject')]),
									_h('input.form-control', {
										attributes:{
											type: 'text',
											required: true,
											name: 'subject'
										},
										value: (state.subject) ? state.subject:  ''
									})
								])
							])
						]),
						_h('div.row', [
							_h('div.col-sm-12', [
								_h('div.form-group.form-group-default.required', [
									_h('label', [String('Message')]),
									_h('textarea.form-control', {
										style:{
											height: '200px'
										},
										attributes:{
											required: true,
											name: 'message',
											rows: 6,
											cols: 30
										}
									}, [String((state.message) ? state.message:  '')])
								])
							])
						]),
						_h('button.btn.btn-primary', {
							attributes:{
								type: 'submit'
							}
						}, [String('Send Message')])
					])
				])
			])
		])
	])
};

module.exports = Render;