var Delegator = require('dom-delegate'),
	http = require('../http'),
	ladda = require('ladda');

var Events = function(){
	
	var _this = this;

	var _BodyDelegator = new Delegator(document.body);
	
	_BodyDelegator.on('click', 'button[data-action="load-more"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="load-more"]').dom[0],
			l = ladda.create(el),
			dom = _this._dom.get(),
			path = '/category/' + dom.name.toLowerCase();

		l.start();

	});

	_this._EventListeners = [_BodyDelegator];
	
};

module.exports = Events;