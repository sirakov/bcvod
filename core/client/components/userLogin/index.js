var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Delegator = require('dom-delegate'),
	http = require('../http'),
	email = require('../login/email'),
	Freezer = require('freezer-js'),
	Render = require('../overlay/overlays/login');

var UserLogin = function(dom, prerender, content){
	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-login-view');

		obj.target = target;

	}

	_this._dom = new Freezer(dom);

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	var _BodyDelegator = new Delegator(document.body);

	_BodyDelegator.on('click', 'a[data-action="email"]', function(e){

		_this._dom.get().set('isEmail', true);
		_this._dom.get().set('disabled', true);

		_this.loop.update(_this._dom.get().toJS());

		setTimeout(function(){

			var input = _this.loop.target.querySelector('#user');

			setTimeout(function(){

				input.focus();

			}, 50);

		}, 50);

		return;

	});

	_BodyDelegator.on('input', 'input#user', function(e){
		
		e.preventDefault();

		_this._dom.get().set('user', e.target.value);

		if (!email.test(_this._dom.get().user)) {

			_this._dom.get().set('disabled', true);

			_this.loop.update(_this._dom.get().toJS());

		} else {

			_this._dom.get().remove('disabled');

			_this.loop.update(_this._dom.get().toJS());		

		}

		return false;
	});


	_BodyDelegator.on('submit', 'form.login-form', function(e){
		
		e.preventDefault();

		_this._dom.get().set('disabled', true);

		_this.loop.update(_this._dom.get().toJS());

		http({
			url: '/login',
			method: 'post',
			data: {
				user: _this._dom.get().user,
				r: _this._dom.get().path
			},
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			},
			xsrfCookieName: 'xsrf',
			xsrfHeaderName: 'X-XSRF-TOKEN'
		}).then(function(res){

			if (res.data.success) {

				_this._dom.get().remove('disabled');

				_this._dom.get().set('success', true);

				_this.loop.update(_this._dom.get().toJS());

			}

		}).catch(function(err){

			if (err.status === 401) {

				_this._dom.get().remove('disabled');

				_this._dom.get().set(error, 'Invalid user');
				
				_this.loop.update(_this._dom.get().toJS());

			} else {

				_this._dom.get().set(error, 'An error occurred while trying to validate user.');

				_this.loop.update(_this._dom.get().toJS());

			}

		});

		return false;
	});

	_this._EventListeners = [_BodyDelegator];
};

UserLogin.prototype = {};

module.exports = UserLogin;