var axios = require('axios'),
	merge = require('mout/object/merge'),
	overlay = require('./overlay'),
	loadingBar = require('./loading'),
	def = {
		headers: {
			'X-Requested-With': 'XMLHttpRequest'
		},
		xsrfCookieName: 'xsrf',
		xsrfHeaderName: 'X-XSRF-TOKEN'
	};

var Http = function(config, callback){

	if (!callback) return axios(merge(def, config));

	loadingBar.start();

	return axios(merge(def, config)).then(function(res){

		if (loadingBar.isLoading()) loadingBar.stop();

		return callback(null, res);

	}).catch(function(err){

		//handle overlay for http errors
		//overlay
		//
		
		if (loadingBar.isLoading()) loadingBar.stop();

		if (err.status === 0) {

			//show connection error

			if (overlay._current !== 'noconn') overlay.add({
				type: 'noconn'
			});


			
		}

		return callback(err, null);

	});
};


module.exports = Http;