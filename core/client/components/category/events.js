var Delegator = require('dom-delegate'),
	http = require('../http')
	qs = require('mout/queryString/encode'),
	contains = require('mout/array/contains'),
	merge = require('mout/object/merge'),
	ladda = require('ladda'),
	page = require('page.js');

var Events = function(){
	
	var _this = this;

	var _BodyDelegator = new Delegator(document.body);
	
	_BodyDelegator.on('click', 'button[data-action="load-more"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="load-more"]').dom[0],
			l = ladda.create(el),
			dom = _this._dom.get().toJS(),
			path = '/category/' + dom.name.toLowerCase(),
			lastAndFilters = merge(dom.last || {}, dom.filters);

		l.start();

		http({
			url: path + qs(lastAndFilters),
			method: 'get'
		}, function(err, res){

			if (err) {

				l.stop();
				l.remove();

				_this.loop.update(_this._dom.get().toJS());

				return;

			}
			
			var currentVideos = _this._dom.get().videos.toJS(),
				videos = res.data.videos;

			if (res.data.last) _this._dom.get().last.set(res.data.last);
			else _this._dom.get().remove('last');

			_this._dom.get().videos.set(currentVideos.concat(videos));

			l.stop();
			l.remove();

			_this.loop.update(_this._dom.get().toJS());

			setTimeout(function(){

				_this.echo.render();

			}, 100);

		});

	});

	_BodyDelegator.on('click', 'button[data-action="show-filter"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="show-filter"]').dom[0],
			dom = _this._dom.get();


		if (!dom.filter) _this._dom.get().set('filter', true);
		else _this._dom.get().remove('filter');

		_this.loop.update(_this._dom.get().toJS());
	
	});

	_BodyDelegator.on('click', 'a[data-action="filter"]', function(e){

		e.preventDefault();

		var el = (e.target.tagName.toLowerCase() === 'a') ? e.target: _this.$(e.target).parents('a[data-action="filter"]').dom[0],
			data = el.dataset,
			type = data.type,
			value = data.value,
			dom = _this._dom.get();

		if (dom.filters[type] === value) _this._dom.get().filters.remove(type);
		else _this._dom.get().filters.set(type, value);

		var filters = _this._dom.get().filters,
			path = '/category/' + dom.name.toLowerCase() + qs(filters),
			ctx = new page.Context(path,  {});

		_this._dom.get().set('isLoading', true);

		ctx.save();

		_this.loop.update(_this._dom.get().toJS());


		http({
			url: path,
			method: 'get'
		}, function(err, res){

			var toSet = _this._dom.get().toJS();

			_this._dom.get().remove('isLoading');

			if (err) return;

			delete toSet.isLoading;

			toSet.videos = res.data.videos;

			if (res.data.last) toSet.last = res.data.last;
			else delete toSet.last;

			// console.log(toSet);

			_this.loop.update(toSet);

			setTimeout(function(){

				_this.echo.render();

			}, 100);

			_this._dom.get().videos.reset(res.data.videos);

			if (res.data.last) _this._dom.get().set('last', res.data.last);
			else _this._dom.get().remove('last');

			return false;


		});

		return false;
	
	});

	_this._EventListeners = [_BodyDelegator];
	
};

module.exports = Events;