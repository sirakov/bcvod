var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	warna = require('warna'),
	forEach = require('fast.js/array/forEach'),
	reduce = require('fast.js/array/reduce'),
    aspectRatio = require('../utils/aspectRatio'),
    _subtractWidth = require('../utils/subtractWidth'),
    increaseAspect = require('../utils/increaseAspect'),
    decreaseAspect = require('../utils/decreaseAspect'),
    genresO = require('./genres'),
    yearsO = require('./years'),
    originsO = require('./origins'),
    musicGenres = require('./musicGenres'),
    musicYears = require('./musicYears'),
    musicOrigins = require('./musicOrigin'),
    languagesO = require('./languages'),
    freeze = require('freezer-js'),
    chunk = function(arr, chunkSize) {
	    var groups = [], i;
	    for (i = 0; i < arr.length; i += chunkSize) {
	        groups.push(arr.slice(i, i + chunkSize));
	    }
	    return groups;
	};

var Render = function(state){

	var layouts = chunk(state.videos, 4),
		isSeries = state.name.toLowerCase() === 'series';

	if (state.name.toLowerCase() === 'music') {

		var genres = musicGenres;
		var years = musicYears;
		var origins	= musicOrigins;

		var languages = [];

	} else {

		var genres = genresO;
		var years = yearsO;
		var origins	= originsO;

		var languages = languagesO;


	}

	state.defaultPoster = new freeze(state.defaultPoster);

	layouts = map(layouts, function(videos){

	    var sum = reduce(videos, function(a, b){

			var poster;
		
			if (b.poster) poster = JSON.parse(b.poster);
			else poster = state.defaultPoster.get().toJS();

            var ar = aspectRatio(poster.width, poster.height);

            return a + ar.width;

        }, 0),
        ratio = reduce(videos, function(a, b){

        	var poster;

			if (b.poster) poster = JSON.parse(b.poster);
			else poster = state.defaultPoster.get().toJS();

            var ar = aspectRatio(poster.width, poster.height);

            return a + (ar.width / ar.height);

        }, 0),
        minH = sum / ratio,
        ImgSumW = 0;

        forEach(videos, function(video){

        	if (video.poster) video.poster = JSON.parse(video.poster);
        	else video.poster = state.defaultPoster.get().toJS();
	        
	        var ar = aspectRatio(video.poster.width, video.poster.height),
	            width = ar.width,
	            height = ar.height,
	            minP = ((minH / ar.height) * 100);

	        if (minP > 100) {
	            
	            var widthW = increaseAspect(width, height, (minP - 100));
	            
	            ImgSumW += widthW.width;
	            
	            video.poster.height = widthW.height;
	            video.poster.width = widthW.width;

	        } else if (minP < 100) {

	            var widthW = decreaseAspect(width, height, (100 - minP));
	            
	            ImgSumW += widthW.width;
	            
	            video.poster.height = widthW.height;
	            video.poster.width = widthW.width;

	        } else {

	            ImgSumW += width;

	            video.poster.width = width;

	        }

	        video.poster = JSON.stringify(video.poster);

	    });

		var videos = map(videos, function(video){

			if (!video.title) video.title = 'Untitled';

			var image;

			if (video.poster) {

				var poster = JSON.parse(video.poster);

				if (videos.length > 1) {

					var width = (poster.width / ImgSumW) * 100,
	                    ra = aspectRatio(((width - _subtractWidth()) / 100) * ImgSumW, minH);

	            } else if (videos.length === 1) {

	            	var width = 20,
	                    ra = aspectRatio(((width - _subtractWidth()) / 100) * ImgSumW, minH / 5);

	            }

				image = _h('img.bc-vod-overlay-scale.bc-vod-video-poster', {
					attributes:{
						alt: video.title,
						'data-echo': state.imgsUrl + '/' + poster.src
					}
				});

			}

			var figcap,
				fig;

			if (poster.src !== 'default.jpg') {

				figcap = '.bc-vod-overlay-slide-bottom';
				fig = '.bc-vod-overlay-hover';

			} else {

				figcap = '';
				fig = '';

			}

			return _h('figure.bc-vod-video-figure.bc-vod-overlay' + fig, {
					attributes:{
						'data-width': poster.width,
						'data-height': poster.height
					},
				    style:{
					    width: (width - _subtractWidth()) + '%'
				    }
			}, [
				_h('div.bc-vod-video-cover', [
					_h('div.ratioHolder', {
						style: {
							padding: '0px 0px ' + ra.ratio +  '% 0px'
						}
					}),
					image
				]),
				_h('figcaption.bold.bc-vod-text-truncate.bc-vod-overlay-panel.bc-vod-overlay-background.bc-vod-overlay-bottom' + figcap, [
					String(video.title)
				]),
				_h('a.bc-vod-position-cover',{
					href: '/v/' + video.vid,
					attributes:{
						title: video.title
					}
				})
			]);


		});

		return _h('section.bc-vod-video-section.clearfix', {
			attributes:{
				'data-video-count': videos.length
			}
		}, videos);

	});

	var LoadMore;

	if (state.last && state.videos.length >= 12) {

		LoadMore = _h('button.ladda-button.btn.btn-primary.bc-vod-container-center.bc-vod-display-block.bc-vod-margin-large', {
			attributes:{
				'data-action': 'load-more',
				'data-style': 'expand-left',
				'data-spinner-color': '#fff'
			}
		}, [
			_h('span.ladda-label', [String('Load More')]),
			_h('span.ladda-spinner'),
			_h('div.ladda-progress', {
				style:{
					width: '0px'
				}
			})
		]);

	}

	var FiltersClass,
		Videos;


	if (state.videos.length) {

		Videos = _h('div.bc-vod-width-1-1.bc-vod-container-center', layouts);

	} else {

		Videos = _h('div.bc-vod-width-1-1.bc-vod-container-center', [
			_h('div.bc-vod-grid', [
				_h('div.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-4', [
					_h('h5.bold.text-center.bc-vod-margin-large-top.bc-vod-margin-large-bottom', [String('No Videos Found')])
				])
			])
		]);

	}

	FiltersClass = [
		'btn',
		'bc-vod-margin-large-bottom'
	];

	if (state.filter) {

		FiltersClass.push('btn-info');

	} else {

		FiltersClass.push('btn-default');

	}

	state.defaultPoster = state.defaultPoster.get().toJS();
	
	return _h('div.bc-vod-category-page.bc-vod-container.bc-vod-container-center', [
		_h('div.bc-vod-grid.bc-vod-margin-large-top', [
			_h('div.bc-vod-width-1-1', [
				_h('button', {
					className: FiltersClass.join(' '),
					attributes:{
						'data-action': 'show-filter'
					},
					style:{
						display: 'block',
						margin: '0 auto'
					}
				}, [
					_h('i.ti-filter.bc-vod-margin-small-right', {
						style:{
							verticalAlign: 'middle'
						}
					}),
					String('Filter')
				]),
				_h('div.bc-vod-filters', {
					style:{
						display: (state.filter) ? 'block' : 'none'
 					}
				}, [
					_h('div.bc-vod-grid', [
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-1-4', [
							_h('h6.bold', [String('Genre')]),
							_h('div.filters', map(genres, function(genre){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (state.filters.genres === genre) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'genres',
										'data-value': genre,
										disabled: (state.isLoading) ? true : undefined
									}
								}, [String(genre)]);
							}))
						]),
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-1-4', [
							_h('h6.bold', [String('Origin')]),
							_h('div.filters', map(origins, function(origin){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (state.filters.origin === origin) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'origin',
										'data-value': origin,
										disabled: (state.isLoading) ? true : undefined
									}
								}, [String(origin)]);
							}))
						]),
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-1-4', [
							_h('h6.bold', [String('Production Year')]),
							_h('div.filters', map(years, function(year){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (state.filters.year === year) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'year',
										'data-value': year,
										disabled: (state.isLoading) ? true : undefined
									}
								}, [String(year)]);
							}))
						]),
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-1-4', {
							style:{
								display: (!languages.length) ? 'none': undefined
							}
						}, [
							_h('h6.bold', [String('Languages')]),
							_h('div.filters', map(languages, function(language){

								var classList = [
									'btn',
									'btn-xs',
									'bc-vod-margin-small-right',
									'bc-vod-margin-small-bottom'
								];

								if (state.filters.language === language) {

									classList.push('btn-info');

								}

								return _h('a', {
									className: classList.join(' '),
									attributes:{
										'data-action': 'filter',
										'data-type': 'language',
										'data-value': language,
										disabled: (state.isLoading) ? true : undefined
									}
								}, [String(language)]);
							}))
						])
					])
				])
			])
		]),
		_h('div.bc-vod-grid.bc-vod-margin-top-remove', {
			style:{
				opacity: (state.isLoading) ? 0.3: undefined
			}
		}, [
			Videos
		]),
		LoadMore
	]);

};

module.exports = Render;