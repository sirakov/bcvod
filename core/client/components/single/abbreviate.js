var numberWithCommas = function(x) {
    return (x) ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0;
};

module.exports = numberWithCommas;