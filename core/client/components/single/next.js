var http = require('../http'),
	initPlayer = require('./initPlayer'),
	page = require('page.js'),
	title = require('../head/title');

var NextVideo = function(_id){

	var _this = this,
		vid = _this._dom.get().toJS();

	if (vid.video.vid === _id) return false;

	http({
		method: 'get',
		url: '/v/'+ _id
	}, function(err, res){

		if (err) return;

		_this._dom.get().set({
			video: res.data.video
		});

		_this._player.dispose();

		_this.$('.player-top').addClass('play-init').html('<video id="bc-vod-plyr" class="video-js vjs-default-skin bcvod-skin" data-player="true"></video>');
		_this.$('#disqus_thread').html('');

		initPlayer(_this._dom.get().toJS(), _this);

		var ctx = new page.Context('/v/' + res.data.video.vid, {
			isOld: true
		})

		page.current = ctx.path;

		ctx.title = title((res.data.video.title) ? res.data.video.title: '', vid.brandName);

		document.title = ctx.title;

		ctx.pushState();

		_this.loop.update(_this._dom.get().toJS());

		setTimeout(function(){

			if (_this.scroller) _this.scroller.echo.render();

		}, 100);

		return;

	});

	
};

module.exports = NextVideo;