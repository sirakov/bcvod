var GetImage = function(poster){

	if (poster) return JSON.parse(poster).src;
	else return undefined;
	
};

module.exports = GetImage;