var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js');

var Single = function(dom, prerender, content){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-video-single-div');

		obj.target = target;

	}

	_this._dom = new Freezer(dom);

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	_this._EventListeners = [];

	_this.$ = require('sprint-js');

	_this._initEvents();

};

Single.prototype = {
	_initEvents: require('./events'),
	next: require('./next')
};

module.exports = Single;
