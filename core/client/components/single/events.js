var Delegator = require('dom-delegate'),
	http = require('../http');

var Events = function(){
	
	var _this = this;

	var _BodyDelegator = new Delegator(document.body);
	
	_BodyDelegator.on('click', 'button[data-action="like"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="like"]').dom[0],
			video = _this._dom.get().video.toJS();

		http({
			method: 'post',
			url: '/v/'+ video.vid +'/likes'
		}, function(err, res){

			if (err) return;

			_this._dom.get().video.set({
				likes: video.likes + 1,
				liked: true
			}); 


			return _this.loop.update(_this._dom.get().toJS());

		});


	});

	_BodyDelegator.on('click', 'button[data-action="unlike"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="unlike"]').dom[0],
			video = _this._dom.get().video.toJS();

		http({
			method: 'delete',
			url: '/v/'+ video.vid +'/likes'
		}, function(err, res){

			if (err) return;

			_this._dom.get().video.set({
				likes: (video.likes === 0) ? 0 : video.likes - 1
			});

			_this._dom.get().video.remove('liked'); 


			return _this.loop.update(_this._dom.get().toJS());

		});

	});

	_BodyDelegator.on('click', 'a[data-action="open-video"]', function(e){

		e.preventDefault();

		var el = (e.target.tagName.toLowerCase() === 'a') ? e.target: _this.$(e.target).parents('a[data-action="open-video"]').dom[0],
			data = el.dataset;
		
		return _this.next(data.vid);

	});


	_this._EventListeners = [_BodyDelegator];
	
};

module.exports = Events;