var	_h = require('virtual-dom/h');

var Video = function (state){
	this.state = state;
};

Video.prototype.type = 'Thunk';
Video.prototype.render = function(previous){

	var _this = this;

	if (previous && previous.vnode) {

		return previous.vnode;

	} else {

		return _h('div.player-top.play-init', [
			_h('video#bc-vod-plyr.video-js.vjs-default-skin.bcvod-skin', {
				attributes:{
					controls: false,
					'data-player': true
				}
			}, [])
		]);

	}

};

module.exports = Video;