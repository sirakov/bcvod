var _h = require('virtual-dom/h'),
	moment = require('moment'),
	capitalize = require('../header/capitalize');

module.exports = function(state){

	var BreadCrumb = [];

	if (state.video.category) {

		var _name = (state.video.category || '').split('/')[1];

		if (_name) {

			BreadCrumb.push({
				"@type": "ListItem",
				"position": 1,
				"item": {
					"@id": state.baseUrl + "/category/" + _name,
					"name": capitalize(_name)
				}
			});

		}
	}

	return _h('script', {
		attributes:{
			'type': 'application/ld+json'
		},
		innerHTML: JSON.stringify([{
			"url": state.baseUrl + '/v/' + state.video.vid,
			"thumbnailUrl": state.thumbnailsUrl + '/' + state.video.thumbnail,
			"embedUrl": state.baseUrl + '/v/' + state.video.vid + '?embed=true',
			"name": state.video.title,
			"description": state.video.description || 'No description yet',
			"height": 640,
			"width": 960,
			"playerType": "HTML5 Flash",
			"videoQuality": "SD",
			"duration": moment.duration(state.video.duration).toISOString(),
			"uploadDate": state.video.createdAt,
			"thumbnail": {
				"@type": "ImageObject",
				"url": state.thumbnailsUrl + '/' + state.video.thumbnail,
				"width": 480,
				"height": 320
			},
			"@type": "VideoObject",
			"@context": "http://schema.org",
			"interactionStatistic": [
				{
			      "@type": "InteractionCounter",
			      "interactionService": {
			        "@type": "Website",
			        "name": state.brandName,
			        "url": state.baseUrl
			      },
			      "interactionType": "http://schema.org/LikeAction",
			      "userInteractionCount": state.video.likes || 0
			    },
			    {
			      "@type": "InteractionCounter",
			      "interactionType": "http://schema.org/WatchAction",
			      "userInteractionCount": state.video.views
			    }
		    ]},{
				"itemListElement": BreadCrumb,
				"@type": "BreadcrumbList",
				"@context": "http://schema.org"
			}
		])
	});

};
