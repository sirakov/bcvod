var _h = require('virtual-dom/h'),
	warna = require('warna'),
	moment = require('moment'),
	map = require('fast.js/array/map'),
	abbreviate = require('./abbreviate'),
	capitalize = require('../header/capitalize'),
	Meta = require('./meta');

var Render = function(state){

	var poster,
		BG,
		UserState,
		UserStateRight,
		Edit;

	if (state.video.poster) poster = JSON.parse(state.video.poster);
	else poster = state.defaultPoster;

	var Vid = _h('div.player-top.play-init', [
			_h('video#bc-vod-plyr.video-js.vjs-default-skin.bcvod-skin', {
				attributes:{
					controls: false,
					'data-player': true
				}
			}, [])
		]);

	if (state.embed) {

		return _h('div.is-embed.bc-vod-video-single-div.bc-vod-width-small-1-1.bc-vod-width-medium-1-1.bc-vod-width-large-10-10.bc-vod-video-wrapper', [
			Vid
		]);

	}

	if (state._currentUser && !state._currentUser.frontend) {

		Edit = _h('a', {
			href: '/admin/v/' + state.video.vid +'/edit?public=true'
		}, [String('Edit')]);

	}

	if (state.video.poster) {

		BG  = _h('div.bc-vod-video-card', {
			style:{
				backgroundImage: 'url('+ state.imgsUrl + '/' + poster.src +'?auto=compress&blur=60)'
			}
		});

	} else {

		BG  = _h('div.bc-vod-video-card');

	}

	var classList = [
		'btn',
		'bc-vod-padding-remove'
	],
	action,
	likedText = '';

	if (state._currentUser && state._currentUser.frontend) {

		if (state.video.liked) {

			action = 'unlike';

		} else {

			action = 'like';

		}


	} else {

		action = 'login';

	}

	if (state.video.liked) {

		likedText = 'Liked';

		classList.push('btn-danger');

	} else {

		if (state.video.likes === 1) {

			likedText = 'Like';

		} else {

			likedText = 'Likes';

		}

		classList.push('btn-primary');

	}

	UserStateRight = _h('div.bc-vod-float-right.bc-vod-hidden-small', [
		_h('button', {
			className: classList.join(' '),
			attributes:{
				'data-action': action
			}
		}, [
			_h('i.ti-heart.bc-vod-margin-small-right'),
			String(abbreviate(state.video.likes || 0))
		])
	]);

	UserState = _h('div.bc-vod-visible-small.bc-vod-margin-top', [
		_h('button', {
			className: classList.join(' '),
			attributes:{
				'data-action': action
			}
		}, [
			_h('i.ti-heart.bc-vod-margin-small-right'),
			String(abbreviate(state.video.likes))
		])
	]);

	var Eps;

	if (state.video.episodes && state.video.episodes.length) {

		Eps = map(state.video.episodes, function(episode){

				episode.episodeId = parseInt(episode.episodeId);

				var suff = (episode.episodeId === 1) ? '' :  '/' + episode.episodeId;

				var currentEp = state.video.episodeId === episode.episodeId,
					Classes = [
						'btn-rounded',
						'btn-lg',
						'btn',
						'bc-vod-margin-small-right',
						'bc-vod-margin-small-bottom'
					];

				if (currentEp) {

					Classes.push('btn-info');

				} else {

					Classes.push('btn-default');

				}

				return _h('a', {
					className: Classes.join(' '),
					attributes:{
						disabled: (currentEp) ? true: undefined
					},
					href: '/v/' + state.video.vid + suff
				}, [
					String(episode.episodeId)
				]);
			});

	}

	var PlayList;

	if (state.playlist && state.playlist.length) {

		// state.video.main = true;

		// state.playlist.splice(0, 0, state.video);
		//
		// ==
		//
		//

		PlayList = _h('div.playlist.ss-container.js_slider.slider', [
			_h('div.ss-wrapper.js_frame', [
				_h('div.ss-content.js_slides', map(state.playlist, function(video){

					var Thumb;

					var savedIn = video.savedIn.split('/'),
						vid = (!video.main) ? [savedIn[1], savedIn[2], video.vid].join('-') : video.vid,
						playClass = ['playlist-video'];

					if (video.thumbnail) {

						Thumb = _h('a.video-thumbnail', {
							attributes:{
								href: '/v/' + vid,
								'data-vid': vid,
								'data-action': 'open-video',
								'data-echo-background': state.thumbnailsUrl + '/' + video.thumbnail + '?w=190&auto=compress'
							}
						});

					} else {

						Thumb = _h('div.video-thumbnail');

					}

					if (vid === state.video.vid) {

						playClass.push('is-active');

					}

					playClass.push('clearfix');
					playClass.push('js_slide');

					return _h('div', {
						className: playClass.join(' ')
					}, [
						Thumb,
						_h('div.video-content', [
							_h('div.video-title.bc-vod-text-truncate', [
								_h('a', {
									attributes:{
										'data-vid': vid,
										'data-action': 'open-video'
									},
									href: '/v/' + vid
								}, [
									String(video.title || 'Untitled Video')
								])
							]),
							_h('div.video-meta', [
								_h('div.video-views', [
									_h('i.ti-pulse.m-r-5'),
									String((abbreviate(video.views) || 0) + ' views')
								])
							])
						])
					]);
				}))
			]),
			_h('a.prev_arrow.thumb_arrow.js_prev', {
				attributes:{
					'href': 'javascript:void(0)'
				}
			}, [
				_h('span')
			]),
			_h('a.next_arrow.thumb_arrow.js_next', {
				attributes:{
					'href': 'javascript:void(0)'
				}
			}, [
				_h('span')
			]),
			_h('div.ss-scroll')
		]);

	} else {

		if (!state.playlistErr) {

			PlayList = _h('div.playlist.ss-container.js_slider.slider', [
				_h('div.ss-wrapper.js_frame', [
					_h('div.ss-content.js_slides', [
						_h('div.spinner')
					])
				]),
				_h('div.ss-scroll')
			]);

		} else {

			PlayList = _h('div.playlist.ss-container.js_slider.slider', [
				_h('div.ss-wrapper.js_frame', [
					_h('div.ss-content.js_slides', [
						_h('div.playlist-error', [
							String('An error occurred while loading playlist from the server.')
						])
					])
				]),
				_h('div.ss-scroll')
			]);


		}

	}

	// console.log(state.video);

	var videoCategory;

	if (state.video.category) {

		videoCategory = state.video.category.split('/')[1];

	}

	var LanguageMeta,
		CategoryMeta,
		YearMeta,
		GenreMeta,
		OriginMeta;

	if (state.video.language) {

		LanguageMeta = _h('div.meta-details-year.m-t-10', [
			_h('div.bold.font-montserrat', ['Language']),
			_h('a.btn.btn-xs.btn-info.m-t-10', {
				href: '/category/' + videoCategory + '?year=' + state.video.language
			}, [state.video.language])
		]);

	}

	if (state.video.year) {

		YearMeta = _h('div.meta-details-year.m-t-10', [
			_h('div.bold.font-montserrat', ['Production Year']),
			_h('a.btn.btn-xs.btn-info.m-t-10', {
				href: '/category/' + videoCategory + '?year=' + state.video.year
			}, [state.video.year])
		]);

	}

	if (state.video.origin) {

		OriginMeta = _h('div.meta-details-origin.m-t-10', [
			_h('div.bold.font-montserrat', ['Origin']),
			_h('a.btn.btn-xs.btn-info.m-t-10', {
				href: '/category/' + videoCategory + '?origin=' + state.video.origin
			}, [capitalize(state.video.origin)])
		]);

	}

	if (state.video.genres && state.video.genres.length) {

		GenreMeta = _h('div.meta-details-genres.m-t-10', [
			_h('div.bold.font-montserrat.m-b-10', ['Genres']),
			_h('div.genres', map(state.video.genres, function(genre){
				return _h('a.btn.btn-xs.bc-vod-margin-small-right.bc-vod-margin-small-bottom.btn-info', {
					href: '/category/' + videoCategory + '?genres=' + genre
				}, [genre]);
			}))
		]);

	}

	if (videoCategory) {

		CategoryMeta = _h('div.meta-details-category', [
			_h('div.bold.font-montserrat', ['Category']),
			_h('a.btn.btn-xs.btn-info.m-t-10', {
				href: '/category/' + videoCategory
			}, [capitalize(videoCategory)])
		])

	}

	var Content = [
		_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-3-5.bc-vod-width-large-3-5', [
			Edit,
			_h('h2.title.bold', [String(state.video.title)]),
			_h('div.bc-vod-grid.clearfix.bc-vod-stats-view', [
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-4', [
					String('Published on '+ moment(new Date(state.video.idt)).format('MMM DD, YYYY') + ' '),
					_h('span.bc-vod-display-block', [
						_h('i.ti-pulse.m-r-5'),
						_h('span.bold', [String(abbreviate(state.video.views))]),
						' views'
					])
				]),
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-4.bc-vod-width-large-2-4.clearfix', [
					UserState,
					UserStateRight
				])
			]),
			_h('div.description.bc-vod-margin-large-top.bc-vod-margin-large-bottom', [
				_h('span', [String(state.video.description || 'No description yet')])
			]),
			_h('div.meta-details', [
				CategoryMeta,
				GenreMeta,
				OriginMeta,
				YearMeta,
				LanguageMeta
			])
		]),
		_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-5.bc-vod-width-large-2-5.clearfix', [
			_h('div.bc-vod-video-poster-single.clearfix.bc-vod-margin-large-top', [
				_h('img.bc-vod-video-poster-img', {
					src: state.imgsUrl + '/' + poster.src + '?w=220&auto=compress',
					attributes:{
						alt: state.video.title
					}
				})
			])
		])
	];

	var Episodes = _h('div.bc-vod-width-1-1', [
		_h('p.bold.bc-vod-margin-large-top', {
			style:{
				display: (state.video.episodes && state.video.episodes.length) ? 'block': 'none'
			}
		}, [String('Episodes')]),
		_h('div.bc-vod-episodes.bc-vod-margin-top', Eps)
	]);

	// console.log(moment.duration(105441).toISOString());

	return _h('div.bc-vod-video-single-div.bc-vod-margin-remove.clearfix', [
		_h('div.bc-vod-single-video-wrapper', [
			_h('div.bc-vod-video-color', {
				style:{
					backgroundColor: (poster && poster.color) ? warna.darken(poster.color, 0.5).hex : '#000'
				}
			}),
			BG,
			_h('div.bc-vod-grid.bc-vod-grid-small', [
				_h('div.bc-vod-width-small-4-5.bc-vod-width-medium-8-10.bc-vod-container-center.bc-vod-video-container.no-padding-xs', [
					_h('div.bc-vod-grid.bc-vod-grid-small', [
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-1-1.bc-vod-width-large-7-10.bc-vod-video-wrapper.no-padding-xs', [
							Vid
						]),
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-1-1.bc-vod-width-large-3-10.no-padding-xs.bc-vod-position-relative', [
							PlayList
						])
					])
				])
			])
		]),
		_h('div.bc-vod-main-wrapper.bc-vod-container.bc-vod-container-center.bc-vod-margin-large-bottom', [
			_h('div.bc-vod-grid.bc-vod-hidden-small', [
				_h('div.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-4-5.bc-vod-width-large-4-5', [
					_h('div.bc-vod-grid.clearfix', [
						Episodes
					]),
					_h('div.bc-vod-grid.clearfix', Content)
				])
			]),
			_h('div.bc-vod-grid.clearfix.bc-vod-visible-small', [
				Episodes
			]),
			_h('div.bc-vod-grid.clearfix.bc-vod-visible-small', Content),
			_h('div.bc-vod-grid.clearfix', [
				_h('div.bc-vod-comments.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-4-5.bc-vod-width-large-4-5.bc-vod-margin-top', [
					_h('div#disqus_thread')
				])
			])
		]),
		Meta(state)
	]);

};

module.exports = Render;
