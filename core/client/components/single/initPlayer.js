var http = require('../http'),
	path = require('path'),
	page = require('page.js'),
	qs = require('mout/queryString/encode'),
	title = require('../head/title'),
	params = require('../utils/params'),
	Lory = require('lory.js').lory,
	findIndex = require('mout/array/findIndex');

window.videojs = require('video.js');

require('../../vjs-hls');

require('../../videojs.socialShare');

require('../../videojs.watermark');

require('../../videojs.persistvolume');

require('../../vast/es5-shim');
require('../../vast/ie8fix');
require('../../vast/swfobject');
require('../../vast/videojs_5.vast.vpaid.min');

require('../../videojs.hotkeys');

// window.DMVAST = require('../../vast-client');
// require('../../videojs.ads');
// require('../../videojs.vast')

// require('../../videojs.ima');

require('../../videojs.ga');

require('../../simple-scrollbar');

var Component = videojs.getComponent('Component'),
	canPlayType = function(type) {

		var mpegurlRE = /^application\/(?:x-|vnd\.apple\.)mpegurl/i;

		return mpegurlRE.test(type);

	};

videojs.getComponent('Flash').registerSourceHandler({
    canHandleSource: function(srcObj) {
      return canPlayType(srcObj.type);
    },
    handleSource: function(source, tech) {

		// tech.setTimeout(function() {
		// 	tech.trigger('loadstart');
		// }, 1);

		tech.src(source.src);

		// return tech;
    },
    canPlayType: function(type) {
      return canPlayType(type);
    }
});

var InitPlayer = function(vid, _this){

	var extname = path.extname(vid.video.s3FileName),
        s3File = '/' + vid.video.s3FileName.replace(extname, '') + '/',
        _source,
        _base,
        _url;

    if (vid.video.episode) {

		_url = vid.baseUrl + '/v/' + vid.video.vid + '/' + vid.video.episodeId;

	} else {

		_url = vid.baseUrl + '/v/' + vid.video.vid;

	}

    if (vid.env === 'development') {

        _source = 'http://192.168.43.61:8250/hls/master.m3u8';

       	_base = vid.assetsUrl + '/javascripts';

    } else {

        _source = vid.streamUrl + s3File + 'master.m3u8';

        _base = vid.assetsUrl + '/_a/js';

    }

    _this.$('.player-top').removeClass('play-init');

    var player = videojs('bc-vod-plyr', {
    	flash: {
			swf: _base + '/plyr.swf'
		},
		preload: 'auto',
		controls: true,
		autoplay: !vid.embed,
    	controlBar: {
    		children:[
    			'playToggle',
    			'currentTimeDisplay',
    			'timeDivider',
    			'durationDisplay',
    			'progressControl',
    			'volumeMenuButton',
    			'fullscreenToggle'
    		]
		}
    });

    player.ready(function(){

    	this.hotkeys({
    		enableNumbers: false,
    		enableVolumeScroll: false,
			alwaysCaptureHotkeys: true,
			volumeUpKey: function(event, player) {

		      return (event.ctrlKey && (event.which === 38));

		    },
		    volumeDownKey: function(event, player) {

		      return (event.ctrlKey && (event.which === 40));

		    },
		    forwardKey: function(event, player) {

		      return (event.ctrlKey && (event.which === 39));

		    },
		    rewindKey: function(event, player) {

		      return (event.ctrlKey && (event.which === 37));

		    }
    	});

		this.persistvolume({
			namespace: 'bc-vod'
		});

		this.src({
	    	type: 'application/x-mpegURL',
	    	src: _source
	    });

		// this.ads();
		this.vastClient({
			vpaidFlashLoaderPath: _base + '/ads.swf',
			playAdAlways: true,
			adCancelTimeout: 10000,
			adsEnabled: true,
			adTagUrl: 'https://pubads.g.doubleclick.net/gampad/ads?sz=1920x1080|1280x720|640x480&iu=/96429187/kukuotvprerolltest&impl=s&gdfp_req=1&env=vp&output=xml_vast3&unviewed_position_start=1&url='+ _url +'&description_url='+ _url +'&correlator=' + Date.now()
		});

		this.ga();

		// this.play();

	});

	var timerOutPlay,
		timerOutPause;

	player.on('vast.adError', function () {

		if (player.vast.isEnabled()) player.vast.disable();

	});

	player.on('vast.adsCancel', function () {

		if (player.vast.isEnabled()) player.vast.disable();

	});

	player.on('vast.contentStart', function () {

		if (player.vast.isEnabled()) player.vast.disable();

	});

	player.on('vast.contentEnd', function () {

		if (player.vast.isEnabled()) player.vast.disable();

	});

	player.on('pause', function(){

		if (!player.hasStarted() || player.vast.isEnabled() || player.ended()) return false;

		if (timerOutPause) clearTimeout(timerOutPause);
		if (timerOutPlay) clearTimeout(timerOutPlay);

		player.childNameIndex_.bigPlayButton.removeClass('is-animate')
											.addClass('is-pause')
											.addClass('is-animate');

		timerOutPause = setTimeout(function(){

			player.childNameIndex_.bigPlayButton.removeClass('is-animate')
												.removeClass('is-pause');

		}, 500);

	});

	player.on('play', function(){

		if (!player.hasStarted() || player.vast.isEnabled()) return false;

		if (timerOutPause) clearTimeout(timerOutPause);
		if (timerOutPlay) clearTimeout(timerOutPlay);

		player.childNameIndex_.bigPlayButton.removeClass('is-animate')
											.removeClass('is-pause')
											.addClass('is-animate');

		timerOutPlay = setTimeout(function(){

			player.childNameIndex_.bigPlayButton.removeClass('is-animate');

		}, 500);

	});

  //   if ('google' in window) {

  //   	var options = {
		// 	id: 'bc-vod-plyr',
		// 	adTagUrl: 'https://pubads.g.doubleclick.net/gampad/ads?sz=1920x1080|1280x720|640x480&iu=/96429187/kukuotvprerolltest&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]'
		// };

		// player.ima(options);
		// player.ima.requestAds();

  //   }

    player.watermark({
	    file: vid.assetsUrl + vid._a['/images/logo-small.png'] + '?' + vid.build,
	    xpos: 100,
	    ypos: 50,
	    xrepeat: 0,
	    opacity: 0.8,
	});

	player.socialShare({
		facebook: {},
		twitter: {
			handle: 'kukuotv_vod'
		}
	});

	if (!_this._dom.get().playlist.length && !vid.embed) {

		// console.log(vid.video);

		var spl = vid.video.vid.split('-'),
			que = qs({
				savedIn: vid.video.savedIn,
				idt: vid.video.idt,
				vid: spl[spl.length - 1],
				category: vid.video.category,
				genres: vid.video.genres
			});

		http({
			url: '/latest' + que,
			method: 'get'
		}).then(function(res){

			var _main = _this,
				_playlist = res.data.latest;

			vid.video.main = true;

			_playlist.splice(0, 0, vid.video);

			_main.loop.update(_main._dom.get().set({
				playlist: _playlist
			}).toJS());

			// console.log(window.innerWidth < 961, window.innerWidth <= 961);

			setTimeout(function(){

				if (window.innerWidth < 961) {

					_main._slider = Lory(_main.loop.target, {
						enableMouseEvents: true
					});

				}

				var el = document.querySelector('.playlist');
				_this.scroller = new SimpleScrollbar(el);

			}, 100);

		}).catch(function(err){

			var _main = _this;

			_main.loop.update(_main._dom.get().set({
				playlistErr: true
			}).toJS());

		});

	}

	// if (!_this._dom.get().viewed) {
	//
	// 	var dom = _this._dom.get().toJS();
	//
	// 	_this._dom.get().set('viewed', true);
	//
	// 	return http({
	// 		url: '/v/' + dom.video.vid + '/views',
	// 		method: 'post',
	// 		data: dom.video
	// 	}).then(function(res){
	//
	//
	//
	// 	}).catch(function(err){
	//
	//
	// 	});
	//
	// }

	// player.on('timeupdate', function(e) {
	//
	// 	if (player.vast && player.vast.isEnabled()) return;
	//
	// 	var _current = player.cache_.currentTime,
	// 		_duration = player.cache_.duration,
	// 		dom = _this._dom.get().toJS();
	//
	// 	if (((_current >= (_duration / 2)) && _current !== 0 && _duration !== 0) && !dom.viewed) {
	//
	// 		_this._dom.get().set('viewed', true);
	//
	// 		return http({
	// 			url: '/v/' + dom.video.vid + '/views',
	// 			method: 'post',
	// 			data: dom.video
	// 		}).then(function(res){
	//
	//
	//
	// 		}).catch(function(err){
	//
	//
	// 		});
	//
	// 	}
	//
	// });

	player.on('ended', function(e) {

		if (player.vast && player.vast.isEnabled()) return player.vast.disable();

		if (vid.embed) return;

		var _data = _this._dom.get().toJS(),
			_index = findIndex(_data.playlist, {vid: _data.video.vid}),
			_next;

		if (_index > -1) {

			_next = _data.playlist[_index + 1];

			if (_next) {

				var _vid = params.get({
					vid: _next.vid,
					savedIn: _next.savedIn
				});

				return _this.next(_vid);

			}

		} else {



		}

		// console.log(e);

		// var video = _this._dom.get().video.toJS(),
		// 	Se = new RegExp('series', 'g'),
  //           isSeries = Se.test(video.category),
		// 	query = {
		// 		genres: (video.genres && video.genres.length) ? video.genres[0] : null,
		//         isSeries: (video.episodeId === (video.episodes && video.episodes.length)) ? null : isSeries,
		//         sequel: video.sequel,
		//         idt: video.idt,
		//         category: video.category
		// 	};

		// if (video.next) return page('/v/' + video.next);

		// for (key in query){

		// 	if (!query[key]) delete query[key];

		// }

		// return http({
		// 	url: '/v/' + video.vid + '/next',
		// 	method: 'post',
		// 	data: query
		// }, function(err, res){

		// 	if (err) return;

		// 	if (res.data.nextId) return page('/v/' + res.data.nextId);

		// });

	});

	_this._player = player;

	if (vid.env === 'production') {

		if (DISQUS) {

			DISQUS.reset({
				reload: true,
				config: function () {
					if (vid.video.episode) {

						this.page.identifier = vid.video.vid + '/' + vid.video.episodeId;

					} else {

						this.page.identifier = vid.video.vid;

					}

					this.page.title = title((vid.video.title) ? vid.video.title: '', vid.brandName);

					this.page.url = _url;
				}
			});

		}

	}

};

module.exports = InitPlayer;
