var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	moment = require('moment');

var Render = function(state){

	var src;

	if (state.video.poster) {

		var poster = JSON.parse(state.video.poster);

		if (poster.src) src = state.assetsUrl + '/posters/' +  poster.src;

	}

	return _h('div.bc-vod-series-section', [
		_h('div.bc-vod-container.bc-vod-container-center',[
			_h('div.bc-vod-grid.bc-vod-margin-large-top', [
				_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-4-5.bc-vod-width-large-4-5.bc-vod-container-center',[
					_h('div.bc-vod-grid', [
						_h('div.bc-vod-width-small-1-1.bc-vod-width-medium-2-5.bc-vod-width-large-2-5', [
							_h('div.preview-pane', [
								_h('div', [
									_h('img.preview-canvas.bc-vod-container-center',{
										style:{
											display: 'block',
											width: '275px'
										},
										src: src
									})
								])
							])
						]),
						_h('div.bc-vod-margin-top.bc-vod-width-small-1-1.bc-vod-width-medium-3-5.bc-vod-width-large-3-5', [
							_h('h1.bold.title', [String(state.video.title)]),
							_h('span.bc-vod-display-block', [
								String('Published on '+ moment(new Date(state.video.idt)).format('MMM DD, YYYY') )
							]),
							_h('p.description', [String(state.video.description || 'No Description yet')]),
							_h('p.bold', [String('Episodes')]),
							_h('div.episodes', map(state.video.episodes, function(episode){
								return _h('a.btn-rounded.btn.bc-vod-margin-small-right.bc-vod-margin-small-bottom', {
									href: '/s/' + state.video.vid + '/' + episode.episodeId
								}, [
									String(episode.episodeId)
								]);
							})),
							_h('div.episodes', {
								style:{
									display: (!state.video.episodes.length) ? 'block' : 'none'
								}
							}, [String('No Episodes yet!')])
						])
					])
				])
			])
		])
	]);
	
};

module.exports = Render;