var _h = require('virtual-dom/h');

var SiteMain = function(children){
	return _h('.site-main', [
		_h('.screen#prerender', [
			_h('.screenContent.bc-vod-position-relative', children)
		])
	]);
};

module.exports = SiteMain;