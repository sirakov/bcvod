var _h = require('virtual-dom/h');

var Article = function(children){
	return _h('article.container', children);	
};

module.exports = Article;