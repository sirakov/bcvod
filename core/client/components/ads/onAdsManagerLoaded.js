var onAdError = require('./onAdError'),
	onContentResumeRequested = require('./onContentResumeRequested'),
	onContentPauseRequested = require('./onContentPauseRequested');

module.exports = function(adsManagerLoadedEvent) {
  	
	var _this = this;

	_this.manager = adsManagerLoadedEvent.getAdsManager(_this.videoContent);

	window.onresize = function(e){
		
		// console.log(_this.container.clientHeight, _this.container.clientWidth)

		_this.manager.resize(_this.plyr.clientWidth, _this.plyr.clientHeight, google.ima.ViewMode.NORMAL);

	};


  // Add listeners to the required events.
	_this.manager.addEventListener(google.ima.AdErrorEvent.Type.AD_ERROR, function(e){
		return onAdError.call(_this, e);
	});

	_this.manager.addEventListener(google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED, function(e){
		return onContentPauseRequested.call(_this, e);
	});

	_this.manager.addEventListener(google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED, function(e){
		return onContentResumeRequested.call(_this, e)
	});

	try {

		_this.manager.init(_this.plyr.clientWidth, _this.plyr.clientHeight, google.ima.ViewMode.NORMAL);

		_this.manager.start();

	} catch (adError) {
		


	}

};