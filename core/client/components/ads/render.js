var _h = require('virtual-dom/h');

var Ads = function(){
	return _h('div.bc-vod-ads.bc-vod-margin-large-top', [
		_h('script', {
			attributes:{
				async: '',
				src: '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'
			}
		}),
		_h('ins.adsbygoogle', {
			style:{
				display: 'block',
				width: '728px',
				height:'90px',
			    margin: '0 auto'
			},
			attributes:{
				'data-ad-client': 'ca-pub-8857488614866194',
				'data-ad-slot': '2058369260',
				'data-ad-format': 'auto',
			}
		}),
		_h('script', {
			innerHTML: '(adsbygoogle = window.adsbygoogle || []).push({});'
		})
	]);
};

module.exports = Ads;