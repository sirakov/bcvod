var onAdError = require('./onAdError'),
	onAdsManagerLoaded = require('./onAdsManagerLoaded');

var Load = function(){

	var _this = this;

	_this.loader = new google.ima.AdsLoader(_this.displayContainer);

	_this.loader.addEventListener(
	    google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
	    function(e){
	    	return onAdsManagerLoaded.call(_this, e);
	    },
	    false
	);

	_this.loader.addEventListener(
	    google.ima.AdErrorEvent.Type.AD_ERROR,
	    function(e){
	    	return onAdError.call(_this, e);
	    },
	    false
	);

	_this.request = new google.ima.AdsRequest();
	_this.request.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?' +
	    'sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&' +
	    'impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&' +
	    'cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=';

	// Specify the linear and nonlinear slot sizes. This helps the SDK to
	// select the correct creative if multiple are returned.
	_this.request.linearAdSlotWidth = 765;
	_this.request.linearAdSlotHeight = 400;
	_this.request.nonLinearAdSlotWidth = 765;
	_this.request.nonLinearAdSlotHeight = 150;

	_this.loader.requestAds(_this.request);
	
};

module.exports = Load;