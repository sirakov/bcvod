var contentEndedListener = require('./contentEndedListener');

var Ads = function(content, clappr){

	var _this = this;

	_this.plyr = content.querySelector('[data-player]');
	_this.videoContent = _this.plyr.querySelector('video');

	if (!_this.videoContent) {

		_this.videoContent = document.createElement('video');

		_this.plyr.querySelector('.container').appendChild(_this.videoContent);

	}

	_this.videoContent.onended = contentEndedListener.bind(_this);

	_this.clappr = clappr;

	_this.container = document.createElement('div');

	_this.container.id = 'bc-vod-ad-container';
	_this.container.classList.add('kukuotv-ima-plugin');

	_this.plyr.appendChild(_this.container);

	_this.displayContainer = new google.ima.AdDisplayContainer(
					        _this.container,
					        _this.videoContent);

	_this.displayContainer .initialize();

	_this.load();


};

Ads.prototype = {
	load: require('./load')
};

module.exports = Ads;