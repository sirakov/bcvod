var contentEndedListener = require('./contentEndedListener');

module.exports = function() {
  
  var _this = this;

  _this.videoContent.removeEventListener('ended', contentEndedListener.bind(_this));

  _this.clappr.core.disableMediaControl();

  _this.clappr.pause();

}