module.exports = function(adErrorEvent) {

  var _this = this;

  console.log(adErrorEvent.getError());

  _this.manager.destroy();

  _this.clappr.play();

  _this.container.style.display = 'none';

};