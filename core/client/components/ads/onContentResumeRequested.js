var contentEndedListener = require('./contentEndedListener');

module.exports = function() {
  
  var _this = this;

  _this.videoContent.addEventListener('ended', contentEndedListener.bind(_this));

  _this.clappr.core.enableMediaControl();

  _this.clappr.play();

  _this.container.style.display = 'none';

}