var _h = require('virtual-dom/h');

var Render = function(state){
	return _h('footer.bc-vod-container.no-padding.bc-vod-container-center.footer.bc-vod-margin-large-top', [
		_h('div.copyright.sm-text-center.bc-vod-margin-large-top', [
			_h('div.small.no-margin.pull-left.sm-pull-reset', [
				_h('span.hint-text', [String('Copyright © ' + new Date().getFullYear())]),
				_h('span.font-montserrat', [String(' Blue Canvas ')]),
				_h('span.hint-text', [String('All rights reserved. ')]),
				_h('div.bc-vod-dsiplay-block.bc-vod-margin-top', [
					_h('p.bold.bc-vod-display-inline-block', [String('Follow us on :')]),
					_h('ul.bc-vod-list.bc-vod-margin-small-left.bc-vod-display-inline-block', [
						_h('li.bc-vod-display-inline-block', [
							_h('a.bc-vod-margin-small-right', {
								href: 'https://twitter.com/kukuotv_vod',
								attributes:{
									target: '_blank'
								},
								style:{
									fontSize: '17px',
								}
							}, [_h('i.ti-twitter', {
								style:{
								    verticalAlign: 'middle'
								}
							})])
						]),
						_h('li.bc-vod-display-inline-block', [
							_h('a.bc-vod-margin-small-right', {
								href: 'https://www.facebook.com/kukuotv',
								attributes:{
									target: '_blank'
								},
								style:{
									fontSize: '17px',
								}
							}, [_h('i.ti-facebook', {
								style:{
								    verticalAlign: 'middle'
								}
							})])
						]),
						_h('li.bc-vod-display-inline-block', [
							_h('a.bc-vod-margin-small-right', {
								href: 'https://www.instagram.com/official_kukuotv/',
								attributes:{
									target: '_blank'
								},
								style:{
									fontSize: '17px',
								}
							}, [_h('i.ti-instagram', {
								style:{
								    verticalAlign: 'middle'
								}
							})])
						])
					])
				])
			]),
			_h('p.small.no-margin.pull-right.sm-pull-reset', [
				_h('span.sm-block', [
					_h('a.m-l-10.m-r-10', {
						href: '/terms'
					}, [String('Terms & Conditions')]),
					String('|'),
					_h('a.m-l-10.m-r-10', {
						href: '/policy'
					}, [String('Privacy Policy')])
				])
			]),
			_h('div.clearfix')
		])
	]);
};

module.exports = Render;