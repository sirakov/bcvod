var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	Freezer = require('freezer-js');

var Videos = function(dom, prerender, content) {

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (prerender) {

		var target = content.querySelector('.bc-vod-videos-div');

		obj.target = target;

	}

	_this._dom = new Freezer(dom);

	_this.loop = mainLoop(_this._dom.get().toJS(), Render, obj);

	if (!prerender) content.appendChild(_this.loop.target);

	_this.$ = require('sprint-js');
	
	_this._initEvents();
};

Videos.prototype = {
	_initEvents: require('./events'),
	_loadOne: require('./loadOne')
};

module.exports = Videos;