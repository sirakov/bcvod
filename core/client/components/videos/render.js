var _h = require('virtual-dom/h'),
	map = require('fast.js/array/map'),
	contains = require('mout/array/contains'),
    icons = {
    	'local': 'ti-server',
    	's3': 'ti-cloud',
    	'live': 'ti-control-play'
    };

var Render = function(obj){

	var videos,
		LoadMore;

	var Title = _h('div.bc-vod-grid.bc-vod-grid-small.bc-vod-margin-large-top', [
			_h('div.bc-vod-margin-bottom.bc-vod-width-small-1-1.bc-vod-width-medium-3-4.bc-vod-width-large-2-4.bc-vod-container-center', [
				_h('h2.p-b-2', [String('Videos')])
			])
		]),
		Counts = _h('div.bc-vod-grid.bc-vod-grid-small.bc-vod-margin-top', [
			_h('ul.nav.nav-tabs.bc-vod-container-center.bc-vod-width-small-1-1.bc-vod-width-medium-3-4.bc-vod-width-large-2-4', [
				_h('li', {
					className: (!obj.public) ? 'active' : undefined
				}, [
					_h('a', {
						href: '/admin'
					}, [String('Drafts ('+ obj.counts.drafts +')')])
				]),
				_h('li', {
					className: (obj.public) ? 'active' : undefined
				}, [
					_h('a', {
						href: '/admin/public'
					}, [String('Public ('+ obj.counts.public +')')])
				])
			])
		]);

	if (obj.videos.length) {

		videos = map(obj.videos, function(video){

			var href,
				DeleteButton,
				PublishButton;

			if (obj.public) href = '/admin/v/' + video.vid + '/edit?public=true';
			else href = '/admin/v/' + video.vid + '/edit';

			if (contains(['live', 'local'], video.status)) {

				DeleteButton = _h('button.btn.btn-danger.btn-xs', {
					attributes:{
						'data-action': 'delete',
						'data-vid': video.vid
					}
				}, [String('Delete')]);

			}

			if (video.state === 'draft') {

				PublishButton = _h('button.btn.btn-info.btn-xs', {
					attributes:{
						'data-action': 'publish',
						'data-vid': video.vid
					}
				}, [String('Publish')]);

			} else if (video.state === 'public') {

				PublishButton = _h('button.btn.btn-info.btn-xs', {
					attributes:{
						'data-action': 'unpublish',
						'data-vid': video.vid
					}
				}, [String('Unpublish')]);

			}

			return _h('li.bc-vod-video-list-item', [
				_h('h5.p-b-5.m-b-5.bold', [
					_h('a', {
						href: href
					}, [String(video.title)])
				]),
				_h('div.meta.m-b-10', [
					String((video.views || 0) + ' views')
				]),
				_h('section.controls.btn-group', [
					PublishButton,
					DeleteButton
				])
			]);
		});

	} else {

		videos = [
			_h('div.bc-vod-width-1-3.bc-vod-margin-top.bc-vod-text-center.bc-vod-container-center', [
				String('No videos found yet. '),
				_h('a', {
					href: '/admin/upload'
				}, [String('Add')])
			])
		];

		return _h('div.bc-vod-videos-div.bc-vod-container-center.bc-vod-container.bc-vod-margin-large-bottom.bc-vod-main-div', [
			Title,
			Counts,
			_h('section.content-list.bc-vod-grid.bc-vod-grid-small.bc-vod-margin-large-top', videos)
		]);

	}

	if (obj.last && obj.counts[(obj.public) ? 'public': 'drafts'] > 15) {

		LoadMore = _h('button.ladda-button.btn.btn-primary.bc-vod-container-center.bc-vod-display-block.bc-vod-margin-large-top', {
			attributes:{
				'data-action': 'load-more',
				'data-style': 'expand-left',
				'data-spinner-color': '#fff'
			}
		}, [
			_h('span.ladda-label', [String('Load More')]),
			_h('span.ladda-spinner'),
			_h('div.ladda-progress', {
				style:{
					width: '0px'
				}
			})
		]);

	} else {

		LoadMore = undefined;

	}

	return _h('div.bc-vod-videos-div.bc-vod-container-center.bc-vod-container.bc-vod-margin-large-bottom.bc-vod-main-div', [
			Title,
			Counts,
			_h('div.bc-vod-grid.bc-vod-grid-small', [
				_h('ul.bc-vod-list.bc-vod-list-line.bc-vod-list-space.bc-vod-width-small-1-1.bc-vod-width-medium-3-4.bc-vod-width-large-2-4.bc-vod-container-center.bc-vod-margin-top', videos)
			]),
			LoadMore
		]);
};

module.exports = Render;