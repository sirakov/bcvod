var Delegator = require('dom-delegate'),
	Overlay = require('../overlay'),
	http = require('../http'),
	findIndex = require('mout/array/findIndex'),
	qs = require('mout/queryString/encode'),
	ladda = require('ladda');

var Events = function(){

	var _this = this;

	var _BodyDelegator = new Delegator(document.body);

	_BodyDelegator.on('click', 'button[data-action="publish"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-vid]').dom[0],
			vid = el.dataset.vid,
			dom = _this._dom.get(),
			vidIndex = findIndex(dom.videos, {
				vid: vid
			}),
			video = dom.videos[vidIndex],
			live = (video.status === 'live') ? true: false,
			countHolder = (dom.public) ? 'public': 'drafts',
			counts = dom.counts.toJS();

		http({
			url: '/admin/v/'+ vid + '/publish',
			method: 'post'
		}, function(err, res){

			if (err) return;

			_this._dom.get().videos.splice(vidIndex, 1);

			counts['drafts']--;
			counts['public']++;

			_this._dom.get().counts.set(counts);

			_this.loop.update(_this._dom.get().toJS());

			return _this._loadOne();

		});
	});

	_BodyDelegator.on('click', 'button[data-action="unpublish"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-vid]').dom[0],
			vid = el.dataset.vid,
			dom = _this._dom.get(),
			vidIndex = findIndex(dom.videos, {
				vid: vid
			}),
			video = dom.videos[vidIndex],
			live = (video.status === 'live') ? true: false,
			countHolder = (dom.public) ? 'public': 'drafts',
			counts = dom.counts.toJS();

		http({
			url: '/admin/v/'+ vid + '/publish',
			method: 'delete'
		}, function(err, res){

			if (err) return;

			_this._dom.get().videos.splice(vidIndex, 1);

			counts['drafts']++;
			counts['public']--;

			_this._dom.get().counts.set(counts);

			_this.loop.update(_this._dom.get().toJS());

			return _this._loadOne();

		});
	});

	_BodyDelegator.on('click', 'button[data-action="delete"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-vid]').dom[0],
			vid = el.dataset.vid,
			dom = _this._dom.get(),
			vidIndex = findIndex(dom.videos, {
				vid: vid
			}),
			video = dom.videos[vidIndex],
			live = (video.state === 'public') ? true: false,
			countHolder = (dom.public) ? 'public': 'drafts',
			counts = dom.counts.toJS();

		var _overlay = Overlay.add({
					type: 'delete'
				}, {
				delete: function(){

					_overlay._destroy();

					http({
						url: '/admin/v/'+ vid + '/edit?live=' + live,
						method: 'delete'
					}, function(err, res){

						if (err) return;

						_this._dom.get().videos.splice(vidIndex, 1);

						counts[countHolder]--;

						_this._dom.get().counts.set(counts);

						_this.loop.update(_this._dom.get().toJS());

						return _this._loadOne();

					});
				}
			});

		_overlay.loop.target.focus();

	});

	_BodyDelegator.on('click', 'button[data-action="load-more"]', function(e){

		var el = (e.target.tagName.toLowerCase() === 'button') ? e.target: _this.$(e.target).parents('button[data-action="load-more"]').dom[0],
			l = ladda.create(el),
			dom = _this._dom.get(),
			path = (dom.public) ? '/admin/public' : '/admin';

		l.start();

		http({
			url: path + qs(_this._dom.get().last),
			method: 'get'
		}, function(err, res){

			if (err) {

				l.stop();
				l.remove();

				_this.loop.update(_this._dom.get().toJS());

				return;

			}
			
			var currentVideos = _this._dom.get().videos.toJS(),
				videos = res.data.videos;

			if (res.data.last) _this._dom.get().last.set(res.data.last);
			else _this._dom.get().remove('last');

			_this._dom.get().videos.set(currentVideos.concat(videos));

			l.stop();
			l.remove();

			_this.loop.update(_this._dom.get().toJS());


		});

	});
	
	_this._EventListeners = [_BodyDelegator];
};

module.exports = Events;