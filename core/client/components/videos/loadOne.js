var qs = require('mout/queryString/encode'),
	http = require('../http');

var LoadOne = function(){
	
	var _this = this,
		dom = _this._dom.get(),
		path = (dom.public) ? '/admin/public' : '/admin',
		query;

	if (!dom.last) return;

	query = _this._dom.get().last.toJS();

	query.limit = 1; 

	http({
		url: path + qs(query),
		method: 'get'
	}, function(err, res){

		if (err) return;
		
		var currentVideos = _this._dom.get().videos.toJS(),
			videos = res.data.videos;

		if (res.data.last) _this._dom.get().last.set(res.data.last);
		else _this._dom.get().remove('last');

		_this._dom.get().videos.set(currentVideos.concat(videos));

		_this.loop.update(_this._dom.get().toJS());

	});

};

module.exports = LoadOne;