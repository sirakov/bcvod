var _h = require('virtual-dom/h'),
	style = require('./style'),
	og = require('./og'),
	truncate = require('mout/string/truncate'),
	defaultDesc = 'KukuoTV is an ad-supported video on demand streaming service dedicated '+
	'to offering premium African entertainment. Our content includes African movies, African'+ 
	'Television series and music of African origin.';

var Render = function(obj){

	var title = '',
		desc = '',
		Description,
		OgDescription,
		PreviewImage,
		ImgUrl;

	if (!obj.title.length) title = obj.brandName;
	else title += truncate(obj.title, 55, null, true) + ' — ' + obj.brandName;

	if (obj.description) desc = truncate(obj.description, 115, null, true);
	else desc = '';

	// if (desc.length) {

		Description = _h('meta', {
			attributes:{
				'name': 'description',
				content: desc
			}
		});

		OgDescription = _h('meta', {
			attributes:{
				'property': 'og:description',
				content: desc
			}
		});

	// }

	if (obj.image) {

		PreviewImage = og('og:image', obj.imgsUrl + '/' + obj.image);

		ImgUrl = obj.imgsUrl + '/' + obj.image;

	} else {

		PreviewImage = og('og:image', obj.assetsUrl + obj._a['/images/default-preview.jpg'] + '?' + obj.build);

		ImgUrl = obj.assetsUrl + obj._a['/images/default-preview.jpg'] + '?' + obj.build;

	}

	return _h('head', [
		_h('meta', {
			attributes:{
				'http-equiv': 'Content-Type',
				content: 'text/html; charset=utf-8'
			}
		}),
		_h('meta', {
			attributes:{
				name: 'viewport',
				content: 'width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no'
			}
		}),
		_h('meta', {
			attributes:{
				'charset': 'UTF-8'
			}
		}),
		_h('title', [String(title)]),
		Description,
		og('og:url', obj.baseUrl + obj.path),
		og('og:site_name', obj.brandName),
		og('og:title', title),
		PreviewImage,
		og('fb:app_id', obj.facebookAppId),
		OgDescription,
		og('og:type', (obj.video) ? 'video': 'website'),
		_h('meta', {
			attributes:{
				name: 'twitter:card',
				'content': 'summary'
			}
		}),
		_h('meta', {
			attributes:{
				name: 'twitter:title',
				'content': title
			}
		}),
		_h('meta', {
			attributes:{
				name: 'twitter:description',
				'content': desc
			}
		}),
		_h('meta', {
			attributes:{
				name: 'twitter:image',
				'content': ImgUrl
			}
		}),
		style('https://fonts.googleapis.com/css?family=Montserrat:400,700'),
		style(obj.assetsUrl + obj._a['/stylesheets/main-base.css'] + '?' + obj.build),
		_h('style.vjs-styles-defaults')
	]);
};

module.exports = Render;