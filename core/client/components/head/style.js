var _h = require('virtual-dom/h');

var StyleSheet = function(href){
	return _h('link', {
		attributes: {
			'rel': 'stylesheet',
			'href': href
		}
	})	
};

module.exports = StyleSheet;