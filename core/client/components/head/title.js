var truncate = require('mout/string/truncate');

module.exports = function(title, brandName){

	var newTitle = '';

	if (!title.length) newTitle = brandName;
	else newTitle += truncate(title, 55, null, true) + ' — ' + brandName;

	return newTitle;

};