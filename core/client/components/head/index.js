var diff = require('virtual-dom/diff'),
	patch = require('virtual-dom/patch'),
	createElement = require('virtual-dom/create-element'),
	mainLoop = require('main-loop'),
	Render = require('./render'),
	merge = require('mout/object/merge'),
	Freezer = require('freezer-js'),
	pick = require('mout/object/pick');

var Head = function(dom, target, custom){

	var _this = this,
		obj = {
			create: createElement,
			diff: diff,
			patch: patch
		};

	if (target) obj.target = target;

	_this.loop = mainLoop(dom, Render, obj);

	if (custom) {

		var customLoop = _this.loop.update;

		_this._dom = new Freezer(pick(dom, [
			'title',
			'image',
			'baseUrl',
			'path',
			'description',
			'facebookAppId',
			'assetsUrl',
			'imgsUrl',
			'build',
			'_a',
			'brandName',
			'video'
		]));

		_this.loop.update = function(obj){

			if (!obj.video) obj.video = false;

			var newObj = merge(_this._dom.get().toJS(), obj);

			return customLoop.call(this, newObj);

		};

	}
	
};

module.exports = Head;

module.exports.client = function(obj){
	var _head = document.head,
		title = _head.querySelector('title');

	obj.title = title.textContent;
	
	return new Head(obj, _head, true);
	
};