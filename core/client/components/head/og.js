var _h = require('virtual-dom/h');

var StyleSheet = function(prop, content){
	return _h('meta', {
		attributes: {
			'property': prop,
			'content': content
		}
	})	
};

module.exports = StyleSheet;