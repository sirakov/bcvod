'use strict';

var Router = require('./Router'),
	ready = require('domready'),
	tagH = 'body.ie.firefox.safari.chrome.opera.window.linux.ubuntu.mac-os',
	tagB = 'html';

(function(window){

	ready(function(){

		require('es6-promise').polyfill();
		require('html5-history-api');
		require('html5-history-api/history.ielte7');
		require('classlist-polyfill');

		window.GLOBALS.supportsFileApi = (window.File && window.FileReader) ? true : false;

		window.ga = window.ga || function(){
			(ga.q=ga.q||[]).push(arguments)
		};

		window.ga.l = +new Date;

		var Load = function (e){

			console.log('Booting Kukuotv');

			if (window.GLOBALS.env === 'development') {

				window.router = new Router({
					win: window,
					doc: window.document
				});

			} else {

				new Router({
					win: window,
					doc: window.document
				});

			}

		};

		return Load();

		// (function(d, b, s, id) {
		// 	var js, fjs = b.querySelectorAll(s)[3];
		// 	if (b.querySelector(id)) return;
		// 	js = d.createElement(s); js.id = id;
		// 	js.src = '//imasdk.googleapis.com/js/sdkloader/ima3.js';
		// 	js.async = true;
		// 	js.onload = Load;
		// 	js.onerror = Load;
		// 	fjs.parentNode.insertBefore(js, fjs);
		// }(document, document.body, 'script', 'ima-sdk'))

	});

})(window);
