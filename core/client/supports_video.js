module.exports = function supports_video() {
  return !!document.createElement('video').canPlayType;
};