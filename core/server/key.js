var fs = require('fs-extra'),
	crypto = require('crypto'),
	config = require('../../config/' + process.env.NODE_ENV);

var ServerKey = function(){

	if (config.srvKey) return;

	logger.info('Generating Server Key');

	config.srvKey = crypto.randomBytes(24).toString('base64');

	fs.outputFileSync('config/'+ process.env.NODE_ENV +'.js', 'module.exports = ' + JSON.stringify(config, null, 4));

	logger.info('Server Key generated!');
	
};

module.exports = ServerKey;