module.exports = function(id){

    if (typeof id === 'string') {

        var split = id.split('-');

        return {
            savedIn: (split.length === 3) ? '/' + split[0] + '/' + split[1] : null,
            uid: (split.length === 3) ? split[2] : null
        };

    } else if (typeof id === 'object') {

        var Id = id.savedIn.split('/');

        Id.shift();

        Id.push(id.uid);

        return Id.join('-').replace('draft-', '');

    }

};