var checkAdmin = function(uid){

	var split = uid.split('-');

	return parseInt(split[split.length - 1]) === 0;

};

module.exports = checkAdmin;