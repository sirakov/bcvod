module.exports = function(id){

    if (typeof id === 'string') {

        var split = id.split('-');

        return {
            savedIn: '/' + split[0] + '/' + split[1],
            vid: split[2]
        };

    } else if (typeof id === 'object') {

        var Id = id.savedIn.split('/');

        Id.shift();

        Id.push(id.vid || id.uid);

        return Id.join('-').replace('draft-', '');

    }

};