var crypto = require('crypto');

module.exports = function(n){

	return crypto.randomBytes(Math.ceil(n/2)).toString('hex').slice(0,n);
	
};