var moment = require('moment'),
	params = require('./params'),
	_base = config.get('url'),
    _thumbnailsUrl = config.get('thumbnailsUrl'),
    truncate = require('mout/string/truncate'),
    capitalize = require('../../client/components/header/capitalize'),
	doc;

function addText(doc, el, str) {
    var text = doc.createTextNode(str);
    el.appendChild(text);
}

function isDefined(x) {
    if (x === '' || x === null || x === undefined || x === NaN) {
        return false;
    }
    return true;
}

function createEl(doc, type, attributes) {
    var el;
    if (isDefined(type)) {
        el = doc.createElement(type);
        if (isDefined(attributes)) {
            var attr;
            for (attr in attributes) {
                el.setAttribute(attr, attributes[attr]);
            }
        }
        return el;
    }
}

var BuildToUrl = function(doc, _item) {
	
	var vid = params({
 			vid: _item.vid,
 			savedIn: _item.savedIn
 		}),
		mod = moment(_item.updatedAt).format('YYYY-MM-DD'),
		catName = (_item.category || '').split('/')[1],
		_tags = (_item.keywords || []).slice(0, 31);

	var url = createEl(doc, 'url');
        loc = createEl(doc, 'loc'),
        video = createEl(doc, 'video:video'),
        video_title = createEl(doc, 'video:title'),
        video_description = createEl(doc, 'video:description'),
		video_player_loc = createEl(doc, 'video:player_loc'),
		video_duration = createEl(doc, 'video:duration'),
		video_thumbnail = createEl(doc, 'video:thumbnail_loc'),
		video_views = createEl(doc, 'video:view_count'),
		video_category = createEl(doc, 'video:category'),
		video_pubDate = createEl(doc, 'video:publication_date');

    addText(doc, video_title, truncate(_item.title, 100));
    
    addText(doc, loc, _base + '/v/' + vid);
    
    addText(doc, video_views, _item.views || 0);

    addText(doc, video_pubDate, _item.createdAt);
    
    if (_item.description) addText(doc, video_description, truncate(_item.description, 2048));
    
    addText(doc, video_player_loc, _base + '/v/' + vid + '?embed=true');
    
    addText(doc, video_duration, ((_item.duration || 0) / 1000));

    if (_item.thumbnail) addText(doc, video_thumbnail, _thumbnailsUrl + '/' + _item.thumbnail);

    if (catName) addText(doc, video_category, capitalize(catName));

    url.appendChild(loc);
    url.appendChild(video);
    
    if (_item.thumbnail) video.appendChild(video_thumbnail);
    
    video.appendChild(video_title);
    
    if (_item.description) video.appendChild(video_description);
    
    video.appendChild(video_player_loc);

    video.appendChild(video_duration);

    video.appendChild(video_views);

    video.appendChild(video_pubDate);

    _.forEach(_tags, function(_tag){

    	var video_tag = createEl(doc, 'video:tag');

    	addText(doc, video_tag, _tag);

    	video.appendChild(video_tag);

    })

    if (catName) video.appendChild(video_category);

    return url;

};

module.exports = BuildToUrl;