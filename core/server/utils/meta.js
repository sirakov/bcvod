module.exports = function(obj){

	if (!obj.title) obj.title = 'Untitled';

	if (!obj.description) obj.description = 'No description yet';

	return obj;
	
};