var moment = require('moment'),
	now = moment().format('YYYY-MM-DD');

module.exports = function(hostname, sitemaps, cb){
		
	var xml = [];

	xml.push('<?xml version="1.0" encoding="UTF-8"?>');
	xml.push('<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

	sitemaps.forEach(function (sitemap, index) {
		xml.push('<sitemap>');
		xml.push('<loc>' + hostname + sitemap + '</loc>');
		xml.push('<lastmod>' + now + '</lastmod>');
		xml.push('</sitemap>');
	});

	xml.push('</sitemapindex>');

	if (cb) return cb(xml.join('\n'));

};