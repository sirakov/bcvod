var os = require('os');

module.exports = function(){
	if (_.contains(['Sirakov'], os.hostname())) process.env.NODE_ENV = 'development';
	else process.env.NODE_ENV = 'production';
};
