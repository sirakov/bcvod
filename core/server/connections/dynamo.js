var vogels = require('vogels'),
	AWS = vogels.AWS;

vogels.AWS.config.update(config.get('aws.dynamodb'));

if (process.env.NODE_ENV === 'production') {

	vogels.dynamoDriver(new AWS.DynamoDB());

} else {

	vogels.dynamoDriver(new AWS.DynamoDB({
		endpoint: new AWS.Endpoint(config.get('connections.dynamodb.endpoint'))
	}));

}

module.exports = vogels;