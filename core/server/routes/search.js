var express = require('express'),
	router = express.Router(),
	toString = require('vdom-to-html'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Search = require('../../client/components/search/render'),
	Main = require('../../client/components/main'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	Video = require('../models/Video'),
	params = require('../utils/params'),
	Freezer = require('freezer-js');

router.route('/').get(function(req, res){

	if (!req.xhr) {

		res.type('html');

		res.write('<!DOCTYPE html><html lang="en">' + toString(Head({
			title: 'Search',
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');
		res.write('<div class="loadingBar"></div>');

	}

	res.locals.filters = {};

	var last = {},
		lastLen = Object.keys(req.query).length;

	if (lastLen) {
		
		last = _.pick(req.query, ['vid', 'savedIn', 'idt']);

		// obj.filters = filters = _.pick(req.query, ['genres', 'language', 'origin', 'year']);

		// for (key in obj.filters) {

		// 	if (typeof obj.filters[key] === 'object') obj.filters[key] = obj.filters[key][0];

		// }

	}

	Video.search(req.query.q, last).then(function(data){

		if (data.last) {

			res.locals.last = data.last;

		}

		res.locals.results = new Freezer(_.map(_.pluck(data.videos, 'attrs'), function(video){

			video.vid = params({
	            savedIn: video.savedIn,
	            vid: video.vid
	        });

	        return video;

		}));

		if (req.query.q) res.locals.filters.q = req.query.q;

		if (req.xhr) {

			return res.json({
				results: res.locals.results.get().toJS(),
				filters: res.locals.filters,
				last: res.locals.last
			});

		}

		res.write(toString(Main([Header(res.locals), Search({
			results: res.locals.results.get().toJS(),
			assetsUrl: res.locals.assetsUrl,
			imgsUrl: res.locals.imgsUrl,
			defaultPoster: res.locals.defaultPoster,
			last: res.locals.last,
			filters: res.locals.filters,
			brandName: res.locals.brandName
		})])));

		res.locals.results = res.locals.results.get().toJS();
		
		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));

		res.write(Script('/javascripts/main-base.bundle.js'));
		
		return res.end('</body></html>');

	}).catch(function(err){

		if (err) return logger.error(err);

	});
	
});

module.exports = router;