var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	HomePage = require('../../client/components/homepage/render'),
	Video = require('../models/Video'),
	setParams = require('../utils/params'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html'),
	Promise = require('bluebird'),
	cache = require('../services/cache'),
	Freezer = require('freezer-js');

router.route('/').get(function(req, res){

	if (!req.xhr) {

		res.type('html');

		res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
			title: '',
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

		res.write('<div class="loadingBar"></div>');

		res.locals.backgroundMix = true;

		var _header = Header(res.locals);

	}

	cache.fetch('homepage').otherwise(function(deferred, cacheKey) {

		var obj = {
			limit: 12,
			type: 'recently'
		},
		lastLen = Object.keys(req.query).length;

		if (lastLen) {

			obj.last = _.pick(req.query, ['vid', 'savedIn', 'idt']);

		}
		var Latest = Video.latest(obj),
			Popular = Video.popular(),
			Genres = Video.getGenres();

		Promise.all([Latest, Popular, Genres]).spread(function(data, popular, genres){

			deferred.resolve({
				videos: _.pluck(data.videos, 'attrs'),
				last: data.last,
				popular: popular,
				genres: genres
			});

		}).catch(function(err){

			if (err) logger.error(err);

			deferred.resolve(null);

		});

	}).then(function(model) {

		// console.log(req, res);

	    res.locals.genres = new Freezer(_.compact(model.genres));

		res.locals.popular = new Freezer(_.map(model.popular, function(video){

			var al = setParams(video.vid);

			if (al.savedIn && al.vid) return video;

			video.vid = setParams({
                savedIn: video.savedIn,
                vid: video.vid
            });

            return video;

		}));

		// res.locals.releases = _.map(_.pluck(release.videos, 'attrs'), function(video){

		// 	video.vid = setParams({
  //               savedIn: video.savedIn,
  //               vid: video.vid
  //           });

  //           return video;

		// });

		if (model.last) {

			res.locals.last = model.last;

		}

		res.locals.videos = new Freezer(_.map(model.videos, function(video){

			var al = setParams(video.vid);

			if (al.savedIn && al.vid) return video;

			video.vid = setParams({
                savedIn: video.savedIn,
                vid: video.vid
            });

            return video;

		}));

		// console.log(res.locals.videos, 'popu', res.locals.popular, 'genres', res.locals.genres);

		if (!res.locals.videos.get().length && !req.xhr) return res.redirect('/admin');

		if (req.xhr) {

			return res.json({
				videos: res.locals.videos.get().toJS(),
				last: res.locals.last,
				popular: res.locals.popular.get().toJS(),
				genres: res.locals.genres.get().toJS()
			});

		}

		var Home = HomePage({
			videos: res.locals.videos.get().toJS(),
			assetsUrl: res.locals.assetsUrl,
			imgsUrl: res.locals.imgsUrl,
			defaultPoster: res.locals.defaultPoster,
			last: res.locals.last,
			popular: res.locals.popular.get().toJS(),
			genres: res.locals.genres.get().toJS()
		});

		res.locals.videos = res.locals.videos.get().toJS();
		res.locals.popular = res.locals.popular.get().toJS();
		res.locals.genres = res.locals.genres.get().toJS();

		res.write(toString(Main([_header, Home, Footer(res.locals)])));

		delete res.locals.backgroundMix;

		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));

		res.write(Script('/javascripts/main-base.bundle.js'));

		res.end('</body></html>');

	}).fail(function() {

	    return res.status(500).end();

	});

});

module.exports = router;
