var express = require('express'),
	router = express.Router(),
	Head = require('../../client/components/head/render'),
	Main = require('../../client/components/main'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	History = require('../../client/components/history/render'),
	Profile = require('../../client/components/profile/render'),
	Video = require('../models/Video'),
	User = require('../models/User'),
	UserHistory = require('../models/UserHistory'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html'),
	async = require('async'),
	getParams = require('../utils/params'),
	freezer = require('freezer-js');

router.route('/').post(require('../services/userUpload'), function(req, res){

	if (!req.user && !req.frontEndUser) return res.status(400).end();

	var params = getParams((req.user)? req.user.uid: req.frontEndUser);

    if (!params.vid && !params.savedIn) return next();

    var toUpdate = {
        uid: params.vid,
        savedIn: params.savedIn,
        fullname: (req.body.fullname) ? req.body.fullname: null,
        location: (req.body.location) ? req.body.location: null
    };

    if (req.file) {

        toUpdate.avatar = config.get('assetsUrl') + '/avatars/' + req.file.fileId;

    }

    User.update(toUpdate, function(err, data){

        if (err) return logger.error(err);

        return res.json(_.pick(data.get(), [
			'uid',
			'savedIn',
			'username',
			'fullname',
			'avatar',
			'location'
		]));

    });



});

router.route('/history').get(function(req, res){

	if (!req.user && !req.frontEndUser) return res.redirect('/');

	if (!req.xhr) {

		res.type('html');

        res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
            title: 'History',
            facebookAppId: res.locals.facebookAppId,
            description: '',
            assetsUrl: res.locals.assetsUrl,
            build: res.locals.build,
            _a: res.locals._a,
            brandName: res.locals.brandName,
            baseUrl: res.locals.baseUrl,
            path: res.locals.path
        })));

        res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

        res.write('<div class="loadingBar"></div>');
		var _header = Header(res.locals);

	}

	var lastLen = Object.keys(req.query).length,
		lastKey,
		Query,
		uid = (req.user && req.user.uid) ? req.user.uid : req.frontEndUser;

	if (lastLen) {
		
		lastKey = _.pick(req.query, ['video', 'user', 'idt']);

	}

	Query = UserHistory.query(uid).usingIndex('TimeIndex').limit(8);

	if (lastKey && Object.keys(lastKey).length) {

		lastKey.idt = parseInt(lastKey.idt);

		Query = Query.startKey(lastKey);

	}

	Query.descending().exec(function(err, history){

		if (err) return logger.error(err);

		if (history.LastEvaluatedKey) res.locals.last = history.LastEvaluatedKey;

		var VideoHistory = _.pluck(_.pluck(history.Items, 'attrs'), 'video');

		VideoHistory = _.map(VideoHistory, function(history){

			history = getParams(history);

			return function(callback){

				Video.get(history, {
					AttributesToGet : [
						'title',
						'description',
						'category',
						'poster',
						'savedIn',
						'vid',
						'idt',
						'genres',
						'year',
						'origin',
						'language'
					]
				}, function(err, video){
					
					if (err) return callback(err, null);

					if (!video) {

						UserHistory.destroy({
							user: uid,
							video: getParams(history)
						}, function(err){

							if (err) return callback(err, null);

							return callback(null, null);

						});

					} else {

						var vid = video.get();

						vid.vid = getParams({
							vid: vid.vid,
							savedIn: vid.savedIn
						});

						return callback(null, vid);

					}

				});

			};

		});

		async.parallel(VideoHistory, function(err, results){

			if (err) return logger.error(err);

			var videos = new freezer(_.compact(results));

			if (req.xhr) {

				return res.json({
					videos: videos.get().toJS(),
					last: res.locals.last
				});

			}

			res.locals.videos = videos.get().toJS();

			res.write(toString(Main([
				_header,
				History(res.locals),
				Footer(res.locals)
			])));

			res.locals.videos = videos.get().toJS();

			res.write(Script(res.locals));

			res.write(Script('', {
				'type': 'text/javascript',
				'src': '//kukuotv.disqus.com/embed.js',
				'data-timestamp': '' + new Date()
			}));
			res.write(Script('', {
				'type': 'text/javascript',
				src: '//www.google-analytics.com/analytics.js'
			}));

            res.write(Script('/javascripts/main-base.bundle.js'));

            return res.end('</body></html>');

		});	

	});
	
});

router.route('/profile').get(function(req, res){

	if (!req.user && !req.frontEndUser) return res.redirect('/');

	res.type('html');

    res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
        title: res.locals._currentUser.fullname || 'Profile',
        facebookAppId: res.locals.facebookAppId,
        description: '',
        assetsUrl: res.locals.assetsUrl,
        build: res.locals.build,
        _a: res.locals._a,
        brandName: res.locals.brandName,
        baseUrl: res.locals.baseUrl,
        path: res.locals.path
    })));

    res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

    res.write('<div class="loadingBar"></div>');
	var _header = Header(res.locals);

	res.write(toString(Main([
		_header,
		Profile(res.locals._currentUser),
		Footer(res.locals)
	])));

	res.write(Script(res.locals));

	res.write(Script('', {
		'type': 'text/javascript',
		'src': '//kukuotv.disqus.com/embed.js',
		'data-timestamp': '' + new Date()
	}));
	res.write(Script('', {
		'type': 'text/javascript',
		src: '//www.google-analytics.com/analytics.js'
	}));

    res.write(Script('/javascripts/main-base.bundle.js'));

    return res.end('</body></html>');

});

module.exports = router;