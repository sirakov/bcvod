var express = require('express'),
	router = express.Router(),
	Head = require('../../client/components/head/render'),
	NotFound = require('../../client/components/errors/notFound'),
	Main = require('../../client/components/main'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html');

router.route('*').get(function(req, res){

	if (req.xhr) return res.status(404).end();

	var _header = Header(res.locals);
	
	res.type('html');
	
	res.status(404);

	res.write('<!DOCTYPE html><html lang="en">' + toString(Head({
		title: '',
		facebookAppId: res.locals.facebookAppId,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a,
		brandName: res.locals.brandName,
		baseUrl: res.locals.baseUrl,
		path: res.locals.path
	})));
	res.write('<body class="'+ BodyClass(res.locals.useragent) +'"><div class="loadingBar"></div>');
	res.write( toString( Main([ _header, NotFound(), Footer(res.locals)]) ) );

	res.write(Script(res.locals));

	res.write(Script('', {
		'type': 'text/javascript',
		'src': '//kukuotv.disqus.com/embed.js',
		'data-timestamp': '' + new Date()
	}));
	res.write(Script('', {
		'type': 'text/javascript',
		src: '//www.google-analytics.com/analytics.js'
	}));

	res.write(Script('/javascripts/main-base.bundle.js'));

	return res.end('</body></html>');
});

module.exports = router;