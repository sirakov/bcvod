var express = require('express'),
	router = express.Router(),
	Video = require('../models/Video'),
	cache = require('../services/cache');

router.route('/').get(function(req, res, next){

	// if (!req.xhr) return next();

	var obj = {
			limit: 10
		},
		lastLen = Object.keys(req.query).length,
		Se = new RegExp('series', 'g'),
		isSeries = Se.test(req.query.category);

	if (lastLen) {

		if (req.query.idt) req.query.idt = parseInt(req.query.idt);

		obj.last = _.pick(req.query, ['vid', 'savedIn', 'idt']);

	}

	Video.latest(obj).then(function(data){

		return res.json({
			latest: _.pluck(data.videos, 'attrs'),
			last: data.last
		});

	}).catch(function(err){

		if (err) logger.error(err);

		return res.status(500).end();

	});

});

module.exports = router;
