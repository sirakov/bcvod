var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Upload = require('../../client/components/upload/render'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html'),
	Video = require('../models/Video'),
	moment = require('moment'),
	uuid = require('../utils/uuid'),
	params = require('../utils/params');

router.route('/').get(function(req, res){

	res.type('html');

	res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
		title: 'Upload',
		facebookAppId: res.locals.facebookAppId,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a,
		brandName: res.locals.brandName,
		baseUrl: res.locals.baseUrl,
		path: res.locals.path
	})));

	res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

	res.write('<div class="loadingBar"></div>');

	var _header = Header(res.locals);

	res.write(toString(Main([_header, Upload({files:[]})])));

	res.write(Script(res.locals));

	res.write(Script('', {
		'type': 'text/javascript',
		'src': '//kukuotv.disqus.com/embed.js',
		'data-timestamp': '' + new Date()
	}));
	res.write(Script('', {
		'type': 'text/javascript',
		src: '//www.google-analytics.com/analytics.js'
	}));
	
	res.write(Script('/javascripts/main-base.bundle.js'));

	return res.end('</body></html>');
	
}).post(function(req, res){

	var uid = uuid(12),
        savedIn = moment().format('[/]YYYY[/]MM');

	Video.create({
		savedIn: '/draft' + savedIn,
		vid: uid,
		state: 'draft',
		title: req.body.title,
		description: req.body.description,
		idt: Date.now(),
		category: '/draft/series' + savedIn,
		status: 'local'
	}, function(err, vid){

		if (err) return logger.error(err);

		return res.json({
			success: true,
			vid: params({
				vid: vid.get('vid'),
				savedIn: vid.get('savedIn')
			})
		});

	});


});

module.exports = router;