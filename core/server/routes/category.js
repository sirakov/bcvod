var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	Category = require('../../client/components/category/render'),
	capitalize = require('../../client/components/header/capitalize'),
	Video = require('../models/Video'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html'),
	setParams = require('../utils/params'),
	Freezer = require('freezer-js');

router.route('/:category').get(function(req, res, next){

	if (!req.xhr) {

		res.type('html');

		res.locals.name = capitalize(req.params.category);

		res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
			title: capitalize(req.params.category),
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

		res.write('<div class="loadingBar"></div>');

		var _header = Header(res.locals);

	}

	var obj = {
		limit: 12,
		prefix: '/' + req.params.category
	},
	lastLen = Object.keys(req.query).length,
	filters;

	if (lastLen) {
		
		obj.last = _.pick(req.query, ['vid', 'savedIn', 'idt', 'category']);

		obj.filters = filters = _.pick(req.query, ['genres', 'language', 'origin', 'year']);

		for (key in obj.filters) {

			if (typeof obj.filters[key] === 'object') obj.filters[key] = obj.filters[key][0];

		}

	}

	Video.latest(obj).then(function(data){


		if (data.last) {

			res.locals.last = data.last;

		}

		var videos = _.map(_.pluck(data.videos, 'attrs'), function(video){

			video.vid = setParams({
                savedIn: video.savedIn,
                vid: video.vid
            });

            return video;

		});

		res.locals.videos = new Freezer(videos);

		if (!filters || !Object.keys(filters).length) {

			filters = {};

		}

		res.locals.filters = filters;

		// if (!videos.length && !req.xhr) return next();

		if (req.xhr) {

			return res.json({
				videos: res.locals.videos.get().toJS(),
				last: res.locals.last,
				filters: res.locals.filters
			});

		}

		var Cat = Category({
			name: capitalize(req.params.category),
			videos: res.locals.videos.get().toJS(),
			assetsUrl: res.locals.assetsUrl,
			imgsUrl: res.locals.imgsUrl,
			defaultPoster: GLOBALS.get().toJS().defaultPoster,
			last: res.locals.last,
			filters: res.locals.filters
		});

		res.write(toString(Main([_header, Cat, Footer(res.locals)])));

		res.locals.videos = res.locals.videos.get().toJS();

		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));

		res.write(Script('/javascripts/main-base.bundle.js'));

		return res.end('</body></html>');

	}).catch(function(err){

		if (err) return logger.error(err);

	});

});

module.exports = router;