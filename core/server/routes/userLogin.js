var express = require('express'),
	router = express.Router(),
	toString = require('vdom-to-html'),
	Head = require('../../client/components/head/render'),
	Login = require('../../client/components/overlay/overlays/login'),
	Main = require('../../client/components/main'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	User = require('../models/User'),
	moment = require('moment'),
	uuid = require('../utils/uuid'),
	passwordless = require('../services/passwordless')
	params = require('../utils/params');

router.route('/').get(function(req, res){

	res.type('html');

	res.write('<!DOCTYPE html><html lang="en">' + toString(Head({
		title: 'Login',
		facebookAppId: res.locals.facebookAppId,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a,
		brandName: res.locals.brandName,
		baseUrl: res.locals.baseUrl,
		path: res.locals.path
	})));
	
	res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');
	res.write('<div class="loadingBar"></div>');

	res.write(toString(Main([Login({
		path: req.path,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a
	})])));
	
	res.write(Script(res.locals));

	res.write(Script('', {
		'type': 'text/javascript',
		'src': '//kukuotv.disqus.com/embed.js',
		'data-timestamp': '' + new Date()
	}));
	res.write(Script('', {
		'type': 'text/javascript',
		src: '//www.google-analytics.com/analytics.js'
	}));

	res.write(Script('/javascripts/main-base.bundle.js'));
	
	return res.end('</body></html>');
	
}).post(
	passwordless.requestToken(function(user, delivery, callback) {
        return User.query(user)
					.limit(1)
					.attributes(['uid', 'savedIn'])
					.usingIndex('EmailIndex')
					.exec(function(err, data){
						
						if (err) {

							return callback(err, null);

						} else if (!data.Items.length)  {

							User.create({
								uid: uuid(12),
								savedIn: moment().format('[/]YYYY[/]MM'),
								type: '/email',
								email: user,
								avatar: config.get('assetsUrl') + config.get('_a')['/images/user-image.png'],
								startedAt: Date.now()
							}, function(err, use){

								if (err) return callback(err, null);

								var id = params( {
										uid: use.get('uid'),
										savedIn: use.get('savedIn')
									});

								return callback(null, id + '-1');

							});

						} else {

							var u = data.Items[0],
								id = params( {
									uid: u.get('uid'),
									savedIn: u.get('savedIn')
								});

							return callback(null, id + '-1');

						}

					});
    }, {
    	originField: 'r'
    }),
	function(req, res){

		// console.log(req.xhr);

		return res.json({success: true});

	});

module.exports = router;