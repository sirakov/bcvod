var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Video = require('../models/Video'),
	Videos = require('../../client/components/videos/render'),
	setParams = require('../utils/params'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html'),
	Promise = require('bluebird');

router.route('/').get(function(req, res){

	var latest,
		last,
		Query = [],
		obj = {};

	obj.draft = true;
	obj.limit = 15;

	if (Object.keys(req.query).length) {
		
		obj.last = _.pick(req.query, ['vid', 'savedIn', 'idt']);

	}

	if (req.query.limit) obj.limit = parseInt(req.query.limit);

	latest = Video.latest(obj);

	Query = [latest, Video.counts()];

	Promise.all(Query).spread(function(data, counts){

		if (data.last) {

			res.locals.last = data.last;

		}

		var videos = _.map(_.pluck(data.videos, 'attrs'), function(video){

			video.vid = setParams({
                savedIn: video.savedIn,
                vid: video.vid
            });

            return video;

		});

		if (counts) res.locals.counts = {
			drafts: counts[0],
			public: counts[1]
		};

		if (req.xhr) {
			
			return res.json({
				videos: videos,
				last: res.locals.last,
				counts: res.locals.counts
			});

		}

		res.locals.videos = videos;

		res.type('html');

		res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
			title: 'Videos',
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

		res.write('<div class="loadingBar"></div>');

		var _header = Header(res.locals);

		res.write(toString(Main([_header, Videos({
			videos: videos,
			assetsUrl: res.locals.assetsUrl,
			last: res.locals.last,
			counts: res.locals.counts
		})])));

		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));

		res.write(Script('/javascripts/main-base.bundle.js'));

		return res.end('</body></html>');

	}).catch(function(err){

		return logger.error(err);

	});

});

router.route('/public').get(function(req, res){

	var latest,
		last,
		Query = [],
		obj = {};

	obj.limit = 15;

	if (Object.keys(req.query).length) {
		
		obj.last = _.pick(req.query, ['vid', 'savedIn', 'idt']);

	}

	if (req.query.limit) obj.limit = parseInt(req.query.limit);

	latest = Video.latest(obj);

	Query = [latest, Video.counts()];

	Promise.all(Query).spread(function(data, counts){

		if (data.last) {

			res.locals.last = data.last;

		}

		var videos = _.map(_.pluck(data.videos, 'attrs'), function(video){

			video.vid = setParams({
                savedIn: video.savedIn,
                vid: video.vid
            });

            return video;

		});

		if (counts) res.locals.counts = {
			drafts: counts[0],
			public: counts[1]
		};

		if (req.xhr) {
			
			return res.json({
				videos: videos,
				last: res.locals.last,
				counts: res.locals.counts,
				public: true
			});

		}

		res.locals.videos = videos;

		res.type('html');

		res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
			title: 'Videos',
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

		res.write('<div class="loadingBar"></div>');

		var _header = Header(res.locals);

		res.write(toString(Main([_header, Videos({
			videos: videos,
			assetsUrl: res.locals.assetsUrl,
			last: res.locals.last,
			counts: res.locals.counts,
			public: true
		})])));

		res.locals.public = true;

		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));

		res.write(Script('/javascripts/main-base.bundle.js'));

		return res.end('</body></html>');

	}).catch(function(err){

		return logger.error(err);

	});

});

module.exports = router;