var express = require('express'),
	router = express.Router(),
	path = require('path'),
	sm = require('sitemap'),
	moment = require('moment'),
	lastmod = moment().format('YYYY-MM-DD'),
	firstMonth = config.get('first_month_sitemap'),
	createSiteMap = require('../utils/sitemap'),
	indexPath = path.resolve(__dirname, './../../../content/sitemap'),
	Video = require('../models/Video'),
	params = require('../utils/params'),
	baseStr = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"></urlset>',
	async = require('async'),
	DOM = require('xmldom'),
	DOMParser = DOM.DOMParser,
    XMLSerializer = DOM.XMLSerializer,
    _base = config.get('url'),
    buildToUrl = require('../utils/buildToUrl'),
    categories = ['series', 'movies', 'music'],
    autoKeyWord = require('auto-keywords'),
    truncate = require('mout/string/truncate'),
    doc;

var pagesSitemap = sm.createSitemap ({
      hostname: config.get('url'),
      cacheTime: 600000,
      urls:[
      	{ url: '/', lastmod: lastmod, priority: 0.9, changefreq: 'daily'  },
      	{ url: '/category/series', lastmod: lastmod, priority: 0.9, changefreq: 'daily'  },
      	{ url: '/category/movies', lastmod: lastmod, priority: 0.9, changefreq: 'daily'  },
      	{ url: '/category/movies', lastmod: lastmod, priority: 0.9, changefreq: 'daily'  },
      	{ url: '/search', lastmod: lastmod, priority: 0.9, changefreq: 'daily'  },
      	{ url: '/contact-us', lastmod: lastmod, changefreq: 'monthly'  },
      	{ url: '/terms', lastmod: lastmod, changefreq: 'monthly'  },
      	{ url: '/policy', lastmod: lastmod, changefreq: 'monthly'  }
      ]
    });

router.route('/sitemap.xml').get(function(req, res){

	var urls = ['/sitemap/pages.xml'];


	_.forEach(categories, function(category){

		urls.push('/sitemap/'+ category + '.xml');

	});

	createSiteMap(config.get('url'), urls, function(xml){

		res.header('Content-Type', 'application/xml');

		return res.send( xml );

	});

});

router.route('/pages.xml').get(function(req, res){

	pagesSitemap.toXML( function (err, xml) {
		
		if (err)  return res.status(500).end();

		res.header('Content-Type', 'application/xml');

		return res.send( xml );

	});
	
});

router.route('/:category.xml').get(function(req, res, next){

	if (!_.contains(categories, req.params.category)) return next();

	var now = moment().format('YYYY-MM'),
		months = [],
		urls = [],
		_category = req.params.category;

	while(now !== firstMonth){

		// urls.push('/sitemap/videos-'+ now + '.xml');
		months.push(now);

		now = moment(now + '-01').subtract(1, 'month').format('YYYY-MM');

	}

	var checkMonths = _.map(months, function(month){

		month = month.split('-');
		month = month.join('/');
		month = '/' + _category + '/' + month;

		return function(cb){

			Video.query(month)
				.usingIndex('CategoryIndex')
				.select('COUNT')
				.exec(function(err, data){

					if (err) return cb(err);

					month = month.split('/');

					month = month[2] + '-' + month[3];

					if (data.Count) urls.push('/sitemap/'+ _category +'/videos-'+ month + '.xml');

					return cb();

				});

		};


	});

	async.parallel(checkMonths, function(err){

		if (err) return logger.error(err);

		createSiteMap(config.get('url'), urls, function(xml){

			res.header('Content-Type', 'application/xml');

			return res.send( xml );

		});

	});
	
});

router.route('/series/videos-:year-:month.xml').get(function(req, res, next){

	if (!req.params.year || !req.params.month) return next();

	var year = req.params.year,
		month = req.params.month,
		category = 'series',
		urlset;

	doc = new DOMParser().parseFromString(baseStr);

    urlset = doc.getElementsByTagName('urlset')[0];

	Video.query('/'+ category + '/' + year + '/' + month)
		 .attributes([
		 	'vid',
		 	'savedIn',
		 	'title',
		 	'description'
		 	])
		 .usingIndex('CategoryIndex')
		 .exec(function(err, data){

		 	if (err)  return res.status(500).end();

		 	if (!data.Items.length) return res.status(404).end();

		 	var Series = _.map(_.pluck(data.Items, 'attrs'), function(parent){

		 		return function(callback){

		 			Video.episodes(parent.savedIn + '/'+ parent.vid, [
					 	'vid',
					 	'savedIn',
					 	'createdAt',
					 	'title',
					 	'description',
					 	'duration',
					 	'thumbnail',
					 	'views',
					 	'category',
					 	'keywords'
					]).then(function(data){

					 	if (!data.length) return callback(null, []);

						callback(null, _.map(data, function(video, i){

				 			video.title = truncate(parent.title, 88) + ' — Episode ' + video.episodeId;
				 			if (!video.description) video.description = parent.description;

				 			video.keywords = autoKeyWord(video.title);
				 			video.category = '/series' + parent.savedIn;

				 			video.savedIn = parent.savedIn;
				 			video.vid = (parseInt(video.episodeId) === 1) ? parent.vid : parent.vid + '/' + video.episodeId;

				 			return video;

				 		}));

						return null;

					}).catch(function(err){

					 	callback(err, null);

					 	return null;

					});

		 		};

		 	});

			async.parallel(Series, function(err, results){

				var _results = _.flatten(results);

				if (err)  return res.status(500).end();

			 	if (!_results.length) return res.status(404).end();

		 		_.forEach(_results, function(_item){

			 		urlset.appendChild(buildToUrl(doc, _item));

			 	});

				res.header('Content-Type', 'application/xml');

				var re = new RegExp('><', 'g'),
					docContentStr = new XMLSerializer().serializeToString(doc);

		        docContentStr = docContentStr.replace(re, '>\n<');

				return res.send( docContentStr );

			});

		 });

});

// router.route('/series/:year-:month-:id.xml').get(function(req, res, next){

// 	if (!req.params.year || !req.params.month) return next();

// 	var year = req.params.year,
// 		month = req.params.month,
// 		id = req.params.id,
// 		category = 'series',
// 		urlset,
// 		urls = [];

// 	doc = new DOMParser().parseFromString(baseStr);

//     urlset = doc.getElementsByTagName('urlset')[0];

// 	Video.get({
// 		savedIn: '/' + year + '/' + month,
// 		vid: id
// 	}, function(err, parent){

// 		if (err) {

// 			logger.error(err);

// 			return res.status(500).end();

// 		}

// 		if (!parent) return res.status(404).end();

// 		parent = parent.get();

		

// 	});

// });

router.route('/:category/videos-:year-:month.xml').get(function(req, res, next){

	if (!_.contains(categories, req.params.category)) return next();

	if (!req.params.year || !req.params.month) return next();

	var year = req.params.year,
		month = req.params.month,
		category = req.params.category,
		urlset;

	doc = new DOMParser().parseFromString(baseStr);

    urlset = doc.getElementsByTagName('urlset')[0];

	Video.query('/'+ category + '/' + year + '/' + month)
		 .attributes([
		 	'vid',
		 	'savedIn',
		 	'createdAt',
		 	'title',
		 	'description',
		 	'duration',
		 	'thumbnail',
		 	'views',
		 	'category',
		 	'keywords'
		 	])
		 .usingIndex('CategoryIndex')
		 .exec(function(err, data){

		 	if (err)  return res.status(500).end();

		 	if (!data.Items.length) return res.status(404).end();

	 		_.forEach(_.pluck(data.Items, 'attrs'), function(_item){

		 		urlset.appendChild(buildToUrl(doc, _item));

		 	});

			res.header('Content-Type', 'application/xml');

			var re = new RegExp('><', 'g'),
				docContentStr = new XMLSerializer().serializeToString(doc);

	        docContentStr = docContentStr.replace(re, '>\n<');

			return res.send( docContentStr );

		 })
	
});

module.exports = router;