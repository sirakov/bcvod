var express = require('express'),
	router = express.Router(),
    AWS = require('aws-sdk'),
	Main = require('../../client/components/main'),
    Head = require('../../client/components/head/render'),
    Header = require('../../client/components/header/render'),
    VideoModule = require('../../client/components/video/render'),
    toString = require('vdom-to-html'),
    Video = require('../models/Video'),
    BodyClass = require('../../client/components/body/class'),
    Script = require('../../client/components/body/script'),
    getParams = require('../utils/params'),
    jobs = require('../services/addQueue'),
    moment = require('moment'),
    uuid = require('../utils/uuid'),
    autoKeyWord = require('auto-keywords');

router.route('/:id/edit').get(function(req, res, next){

    var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

    if (req.query.public) savedIn = params.savedIn;
    else savedIn = '/draft' + params.savedIn;

	Video.get({savedIn: savedIn,vid: params.vid}, function(err, data){
        if (err) logger.error(err);

        if (data) {

            Video.episodes(params.savedIn + '/' + params.vid).then(function(episodes){

                if (req.xhr) {

                    var vidObj = data.get(),
                        re = new RegExp('music', 'g');

                    if (!vidObj.genres) vidObj.genres = [];

                    if (re.test(vidObj.category) && vidObj.genres.length) {

                        vidObj.genres = vidObj.genres[0];

                    } else if (re.test(vidObj.category)) {

                        vidObj.genres = '';

                    }

                    if (!episodes.length) vidObj.episodes = [];
                    else vidObj.episodes = episodes;

                    vidObj.vid = getParams({
                        savedIn: vidObj.savedIn,
                        vid: vidObj.vid
                    });

                    return res.json(vidObj);

                } else {

                    var vidObj = data.get(),
                        re = new RegExp('music', 'g');

                    if (!vidObj.genres) vidObj.genres = [];

                    if (re.test(vidObj.category) && vidObj.genres.length) {

                        vidObj.genres = vidObj.genres[0];

                    } else if (re.test(vidObj.category)) {

                        vidObj.genres = '';

                    }

                    if (!episodes.length) vidObj.episodes = [];
                    else vidObj.episodes = episodes;

                    vidObj.vid = getParams({
                        savedIn: vidObj.savedIn,
                        vid: vidObj.vid
                    });

                    var vid = {
                        video: vidObj,
                        categories: res.locals.categories,
                        assetsUrl: res.locals.assetsUrl,
                        imgsUrl: res.locals.imgsUrl
                    };

                    res.type('html');

                    res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
                        title: (vidObj.title) ? vidObj.title : '',
                        facebookAppId: res.locals.facebookAppId,
                        assetsUrl: res.locals.assetsUrl,
                        build: res.locals.build,
                        _a: res.locals._a,
                        brandName: res.locals.brandName,
                        baseUrl: res.locals.baseUrl,
                        path: res.locals.path
                    })));

                    res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

                    res.write('<div class="loadingBar"></div>');

                    var _header = Header(res.locals);

                    res.write(toString(Main([_header, VideoModule(vid)])));

                    res.locals.video = vidObj;

                    res.write(Script(res.locals));

                    res.write(Script('', {
                        'type': 'text/javascript',
                        'src': '//kukuotv.disqus.com/embed.js',
                        'data-timestamp': '' + new Date()
                    }));
                    res.write(Script('', {
                        'type': 'text/javascript',
                        src: '//www.google-analytics.com/analytics.js'
                    }));

                    res.write(Script('/javascripts/main-base.bundle.js'));

                    return res.end('</body></html>');

                }

            }).catch(function(err){

                return logger.error(err);

            });

        } else {

            return next();

        }

    });
	
}).post(require('../services/upload'), function(req, res, next){

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    var toUpdate = {
        vid: params.vid,
        savedIn: (req.body.live) ? params.savedIn : '/draft' + params.savedIn,
        title: (req.body.title) ? req.body.title: null,
        description: (req.body.description) ? req.body.description: null,
        category: (req.body.category) ? req.body.category: null,
        sequel: (req.body.sequel) ? req.body.sequel: null,
        genres: (req.body.genres && req.body.genres.length) ? req.body.genres: null,
        origin: (req.body.origin) ? req.body.origin: null,
        year: (req.body.year) ? req.body.year: null,
        language: (req.body.language) ? req.body.language: null
    };

    if (toUpdate.title) toUpdate.keywords = autoKeyWord(toUpdate.title);

    if (req.file) {

        var source;

        if (req.body.imgMeta) source = JSON.parse(req.body.imgMeta);
        else source = {};

        source.src = req.file.fileId;

        toUpdate.poster = JSON.stringify(source);

    }

    //check to remove poster

    Video.update(toUpdate, function(err, data){

        if (err) return logger.error(err);

        if (!data.get().thumbnail) {

            Video.episodes(params.savedIn + '/' + params.vid).then(function(episodes){

                if (!episodes.length) return res.json(data.get());

                var _first = episodes[0];

                // console.log(_first);

                Video.get({
                    savedIn: params.savedIn + '/' + params.vid,
                    vid: _first.vid
                }, {AttributesToGet: ['thumbnail']}, function(err, episode){

                    if (err) return logger.error(err);

                    data.set({
                        thumbnail: episode.get().thumbnail
                    });

                    data.update(function(err){

                        if (err) return logger.error(err);

                        return res.json(data.get());

                    });

                });

            }).catch(function(err){

                return logger.error(err);

            })

        } else {

            return res.json(data.get());

        }

    });

}).delete(function(req, res, next){

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    var query = req.query;

    Video.get({
        vid: params.vid,
        savedIn: (query.live === 'true') ? params.savedIn : '/draft' + params.savedIn
    }, function(err, video){

        if (err) return logger.log(err);

        if (!video) return next();

        var toDelete = video.get();

        Video.destroy({
            vid: toDelete.vid,
            savedIn: toDelete.savedIn
        }, function(err){

            if (err) return logger.error(err);

            Video.destroyLikes(toDelete.savedIn + '/' + toDelete.vid).then(function(data){

                jobs('delete', toDelete);

                return res.status(200).end();

            }).catch(function(err){

                return logger.error(err);

            });

        });


    });

});

router.route('/:id/publish').post(function(req, res, next){

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    Video.get({
        savedIn: '/draft' + params.savedIn,
        vid: params.vid
    }, function(err, series){

        if (err) return logger.error(err);

        if (!series) return res.status(404).end();

        var old = series.get();

        old.savedIn = old.savedIn.replace('/draft', '');
        old.category = (old.category) ? old.category.replace('/draft', '') : '/movies' + old.savedIn;

        old.state = 'public';
        old.status = 'live';

        Video.destroy({
            savedIn: '/draft' + old.savedIn,
            vid: old.vid
        }, function(err){

            if (err) return logger.error(err);

            Video.create(old, function(err, data){

                if (err) return logger.error(err);

                return res.json({
                    video: data.get()
                });

            });

        });



    });

}).delete(function(req, res, next){

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    Video.get({
        savedIn: params.savedIn,
        vid: params.vid
    }, function(err, series){

        if (err) return logger.error(err);

        if (!series) return res.status(404).end();

        var old = series.get();

        old.savedIn = '/draft' + old.savedIn;
        old.category = (old.category) ? '/draft' + old.category: '/draft/movies' + old.savedIn.replace('/draft', '');

        old.state = 'draft';
        old.idt = Date.now();

        Video.destroy({
            savedIn: params.savedIn,
            vid: params.vid
        }, function(err){

            if (err) return logger.error(err);

            Video.create(old, function(err, data){

                if (err) return logger.error(err);

                return res.json({
                    video: data.get()
                });

            });

        });



    });

});

router.route('/:id/eps').post(function(req, res, next){

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    var epId = getParams(req.body.videoId);

    if (!epId.vid && !epId.savedIn) return next();

    if (!req.body.videoId || !req.body.episodeId) return res.status(500).end();

    if (req.body.videoId === req.params.id) return res.status(500).end();

    Video.query(params.savedIn + '/' + params.vid)
        .where('vid').equals('/episode/' + req.body.episodeId)
        .select('COUNT')
        .exec(function(err, exists){

            if (err) return logger.error(err);

            if (exists.Count !== 0) return res.status(500).end();

            Video.get({
                savedIn: epId.savedIn,
                vid: epId.vid
            }, function(err, ep){

                if (err) return logger.error(err);

                if (!ep) return res.status(500).end();

                var episodeId = req.body.episodeId;

                var old = ep.get(),
                    newEp = _.pick(old, [
                        'status',
                        'description',
                        'description_long',
                        'savedIn',
                        'size',
                        'oFile',
                        's3FileName',
                        'vid',
                        'oFileName',
                        'thumbnail',
                        'duration',
                        'transcodeId',
                        'likes',
                        'views',
                        'viewLikes'
                    ]);

                newEp.savedIn = params.savedIn + '/' + params.vid;
                newEp.vid = '/episode/' + parseInt(episodeId);
                newEp.episode = true;
                newEp.oldVid = JSON.stringify({
                    vid: old.vid,
                    savedIn: old.savedIn,
                    idt: old.idt
                });

                Video.destroy({
                    savedIn: old.savedIn,
                    vid: old.vid
                }, function(err){

                    if (err) return logger.error(err);

                    Video.create(newEp, function(err, episode){

                        if (err) return logger.error(err);

                        if (!episode) return res.status(500).end();

                        return res.json({
                            success: true,
                            episodeId: episodeId
                        });

                    });

                });

            });
        });

});

router.route('/:id/eps').put(function(req, res, next){

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    if (!req.body.episodeId) return res.status(400).end();

    Video.get({
        savedIn: params.savedIn + '/' + params.vid,
        vid: '/episode/' + req.body.episodeId
    }, function(err, ep){

        if (err) return logger.error(err);

        if (!ep) return res.status(404).end();

        var old = ep.get(),
            savedIn = moment().format('[/]YYYY[/]MM'),
            oldVid = (old.oldVid) ? JSON.parse(old.oldVid) : {};

        old.title = req.body.title + ' Episode ' +  req.body.episodeId;
        // old.description = req.body.title + ' Episode ' +  req.body.episodeId;
        old.savedIn = '/draft' + (oldVid.savedIn || savedIn);
        old.vid = oldVid.vid || uuid(12);
        old.category = '/draft/movies' + (oldVid.savedIn || savedIn);

        old.idt = parseInt(oldVid.idt) || Date.now();

        old.state = 'draft';
        delete old.episode;

        Video.destroy({
            savedIn: params.savedIn + '/' + params.vid,
            vid: '/episode/' + req.body.episodeId
        }, function(err){

            if (err) return logger.error(err);

            Video.create(old, function(err, episode){

                if (err) return logger.error(err);

                if (!episode) return res.status(500).end();

                return res.json({
                    success: true
                });

            });

        })

    });


});

module.exports = router;