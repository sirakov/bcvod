var express = require('express'),
	router = express.Router(),
	toString = require('vdom-to-html'),
	Head = require('../../client/components/head/render'),
	Login = require('../../client/components/login/render'),
	Main = require('../../client/components/main'),
	Admin = require('../models/Admin'),
	passwordless = require('../services/passwordless'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	params = require('../utils/params');

router.route('/').get(function(req, res){

	if (req.adminUser) {

		// console.log(req)

		if (req.query.r) return res.redirect(req.query.r);

		return res.redirect('/admin');
	}

	res.type('html');

	res.write('<!DOCTYPE html><html lang="en">' + toString(Head({
		title: 'Login',
		facebookAppId: res.locals.facebookAppId,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a,
		brandName: res.locals.brandName,
		baseUrl: res.locals.baseUrl,
		path: res.locals.path
	})));
	
	res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');
	res.write('<div class="loadingBar"></div>');
	res.write(toString(Main([Login({
		disable: true,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a
	})])));
	
	res.write(Script(res.locals));

	res.write(Script('', {
		'type': 'text/javascript',
		'src': '//kukuotv.disqus.com/embed.js',
		'data-timestamp': '' + new Date()
	}));
	res.write(Script('', {
		'type': 'text/javascript',
		src: '//www.google-analytics.com/analytics.js'
	}));

	res.write(Script('/javascripts/main-base.bundle.js'));
	
	return res.end('</body></html>');
	
}).post(
	passwordless.requestToken(function(user, delivery, callback) {
        return Admin.query(user)
					.limit(1)
					.attributes(['uid', 'savedIn'])
					.usingIndex('EmailIndex')
					.exec(function(err, data){
						if (err) return callback(err, null);
						else if (!data.Items.length)  return callback(null, null);

						var u = data.Items[0],
							id = params( {
								uid: u.get('uid'),
								savedIn: u.get('savedIn')
							});

						return callback(null, id + '-0');

					});
    }, {
    	originField: 'r'
    }),
	function(req, res){

		// console.log(req.xhr);

		return res.json({success: true});

	});

module.exports = router;