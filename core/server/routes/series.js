var express = require('express'),
	router = express.Router(),
    AWS = require('aws-sdk'),
	Main = require('../../client/components/main'),
    Head = require('../../client/components/head/render'),
    Header = require('../../client/components/header/render'),
    Footer = require('../../client/components/footer/render'),
    SeriesModule = require('../../client/components/series/render'),
    Single = require('../../client/components/single/render'),
    merge = require('mout/object/merge'),
    toString = require('vdom-to-html'),
    Video = require('../models/Video'),
    BodyClass = require('../../client/components/body/class'),
    Script = require('../../client/components/body/script'),
    getParams = require('../utils/params'),
    jobs = require('../services/queue'),
    getImage = require('../../client/components/single/getImage'),
    Promise = require('bluebird');

router.route('/:id').get(function(req, res, next){

    var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

	Video.get({savedIn: params.savedIn,vid: params.vid}, function(err, data){
        
        if (err) return logger.error(err);

        if (data) {

            Video.episodes(params.savedIn + '/' + params.vid).then(function(episodes){

                if (req.xhr) {

                    var vidObj = data.get();

                    if (!episodes.length) vidObj.episodes = [];
                    else vidObj.episodes = episodes;

                    vidObj.vid = getParams({
                        savedIn: vidObj.savedIn,
                        vid: vidObj.vid
                    });

                    return res.json(vidObj);

                } else {

                    var vidObj = data.get();

                    if (!episodes.length) vidObj.episodes = [];
                    else vidObj.episodes = episodes;

                    vidObj.vid = getParams({
                        savedIn: vidObj.savedIn,
                        vid: vidObj.vid
                    });

                    var vid = {
                        video: vidObj,
                        categories: res.locals.categories,
                        assetsUrl: res.locals.assetsUrl
                    };

                    res.type('html');

                    res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
                        title: (vidObj.title) ? vidObj.title : '',
                        facebookAppId: res.locals.facebookAppId,
                        assetsUrl: res.locals.assetsUrl,
                        build: res.locals.build,
                        _a: res.locals._a,
                        brandName: res.locals.brandName,
                        baseUrl: res.locals.baseUrl,
                        path: res.locals.path
                    })));

                    res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

                    res.write('<div class="loadingBar"></div>');

                    var _header = Header(res.locals);

                    res.write(toString(Main([_header, SeriesModule(vid)])));

                    res.locals.video = vidObj;

                    res.write(Script(res.locals));

                    res.write(Script('', {
                            'type': 'text/javascript',
                            'src': '//kukuotv.disqus.com/embed.js',
                            'data-timestamp': '' + new Date()
                        }));
                        res.write(Script('', {
                            'type': 'text/javascript',
                            src: '//www.google-analytics.com/analytics.js'
                        }));

                    res.write(Script('/javascripts/main-base.bundle.js'));

                    return res.end('</body></html>');

                }

            }).catch(function(err){

                return logger.error(err);

            });

        } else {

            return next();

        }

    });
	
});

router.route('/:id/:episode').get(function(req, res, next){

    var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

    Video.get({savedIn: params.savedIn,vid: params.vid}, function(err, data){
        
        if (err) logger.error(err);

        var ep = Video.episode({
                savedIn: data.get('savedIn') + '/' + data.get('vid'),
                vid: '/episode/' + req.params.episode
            }),
            eps = Video.episodes(params.savedIn + '/' + params.vid);

        Promise.all([ep, eps]).spread(function(episode, episodes){

            if (data) {

                var vidObj = data.get();

                vidObj.views = (vidObj.views) ? vidObj.views + 1 : 1;

                if (!vidObj.likes) vidObj.likes = 0;

                vidObj.episodes = episodes;

                vidObj = merge(episode, vidObj);

                queue.create('views', {
                    vid: vidObj.vid,
                    savedIn: vidObj.savedIn,
                    views: vidObj.views,
                    likes: vidObj.likes,
                    type: 'views'
                }).delay(1500)
                  .priority('high')
                  .save();

                var Liked = Video.getLiked({
                        user: (req.user) ? req.user.uid : null,
                        vid: vidObj.vid
                    }),
                    Next = Video.next({
                        vid: vidObj.vid,
                        savedIn: vidObj.savedIn,
                        category: vidObj.category,
                        idt: vidObj.idt,
                        genres: (vidObj.genres && vidObj.genres.length) ? vidObj.genres[0] : null
                    });

                Promise.all([Liked, Next]).spread(function(liked, next){

                    vidObj.vid = getParams({
                        savedIn: vidObj.savedIn,
                        vid: vidObj.vid
                    });

                    if (liked) vidObj.liked = true;

                    if (next) vidObj.next = next;

                    if (req.xhr) {

                        return res.json({
                            video: vidObj
                        });

                    } else if (data) {

                        var video = {
                            video: vidObj,
                            assetsUrl: res.locals.assetsUrl,
                            streamUrl: res.locals.streamUrl,
                            defaultPoster: res.locals.defaultPoster,
                            env: res.locals.env,
                            _a: res.locals._a,
                            _currentUser: res.locals._currentUser
                        };

                        res.type('html');

                        res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
                            title: (vidObj.title) ? vidObj.title : '',
                            facebookAppId: res.locals.facebookAppId,
                            description: (vidObj.description) ? vidObj.description : '',
                            image: getImage(vidObj.poster),
                            assetsUrl: res.locals.assetsUrl,
                            build: res.locals.build,
                            _a: res.locals._a,
                            brandName: res.locals.brandName,
                            baseUrl: res.locals.baseUrl,
                            path: res.locals.path
                        })));

                        res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

                        res.write('<div class="loadingBar"></div>');

                        res.locals.backgroundMix = true;

                        var _header = Header(res.locals);

                        res.write(toString(Main([_header, Single(video), Footer(res.locals)])));

                        res.locals.video = vidObj;

                        delete res.locals.backgroundMix;

                        res.write(Script(res.locals));

                        res.write(Script('', {
                            'type': 'text/javascript',
                            'src': '//kukuotv.disqus.com/embed.js',
                            'data-timestamp': '' + new Date()
                        }));
                        res.write(Script('', {
                            'type': 'text/javascript',
                            src: '//www.google-analytics.com/analytics.js'
                        }));

                        res.write(Script('/javascripts/main-base.bundle.js'));

                        return res.end('</body></html>');

                    }

                    return res.status(200).end();

                }).catch(function(err){

                    if (err) return logger.error(err);

                })

            } else {

                return next();

            }

        }).catch(function(err){

            return logger.error(err);
        });

    });

});

module.exports = router;