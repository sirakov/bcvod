var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	Single = require('../../client/components/single/render'),
	Video = require('../models/Video'),
    getImage = require('../../client/components/single/getImage'),
	getParams = require('../utils/params'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	toString = require('vdom-to-html'),
    queue = require('../services/addQueue'),
    merge = require('mout/object/merge'),
    path = require('path'),
    Promise = require('bluebird'),
    typecast = require('mout/string/typecast');

router.route('/:id/:episode?').get(function(req, res, next){

	var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

    Video.get({savedIn: params.savedIn,vid: params.vid}, function(err, data){

        if (err) logger.error(err);

        if (data) {

            var vidObj = data.get(),
                Se = new RegExp('series', 'g'),
                isSeries = Se.test(vidObj.category);

            var eps = Video.episodes(params.savedIn + '/' + params.vid);

            eps.then(function(episodes){

                var ep;

                if (req.params.episode) {

                    ep = '/episode/' + req.params.episode;

                } else if (episodes.length) {

                    ep = '/episode/' + episodes[0].vid.replace('/episode/', '');

                } else {

                    ep = '/episode/1';
                }

                return Video.episode({
                    savedIn: data.get('savedIn') + '/' + data.get('vid'),
                    vid: ep,
                    episodes: episodes
                });

            }).then(function(episode){

                if (!vidObj.views) vidObj.views = 0;

                if (!vidObj.likes) vidObj.likes = 0;

                if (isSeries) {

                    vidObj.parent = {
                        likes: vidObj.likes,
                        views: vidObj.views
                    };

                    vidObj.views = (episode && episode.views) ? episode.views : 0;

                    // console.log(vidObj);

                    if (episode) vidObj = merge(episode, vidObj);
                    else return next();

                    // console.log(vidObj);

                    if (episode) {

                        vidObj.title = vidObj.title + ' — ' + 'Episode ' + episode.vid.replace('/episode/', '');

                        vidObj.episodeId = parseInt(episode.vid.replace('/episode/', ''));

                    }

                }

                if (req.user || req.frontEndUser) {

                    queue('history', {
                        vid: vidObj.vid,
                        savedIn: vidObj.savedIn,
                        user: (req.user) ? req.user.uid : req.frontEndUser
                    });

                }

				queue('views', {
			        vid: (vidObj.episode) ? '/episode/' + vidObj.episodeId : params.vid,
			        savedIn: (vidObj.episode) ? vidObj.savedIn + '/' + params.vid : vidObj.savedIn,
			        episode: (vidObj.episode) ? true: false,
			        parent: {
			            savedIn: params.savedIn,
			            vid: params.vid,
			            views: (vidObj.parent && vidObj.parent.views)? vidObj.parent.views + 1: 1,
			            likes: (vidObj.parent && vidObj.parent.likes)? vidObj.parent.likes: 0
			        },
			        views: (vidObj.views)? vidObj.views + 1 : 1,
			        likes: (vidObj.likes)? vidObj.likes: 0,
			        type: 'views'
			    });

                var Liked = Video.getLiked({
                        user: (req.user) ? req.user.uid : req.frontEndUser,
                        vid: vidObj.savedIn + '/' + vidObj.vid
                    });

                return Promise.all([Liked]).spread(function(liked){

                    vidObj.vid = getParams({
                        savedIn: vidObj.savedIn,
                        vid: vidObj.vid
                    });

                    if (liked) vidObj.liked = true;

                    if (isSeries && vidObj.episodes && vidObj.episodes.length) {

                        vidObj.next = vidObj.vid + '/' + (vidObj.episodeId + 1);

                        if (!vidObj.sequel && vidObj.episodeId === vidObj.episodes.length) vidObj.next = null;
                        else if (vidObj.episodeId === vidObj.episodes.length) vidObj.next = vidObj.sequel;

                    } else {

                        if (!vidObj.sequel) vidObj.next = null;
                        else vidObj.next = vidObj.sequel;

                    }

                    if (req.xhr) {

                        return res.json({
                            video: vidObj
                        });

                    } else {

                        var video = {
                            video: vidObj,
                            assetsUrl: res.locals.assetsUrl,
                            streamUrl: res.locals.streamUrl,
                            imgsUrl: res.locals.imgsUrl,
                            thumbnailsUrl: res.locals.thumbnailsUrl,
                            baseUrl: res.locals.baseUrl,
                            brandName: res.locals.brandName,
                            embed: typecast(req.query.embed),
                            defaultPoster: res.locals.defaultPoster,
                            env: res.locals.env,
                            _a: res.locals._a,
                            _currentUser: res.locals._currentUser
                        };

                        res.type('html');

                        res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
                            title: (vidObj.title) ? vidObj.title : '',
                            facebookAppId: res.locals.facebookAppId,
                            description: (vidObj.description) ? vidObj.description : '',
                            image: getImage(vidObj.poster),
                            imgsUrl: res.locals.imgsUrl,
                            assetsUrl: res.locals.assetsUrl,
                            build: res.locals.build,
                            _a: res.locals._a,
                            brandName: res.locals.brandName,
                            baseUrl: res.locals.baseUrl,
                            path: res.locals.path,
                            video: true
                        })));

                        res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

                        res.write('<div class="loadingBar"></div>');

                        var _header,
                            _footer;

                        if (!video.embed) {

                            res.locals.backgroundMix = true;

                            _header = Header(res.locals);

                            _footer = Footer(res.locals);

                        }

                        res.write(toString(Main([_header, Single(video), _footer])));

                        res.locals.video = vidObj;

                        delete res.locals.backgroundMix;

                        res.write(Script(res.locals));

                        res.write(Script('', {
                            'type': 'text/javascript',
                            'src': '//kukuotv.disqus.com/embed.js',
                            'data-timestamp': '' + new Date()
                        }));
                        res.write(Script('', {
                            'type': 'text/javascript',
                            src: '//www.google-analytics.com/analytics.js'
                        }));

                        res.write(Script('/javascripts/main-base.bundle.js'));

                        return res.end('</body></html>');

                    }

                    return res.status(200).end();

                }).catch(function(err){

                    return logger.error(err);

                });

            }).catch(function(err){

                return logger.error(err);

            });

        } else {

            return next();
        }

    });

});

router.route('/:id/next').post(function(req, res, next){

    var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

    if (!req.xhr) return next();

    // console.log(req.body);

    Video.next({
        vid: params.vid,
        savedIn: params.savedIn,
        category: req.body.category,
        idt: parseInt(req.body.idt),
        genres: req.body.genres,
        isSeries: req.body.isSeries,
        sequel: req.body.sequel
    }).then(function(nextId){

        return res.json({
            nextId: nextId
        });

    }).catch(function(err){

        return logger.error(err);

    });

});

// router.route('/:id/views').post(function(req, res, next){
//
//     var params = getParams(req.params.id),
//         savedIn;
//
//     if (!params.vid && !params.savedIn) return next();
//
//     var vidObj = req.body;
//
//     queue('views', {
//         vid: (vidObj.episode) ? '/episode/' + vidObj.episodeId : params.vid,
//         savedIn: (vidObj.episode) ? vidObj.savedIn + '/' + params.vid : vidObj.savedIn,
//         episode: (vidObj.episode) ? true: false,
//         parent: {
//             savedIn: params.savedIn,
//             vid: params.vid,
//             views: (vidObj.parent && vidObj.parent.views)? vidObj.parent.views + 1: 1,
//             likes: (vidObj.parent && vidObj.parent.likes)? vidObj.parent.likes: 0
//         },
//         views: (vidObj.views)? vidObj.views + 1 : 1,
//         likes: (vidObj.likes)? vidObj.likes: 0,
//         type: 'views'
//     });
//
//     return res.status(200).end();
//
// });

router.route('/:id/likes').post(function(req, res, next){

    if (!req.user && !req.frontEndUser) return res.status(400).end();

    var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

    queue('views', {
        vid: params.vid,
        savedIn: params.savedIn,
        user: (req.user) ? req.user.uid : req.frontEndUser,
        type: 'like'
    });

    return res.status(200).end();

}).delete(function(req, res, next){

    if (!req.user && !req.frontEndUser) return res.status(400).end();

    var params = getParams(req.params.id),
        savedIn;

    if (!params.vid && !params.savedIn) return next();

    queue('views', {
        vid: params.vid,
        savedIn: params.savedIn,
        user: (req.user) ? req.user.uid : req.frontEndUser,
        type: 'unlike'
    });

      return res.status(200).end();

});

module.exports = router;
