var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	Contact = require('../../client/components/contact/render'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	Mailer = require('../services/mailer'),
	toString = require('vdom-to-html'),
	email = require('../../client/components/login/email');

router.route('/').get(function(req, res){

	res.type('html');

	res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
		title: 'Contact us',
		facebookAppId: res.locals.facebookAppId,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a,
		brandName: res.locals.brandName,
		baseUrl: res.locals.baseUrl,
		path: res.locals.path
	})));

	res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

	res.write('<div class="loadingBar"></div>');

	var _header = Header(res.locals);

	res.write(toString(Main([_header, Contact({
		xsrf: res.locals.xsrf
	}), Footer(res.locals)])));

	res.write(Script(res.locals));

	res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));
	
	res.write(Script('/javascripts/main-base.bundle.js'));

	return res.end('</body></html>');
	
}).post(function(req, res){

	if (!email.test(req.body.email)) {

		res.type('html');

		res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
			title: 'Contact us',
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

		res.write('<div class="loadingBar"></div>');

		var _header = Header(res.locals);

		res.write(toString(Main([_header, Contact({
			xsrf: res.locals.xsrf,
			subject: req.body.subject,
	        message: req.body.message,
	        name: req.body.name,
	        email: req.body.email,
	        error: true
		}), Footer(res.locals)])));

		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));
		
		res.write(Script('/javascripts/main-base.bundle.js'));

		return res.end('</body></html>');

	}

	Mailer({
        to: config.get('contact_email'),
        subject: req.body.subject,
        message: req.body.message,
        name: req.body.name,
        email: req.body.email,
        fileName: 'contact'
    }, function(err, data){

    	if (err) return res.status(400).end();

		res.type('html');

		res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
			title: 'Contact us',
			facebookAppId: res.locals.facebookAppId,
			assetsUrl: res.locals.assetsUrl,
			build: res.locals.build,
			_a: res.locals._a,
			brandName: res.locals.brandName,
			baseUrl: res.locals.baseUrl,
			path: res.locals.path
		})));

		res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

		res.write('<div class="loadingBar"></div>');

		var _header = Header(res.locals);

		res.write(toString(Main([_header, Contact({
			success: true,
			xsrf: res.locals.xsrf
		}), Footer(res.locals)])));

		res.write(Script(res.locals));

		res.write(Script('', {
			'type': 'text/javascript',
			'src': '//kukuotv.disqus.com/embed.js',
			'data-timestamp': '' + new Date()
		}));
		res.write(Script('', {
			'type': 'text/javascript',
			src: '//www.google-analytics.com/analytics.js'
		}));
		
		res.write(Script('/javascripts/main-base.bundle.js'));

		return res.end('</body></html>');

	});
	
});

module.exports = router;