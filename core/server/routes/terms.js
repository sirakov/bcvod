var express = require('express'),
	router = express.Router(),
	Main = require('../../client/components/main'),
	Head = require('../../client/components/head/render'),
	Header = require('../../client/components/header/render'),
	Footer = require('../../client/components/footer/render'),
	Terms = require('../../client/components/terms/render'),
	BodyClass = require('../../client/components/body/class'),
	Script = require('../../client/components/body/script'),
	marked = require('marked'),
	fs = require('fs-extra'),
	html = marked(fs.readFileSync('terms.md').toString()),
	toString = require('vdom-to-html');

router.route('/').get(function(req, res){

	if (req.xhr) {

		return res.json({
			html: html
		});

	}

	res.type('html');

	res.write('<!DOCTYPE html><html lang="en">'+ toString(Head({
		title: 'Terms of use',
		facebookAppId: res.locals.facebookAppId,
		assetsUrl: res.locals.assetsUrl,
		build: res.locals.build,
		_a: res.locals._a,
		brandName: res.locals.brandName,
		baseUrl: res.locals.baseUrl,
		path: res.locals.path
	})));

	res.write('<body class="'+ BodyClass(res.locals.useragent) +'">');

	res.write('<div class="loadingBar"></div>');

	var _header = Header(res.locals);

	res.write(toString(Main([_header, Terms({
		html: html
	}), Footer(res.locals)])));

	res.write(Script(res.locals));

	res.write(Script('', {
		'type': 'text/javascript',
		'src': '//kukuotv.disqus.com/embed.js',
		'data-timestamp': '' + new Date()
	}));
	res.write(Script('', {
		'type': 'text/javascript',
		src: '//www.google-analytics.com/analytics.js'
	}));
	
	res.write(Script('/javascripts/main-base.bundle.js'));

	return res.end('</body></html>');
	
});

module.exports = router;