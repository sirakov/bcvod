GLOBAL._ = require('lodash');
GLOBAL.Logger = require('le_node'),
GLOBAL.logger = require('winston');

require('./env')();

GLOBAL.package = require('fs-extra').readJsonSync(__dirname + '/../../package.json');

GLOBAL.GLOBALS = {};

GLOBAL.config = require('config');

logger.add(logger.transports.Logentries, { token: config.get('log_token') });

if (process.env.NODE_ENV !== 'development') {

	var instance = require('ec2-instance-data');

	instance.init(function () {

		GLOBAL.instanceId = instance.latest['meta-data']['instance-id'];
		GLOBAL.instanceIp = instance.latest['meta-data']['public-ipv4'];

	});

}

var express = require('express'),
	app = require('./app'),
	compress = require('compression'),
	helmet = require('helmet'),
	session = require('client-sessions'),
	csrf = require('lusca'),
	body = require('body-parser'),
	cors = require('cors'),
	passport = require('./services/passport'),
	morgan = require('morgan'),
	Admin = require('./models/Admin'),
	User = require('./models/User'),
	passwordless = require('./services/passwordless'),
	categories = require('../client/components/header/categories'),
	menus = require('../client/components/header/menus'),
	base64url = require('base64url'),
	platform = require('platform'),
	params = require('./utils/params'),
	merge = require('mout/object/merge'),
	Freezer = require('freezer-js'),
	checkAdmin = require('./utils/checkAdmin'),
	morganStream = {
	    write: function(message, encoding){
	        logger.info(message);
	    }
	};

GLOBALS = new Freezer({
	env: process.env.NODE_ENV,
	build: base64url.fromBase64(new Buffer(GLOBAL.package.version).toString('base64')),
	brandName: config.get('brandName'),
	thumbnailsUrl: config.get('thumbnailsUrl'),
	imgsUrl: config.get('imgsUrl'),
	assetsUrl: config.get('assetsUrl'),
	baseUrl: config.get('baseUrl'),
	utilsUrl: config.get('utilsUrl'),
	streamUrl: config.get('streamUrl'),
	googleTrackingId: config.get('googleTrackingId'),
	facebookAppId: config.get('facebookAppId'),
	categories: categories,
	menus: menus,
	defaultPoster: config.get('defaultPoster'),
	_a: config.get('_a')
});

morgan.token('instance', function(req, res){

	if ('instanceIp' in GLOBAL) {

		return instanceIp;

	} else {

		return '';

	}

})

app.set('trust proxy', true);

app.use(function(req, res, next){

	if ('instanceId' in GLOBAL) {

		res.set({
			'x-server-id': instanceId
		});

	}

	res.set({
		'x-server-version': 'v' + GLOBAL.package.version
	});

	return next();

});

app.use(compress());

app.use(helmet());

app.use(cors({
	origin: '*'
}));

app.use(function(req, res, next){
	if (req.path.indexOf('hls') > -1) {

		if (!req.headers['x-file-secure']) return res.status(403).end();

	}
	return next();
},express.static(__dirname + '/../../content/', {maxAge: 365 * 24 * 60 * 60 * 1000}));

app.disable('etag');

app.use(morgan(':instance :status :method :url', {'stream': morganStream}));

app.use(body.urlencoded({ extended: true }));

app.use(body.json());

app.use(session({
	cookieName: 'sid',
	requestKey: 'session',
	secret: 'KukuotvServerProductionCluster',
	duration: 30 * 24 * 60 * 60 * 1000,
	activeDuration: 1000 * 60 * 5
}));

app.use(csrf.csrf({
	cookie: 'xsrf',
	key: 'xsrf',
	header: 'x-xsrf-token'
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(passwordless.sessionSupport());
app.use(passwordless.acceptToken({enableOriginRedirect: true, successRedirect: '/'}));

app.use(helmet.noCache());

app.use(function(req, res, next) {
    res.setHeader('Content-Security-Policy', 
    	'script-src \'self\' \'unsafe-inline\' \'unsafe-eval\' '+
    	'https://apis.google.com ' + config.get('assetsUrl') + 
    	' https://cdn.rawgit.com' +
    	' http://pagead2.googlesyndication.com https://pagead2.googlesyndication.com' +
    	' http://*.disquscdn.com http://kukuotv.disqus.com http://www.google-analytics.com https://kukuotv.disqus.com https://www.google-analytics.com ' +
    	'http://imasdk.googleapis.com https://imasdk.googleapis.com');
    return next();
});

app.use(function(req, res, next){

	if (!req.adminUser) return next();

	var isAdmin = checkAdmin(req.adminUser);

	if (!isAdmin) req.frontEndUser = params(params(req.adminUser));

	return next();

});

app.use(function(req, res, next){

	res.locals.path = req.path;

	if (!req.xhr) {

		res.locals = merge(res.locals, GLOBALS.get().toJS());

		res.locals.useragent = platform.parse(req.headers['user-agent']);

	}

	if (req.frontEndUser && !req.xhr) {

		var user = params(req.frontEndUser);

		if (!user.savedIn && !user.vid) return next();

		User.get({savedIn: user.savedIn, uid: user.vid}, function(err, fUser){

			if (err) return logger.error(err);

			if (fUser) {

				res.locals._currentUser = _.pick(fUser.get(), [
					'uid',
					'savedIn',
					'username',
					'fullname',
					'avatar',
					'location'
				]);

				res.locals._currentUser.frontend = true;

				if (!fUser.get('active')) {

					User.update({
						savedIn: user.savedIn,
						uid: user.vid,
						active: true
					}, function(err, use){

						if (err) return logger.error(err);

						return next();

					});

				} else {

					return next();

				}

			} else if (res.locals._currentUser) {

				delete res.locals._currentUser;

				return next();
			}

		});

	} else if (req.adminUser && !req.xhr) {

		var user = params(req.adminUser);

		if (!user.savedIn && !user.vid) return next();

		Admin.get({savedIn: user.savedIn, uid: user.vid}, function(err, admin){
			if (err) return logger.error(err);

			if (admin) {

				res.locals._currentUser = _.pick(admin.get(), ['uid', 'master', 'savedIn']);

				return next();

			}  else {

				return next();

			}

		});

	} else if (req.user && !req.xhr) {

		res.locals._currentUser = _.pick(req.user, [
			'uid',
			'savedIn',
			'fullname',
			'avatar',
			'location'
		]);

		res.locals._currentUser.frontend = true;

		return next();

	} else {
		
		return next();

	}

});

app.use('/login', function(req, res, next){

	if (req.user || req.adminUser) return res.redirect('/');

	if (req.query.r) req.session.returnTo = req.query.r;

	return next();

}, require('./routes/userLogin'));

app.get('/login/twitter', passport.authenticate('twitter'));

app.get('/login/twitter/callback',
	passport.authenticate('twitter', {
		failureRedirect: '/login',
		successReturnToOrRedirect: true
	})
);

app.get('/login/facebook', passport.authenticate('facebook', {
	scope: ['email', 'public_profile']
}));

app.get('/login/facebook/callback',
	passport.authenticate('facebook', {
		failureRedirect: '/login',
		successReturnToOrRedirect: true
	})
);

app.use('/admin/login', require('./routes/login'));

app.use('/admin', function(req, res, next){

	if (!req.adminUser) return next();
	
	var isAdmin = checkAdmin(req.adminUser);

	if (!isAdmin) return res.redirect('/');

	return next();


}, passwordless.restricted({ originField: 'r', failureRedirect: '/admin/login' }), function(req, res, next){

	res.locals.admin = true;

	return next();

});

app.get('/me/logout', function(req, res, next){

	if (req.logout && req.user) {

		req.logout();

		if (req.query.r) return res.redirect(req.query.r);

		return res.redirect('/');

	} else {

		return next();

	}

}, passwordless.logout(), function(req, res) {

	if (req.query.r) return res.redirect(req.query.r);
    
    return res.redirect('/admin/login');

});

app.use('/admin/upload', require('./routes/upload'));

app.use('/admin', require('./routes/videos'));

app.use('/admin/v', require('./routes/video'));

app.use('/me', require('./routes/user'));

// app.use('/s', require('./routes/series'));

app.use('/v', require('./routes/single'));

app.use('/policy', require('./routes/policy'));
app.use('/terms', require('./routes/terms'));

app.use('/search', require('./routes/search'));

app.use('/', require('./routes/homepage'));

app.use('/latest', require('./routes/latest'));

app.use('/category', require('./routes/category'));

app.use('/contact-us', require('./routes/contact'));

app.use('/sitemap', require('./routes/sitemap'));

// app.get('/', require('./routes/index'));

// app.use('/admin', function(req, res){

// 	if (!req.session.user) return res.redirect('/admin/login?r=' + )

// });

module.exports = app;
