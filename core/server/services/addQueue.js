var sqs = require('./sqs');

var AddToQueue = function(type, data, cb){
	
	return sqs.sendMessage({
		MessageBody: JSON.stringify(data),
		QueueUrl: config.get('queueEndpoint') + '/' + type + 'Queue',
	}, function(err, data) {

		if (err) return logger.error(err, err.stack);

	});

};

module.exports = AddToQueue;