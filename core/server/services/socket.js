var app = require('../app'),
	server = require('http').createServer(app),
	io = require('socket.io')(server);

exports.io = io;
exports.server = server;