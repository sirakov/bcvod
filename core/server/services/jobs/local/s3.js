var debug = require('debug')('jobs:s3'),
	Video = require('../../../models/Video'),
	io = require('../../socket').io,
	queue = require('../../addQueue');

var S3 = function(job, done){

	var file = job;

	debug('Starting ' + file.name + ' upload to s3');

	setTimeout(function(){

		Video.update({
	        savedIn: file.savedIn,
	        vid: file.vid,
	        status: 'transcode',
	        s3FileName: file.oFileName
	    }, function(err, video){

	    	if (err) return done(err);

			debug('Done uploading ' + file.name + ' to s3');

			io.sockets.emit(file.vid + ':s3');
		
			queue('transcoder', {
				key: file.name,
				vid: file.vid,
				savedIn: file.savedIn,
				name: file.name,
				prefix: file.name,
				oFileName: file.oFileName
			});

			return done();

		});

	}, 1500);

	
};

module.exports = S3;