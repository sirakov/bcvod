var io = require('../../socket').io,
	Video = require('../../../models/Video'),
	debug = require('debug')('transcoder');

var Transcoder = function(job, done){

	var file = job;

	setTimeout(function(){

		Video.get({
	        savedIn: file.savedIn,
	        vid: file.vid
	    }, function(err, video){

	    	if (err) return done(err);

	    	debug('Transcode complete for  ' + file.name);

	    	var newVideo = video.get();

	    	newVideo.state = 'public';
			newVideo.savedIn = newVideo.savedIn.replace('/draft', '');
			newVideo.status = 'live';
			newVideo.duration = 100000;
			newVideo.transcodeId = 'sfsdsdf-d-fsd-fs-dfsdf';
			newVideo.thumbnail = 'images/thumb.jpg';

			Video.destroy({
				savedIn: file.savedIn,
				vid: file.vid
			}, function(err){
				
				if (err) return done(err);

				Video.create(newVideo, function(err, data){

					if (err) return done(err);

					io.sockets.emit(file.vid + ':transcode', {
						savedIn: data.get().savedIn
					});

					return done();

				});

			});

		});

	}, 1500);

};

module.exports = Transcoder;