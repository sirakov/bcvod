var debug = require('debug')('jobs:views'),
	Video = require('../../../models/Video'),
	UserLikes = require('../../../models/UserLikes');

var Views = function(job, done){

	var video = job;

	if (video.type === 'views') {

		if (video.episode) {

			Video.update({
				savedIn: video.parent.savedIn,
				vid: video.parent.vid,
				views: video.parent.views,
				viewLikes: video.parent.views + '/' + video.parent.likes
			}, function(err, data){

				if (err) throw new Error(err);

				Video.update({
					savedIn: video.savedIn,
					vid: video.vid,
					views: video.views,
					viewLikes: video.views + '/' + video.likes
				}, function(err, video){

					if (err) throw new Error(err);

					return done();

				});

			});

		} else {

			Video.update({
				savedIn: video.savedIn,
				vid: video.vid,
				views: video.views,
				viewLikes: video.views + '/' + video.likes
			}, function(err, video){

				if (err) throw new Error(err);

				return done();

			});

		}

	} else if (video.type === 'like') {

		Video.query(video.savedIn + '/' + video.vid)
				.where('vid').beginsWith('/likes')
				.filter('likedBy').contains(video.user)
				.exec(function(err, liked){

					if (err) throw new Error(err);

					if (liked.Items.length) return done();

					Video.query(video.savedIn + '/' + video.vid)
							.where('vid').beginsWith('/likes')
							.limit(1)
							.descending()
							.exec(function(err, lastLikes){

								if (err) throw new Error(err);

								if (lastLikes.Items.length) {

									var last = lastLikes.Items[0].get();

									if (last.likedBy.length < 40) {

										last.likedBy.push(video.user);

										Video.update(last, function(err, vid){

											if (err) throw new Error(err);

											Video.incLikes({
												savedIn: video.savedIn,
												vid: video.vid
											}).then(function(vid){

												return done();

											}).catch(function(err){

												throw new Error(err);

											});

										});


									} else {

										Video.query(video.savedIn + '/' + video.vid)
											.where('vid').beginsWith('/likes')
											.select('COUNT')
											.exec(function(err, likeCount){

												if (err) throw new Error(err);

												Video.create({
													savedIn: video.savedIn + '/' + video.vid,
													vid: '/likes/' + (parseInt(likeCount.Count) + 1),
													likedBy: [video.user] 
												}, function(err, likes){

													if (err) throw new Error(err);

													Video.incLikes({
														savedIn: video.savedIn,
														vid: video.vid
													}).then(function(vid){

														return done();

													}).catch(function(err){

														throw new Error(err);

													});


												});

											});

									}


								} else {

									Video.create({
										savedIn: video.savedIn + '/' + video.vid,
										vid: '/likes/1',
										likedBy: [video.user] 
									}, function(err, likes){

										if (err) throw new Error(err);

										Video.incLikes({
											savedIn: video.savedIn,
											vid: video.vid
										}).then(function(vid){

											return done();

										}).catch(function(err){

											throw new Error(err);

										});


									});

								}

							});

				});

		// UserLikes.get({
		// 	user: video.user.split('-').join('/'),
		// 	video: video.vid
		// }, function(err, liked){

		// 	if (err) throw new Error(err);

		// 	if (liked) return done();

		// 	Video.get({
		// 		savedIn: video.savedIn,
		// 		vid: video.vid
		// 	}, {ConsistentRead: true, AttributesToGet : ['likes','views']}, function(err, vid){

		// 		if (err) throw new Error(err);

		// 		var likes = vid.get('likes') || 0,
		// 			views = vid.get('views') || 0,
		// 			newLikes = likes + 1;

		// 		Video.update({
		// 			savedIn: video.savedIn,
		// 			vid: video.vid,
		// 			likes: newLikes,
		// 			viewLikes: views + '/' + newLikes 
		// 		}, function(err, data){

		// 			if (err) throw new Error(err);

		// 			UserLikes.create({
		// 				user: video.user.split('-').join('/'),
		// 				video: video.vid
		// 			}, function(err, like){

		// 				if (err) throw new Error(err);

		// 				return done();

		// 			});

		// 		})



		// 	});

		// });

	} else if (video.type === 'unlike') {

		Video.query(video.savedIn + '/' + video.vid)
				.where('vid').beginsWith('/likes')
				.filter('likedBy').contains(video.user)
				.exec(function(err, liked){

					if (err) throw new Error(err);

					if (!liked.Items.length) return done();

					var liked = liked.Items[0].get(),
						likeSplit = liked.vid.split('/'),
						likeId = parseInt(likeSplit[2]),
						likedIndex = liked.likedBy.indexOf(video.user);

					if (likedIndex > -1) liked.likedBy.splice(likedIndex, 1);

					Video.query(video.savedIn + '/' + video.vid)
							.where('vid').beginsWith('/likes')
							.limit(1)
							.descending()
							.exec(function(err, lastLikes){

								if (err) throw new Error(err);

								// console.log(video.id, 'ss');

								var lastLikes = lastLikes.Items[0].get(),
									likeLastSplit = lastLikes.vid.split('/'),
									likeLastId = parseInt(likeLastSplit[2]);

								if (likeLastId === likeId) {

									if (liked.likedBy.length) {

										Video.update(liked, function(err, data){

											if (err) throw new Error(err);

											Video.decLikes({
												savedIn: video.savedIn,
												vid: video.vid
											}).then(function(vid){

												return done();

											}).catch(function(err){

												throw new Error(err);

											});

										});

									} else {

										Video.destroy({
											savedIn: lastLikes.savedIn,
											vid: lastLikes.vid
										}, function(err, data){

											if (err) throw new Error(err);

											Video.decLikes({
												savedIn: video.savedIn,
												vid: video.vid
											}).then(function(vid){

												return done();

											}).catch(function(err){

												throw new Error(err);

											});

										});

									}

								} else {

									liked.likedBy.push(lastLikes.likedBy[0]);

									lastLikes.likedBy.splice(0, 1);

									if (lastLikes.likedBy.length) {

										Video.update(liked, function(err, data){

											if (err) throw new Error(err);

											Video.update(lastLikes, function(err, data){

												if (err) throw new Error(err);

												Video.decLikes({
													savedIn: video.savedIn,
													vid: video.vid
												}).then(function(vid){

													return done();

												}).catch(function(err){

													throw new Error(err);

												});

											});

										});

									} else {

										Video.update(liked, function(err, data){

											if (err) throw new Error(err);

											Video.destroy({
												savedIn: lastLikes.savedIn,
												vid: lastLikes.vid
											}, function(err, data){

												if (err) throw new Error(err);

												Video.decLikes({
													savedIn: video.savedIn,
													vid: video.vid
												}).then(function(vid){

													return done();

												}).catch(function(err){

													throw new Error
												});

											});

										});

									}

								}

							});


				});

		// UserLikes.get({
		// 	user: video.user.split('-').join('/'),
		// 	video: video.vid
		// }, function(err, liked){

		// 	if (err) throw new Error(err);

		// 	if (!liked) return done();

		// 	Video.get({
		// 		savedIn: video.savedIn,
		// 		vid: video.vid
		// 	}, {ConsistentRead: true, AttributesToGet : ['likes','views']}, function(err, vid){

		// 		if (err) throw new Error(err);

		// 		var likes = vid.get('likes') || 0,
		// 			views = vid.get('views') || 0,
		// 			newLikes = (likes > 0) ? likes - 1 : 0;

		// 		Video.update({
		// 			savedIn: video.savedIn,
		// 			vid: video.vid,
		// 			likes: newLikes,
		// 			viewLikes: views + '/' + newLikes 
		// 		}, function(err, data){

		// 			if (err) throw new Error(err);

		// 			UserLikes.destroy({
		// 				user: video.user.split('-').join('/'),
		// 				video: video.vid
		// 			}, function(err){

		// 				if (err) throw new Error(err);

		// 				return done();
						
		// 			});

		// 		})

		// 	});

		// });

	} else {

		return done();

	}

};

module.exports = Views;