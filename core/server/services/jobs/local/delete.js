var fs = require('fs-extra'),
	debug = require('debug')('jobs:delete'),
	Path = require('path');

var Delete = function(job, done){

    var video = job,
    	poster = (video.poster) ? JSON.parse(video.poster) : null,
    	posterUrl,
        videoPath;

    if (poster && poster.src) {

    	posterUrl = Path.normalize(__dirname  + '/../../../../../content/posters/' + poster.src);

    }

    if (video.status === 'local') {

    	videoPath = Path.normalize(__dirname + '/../../../../../' + config.get('tmp_dir') + video.oFileName);

    } else if (video.status === 'live') {

    	videoPath = Path.normalize(__dirname + '/../../../../../' + config.get('tmp_dir') + video.s3FileName);

    }

    if (posterUrl && fs.existsSync(posterUrl)) fs.unlinkSync(posterUrl);

    if (videoPath && fs.existsSync(videoPath)) fs.unlinkSync(videoPath);

    return done();

};

module.exports = Delete;