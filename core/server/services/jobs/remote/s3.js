var Path = require('path'),
	fs = require('fs-extra'),
	crypto = require('crypto'),
	moment = require('moment'),
	debug = require('debug')('jobs:s3'),
	base64url = require('base64url'),
	s3 = require('s3'),
	Video = require('../../../models/Video'),
	io = require('../../socket').io,
	client = s3.createClient({
		maxAsyncS3: 20,
		s3RetryCount: 3,
		s3RetryDelay: 1000,
		multipartUploadThreshold: 20971520,
		multipartUploadSize: 15728640,
		s3Options: config.get('aws.s3')
	}),
	queue = require('../../addQueue');

var S3 = function(job, done){

	debug('Starting ' + job.name + ' upload to s3');

	console.log('Uploading to s3: ', JSON.stringify(job));

	var file = job,
		Key = base64url.fromBase64(crypto.randomBytes(18).toString('base64')),
		path = Path.normalize(__dirname + '/../../../../../' + config.get('tmp_dir') + file.oFileName),
		Prefix = moment().format('YYYY[/]MM[/]'),
		params = {
			localFile: path,
			s3Params:{
				Bucket: config.get('buckets.input'),
				Key: Prefix + Key  + Path.extname(file.name),
				CacheControl: 'max-age=604800'
			}
		};


	var upload = client.uploadFile(params);

	upload.on('error', function (error) {
		debug('Error occurred while uploading ' + file.name + ' to s3');
		throw new Error(error);
	});

	upload.on('progress', function () {
		// console.log(details);
	});

	upload.on('end', function () {

		fs.unlink(path, function(err){

			if (err) throw new Error(err);

			Video.update({
                savedIn: file.savedIn,
                vid: file.vid,
                s3FileName: params.s3Params.Key,
                status: 'transcode'
            }, function(err, video){

            	if (err) throw new Error(err);

				debug('Done uploading ' + file.name + ' to s3');

				io.sockets.emit(file.vid + ':s3');
			
				queue('transcoder', {
					key: params.s3Params.Key,
					vid: file.vid,
					savedIn: file.savedIn,
					name: file.name,
					prefix: Prefix + Key + '/',
					oFileName: file.oFileName
				});

				return done();

			});

		});

	});

	
};

module.exports = S3;
