var debug = require('debug')('jobs:delete'),
	s3 = require('s3'),
	path = require('path'),
	fs = require('fs-extra'),
	client = s3.createClient({
		maxAsyncS3: 20,
		s3RetryCount: 3,
		s3RetryDelay: 1000,
		multipartUploadThreshold: 20971520,
		multipartUploadSize: 15728640,
		s3Options: config.get('aws.s3')
	});

var Delete = function(job, done){

	var video = job,
		poster = (video.poster) ? JSON.parse(video.poster) : null,
		videoPath;

	if (video.status === 'local') {

    	videoPath = path.normalize(__dirname + '/../../../../../' + config.get('tmp_dir') + video.oFileName);

    	if (videoPath && fs.existsSync(videoPath)) fs.unlinkSync(videoPath);

		if (poster && poster.src) {

	    	client.s3.deleteObject({
				Bucket: config.get('buckets.assets'),
				Key: 'posters/' + poster.src
			}, function(err, data) {
				
				if (err) throw new Error(err);

				return done();

			});

	    } else {

	    	return done();

	    }

    } else if (video.status === 'live') {

    	if (video.s3FileName) {

    		var extname = path.extname(video.s3FileName),
				params = {
					Bucket: config.get('buckets.output'),
					Prefix: video.s3FileName.replace(extname, '') + '/'
				},
				upload = client.deleteDir(params);

			upload.on('error', function (error) {
				debug('Error occurred while uploading ' + file.name + ' to s3');
				throw new Error(error);
			});

			upload.on('progress', function () {
				// console.log(details);
			});

			upload.on('end', function (){

				client.s3.deleteObject({
					Bucket: config.get('buckets.input'),
					Key: video.s3FileName
				}, function(err, data) {

					if (err) throw new Error(err);

					if (poster && poster.src) {

				    	client.s3.deleteObject({
							Bucket: config.get('buckets.assets'),
							Key: 'posters/' + poster.src
						}, function(err, data) {
							
							if (err) throw new Error(err);

							return done();

						});

				    } else {

				    	return done();

				    }


				});

			});	

    	} else {

    		videoPath = path.normalize(__dirname + '/../../../../../' + config.get('tmp_dir') + video.oFileName);

	    	if (videoPath && fs.existsSync(videoPath)) fs.unlinkSync(videoPath);

			if (poster && poster.src) {

		    	client.s3.deleteObject({
					Bucket: config.get('buckets.assets'),
					Key: 'posters/' + poster.src
				}, function(err, data) {
					
					if (err) throw new Error(err);

					return done();

				});

		    } else {

		    	return done();

		    }

    	}

    } else {

    	return done();
    	
    }

};

module.exports = Delete;