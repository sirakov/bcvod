var debug = require('debug')('jobs:views'),
	Video = require('../../../models/Video'),
	UserHistory = require('../../../models/UserHistory'),
	params = require('../../../utils/params');

var History = function(job, done){

	var video = job;

	UserHistory.update({
		video: params({
			savedIn: video.savedIn,
			vid: video.vid
		}),
		idt: Date.now(),
		user: video.user
	}, function(err, data){

		if (err) throw new Error(err);


		return done();

	})

};

module.exports = History;