var aws = require('aws-sdk'),
	transcoder = new aws.ElasticTranscoder(config.get('aws.transcoder')),
	s3 = new aws.S3(config.get('aws.s3')),
	io = require('../../socket').io,
	Video = require('../../../models/Video'),
	debug = require('debug')('transcoder'),
	transcodeOpts = config.get('transcode'),
	async = require('async'),
	pad = require('mout/number/pad');

var Transcoder = function(job, done){

	var file = job,
		params = {
			Input: { 
				Key: file.key
			},
			PipelineId: transcodeOpts.pipelineId,
			OutputKeyPrefix: file.prefix,
			Outputs: [{
				Key: 'playbacks/400k/segment',
				PresetId: transcodeOpts['400k'],
				SegmentDuration: '5',
				Watermarks: [{
			        InputKey: 'watermark.png',
			        PresetWatermarkId: 'BottomLeft'
				}]
			}, {
				Key: 'playbacks/600k/segment',
				PresetId: transcodeOpts['600k'],
				SegmentDuration: '5',
				Watermarks: [{
			        InputKey: 'watermark.png',
			        PresetWatermarkId: 'BottomLeft'
				}]
			}, {
				Key: 'playbacks/1000k/segment',
				PresetId: transcodeOpts['1000k'],
				SegmentDuration: '5',
				ThumbnailPattern: 'thumbnails/thumb-{count}',
				Watermarks: [{
			        InputKey: 'watermark.png',
			        PresetWatermarkId: 'BottomLeft'
				}]
			}],
			Playlists: [{
				Name: 'master',
				Format: 'HLSv3',
				OutputKeys:[
					'playbacks/400k/segment',
					'playbacks/600k/segment',
					'playbacks/1000k/segment'
				]
			}]
	    };

    transcoder.createJob(params, function(err, data) {
		
		if (err) throw new Error(err);

		debug('Transcoding started for ' + file.key);

		console.log('Transcoding: ', JSON.stringify(file));

		transcoder.waitFor('jobComplete', {
			Id: data.Job.Id
		}, function(err, data) {

			if (err) return logger.error(err);

			Video.get({
		        savedIn: file.savedIn,
		        vid: file.vid
		    }, function(err, video){

		    	if (err) throw new Error(err);

		    	debug('Transcode complete for  ' + file.name);

		    	if (video) {

		    		var newVideo = video.get();

			    	newVideo.state = 'public';
					newVideo.savedIn = newVideo.savedIn.replace('/draft', '');
					newVideo.status = 'live';
					newVideo.duration = data.Job.Output.DurationMillis;
					newVideo.transcodeId = data.Job.Id;

					var count = Math.round((((newVideo.duration / 1000) / 20) + 1) / 2);

					newVideo.thumbnail = file.prefix + 'thumbnails/thumb-'+ pad(count, 5) +'.jpg';

					Video.destroy({
						savedIn: file.savedIn,
						vid: file.vid
					}, function(err){
						
						if (err) throw new Error(err);

						Video.create(newVideo, function(err, data){

							if (err) throw new Error(err);

							io.sockets.emit(file.vid + ':transcode', {
								savedIn: data.get().savedIn
							});

							s3.listObjects({
								Bucket: config.get('buckets.output'),
								Prefix: file.prefix
							}, function(err, data){

								if (err) throw new Error(err);

								var keys = _.pluck(data.Contents, 'Key');

								var objs = _.map(keys, function(key){

									return function(cb){

										s3.headObject({
											Bucket: config.get('buckets.output'),
											Key: key,
										}, function(err, old){
											
											if (err) return cb(err);

											s3.copyObject({
												Bucket: config.get('buckets.output'),
												CopySource: config.get('buckets.output') + '/' + key,
												Key: key,
												CacheControl: 'max-age=31536000',
												ContentType: old.ContentType,
												MetadataDirective: 'REPLACE',
											}, function(err, data){
												
												if (err) return cb(err);

												return cb();

											});

										});

									};

								});

								async.parallel(objs, function(err, data){

									if (err) throw new Error(err);

									return done();

								});

							});

						});

					});

		    	} else {

		   //  		io.sockets.emit(file.vid + ':transcode', {
					// 	savedIn: data.get().savedIn
					// });

					return done();

		    	}

			});

		});


    });

};

module.exports = Transcoder;
