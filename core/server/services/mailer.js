var nodemailer = require('nodemailer'),
	fs = require('fs-extra'),
	doT = require('dot'),
	path = require('path'),
	mg = require('nodemailer-mailgun-transport'),
	transporter,
	merge = require('mout/object/merge'),
	files = {
		'login': file('login'),
		'contact': file('contact')
	},
	auth = {
		auth: config.get('mailgun')
	};

if (process.env.NODE_ENV === 'production') {

	transporter = nodemailer.createTransport(mg(auth));

} else {

	transporter = nodemailer.createTransport({
		host: '127.0.0.1',
		port: 2555
	});

}

function file(name) {
	var file = fs.readFileSync(path.join(__dirname, '../email-templates/' + name + '.html'), 'utf8');
	
	return doT.template(file);
}

var Mailer = function(data, callback){
	
	var obj = merge(data, {
		siteUrl: config.get('url')
	});


	transporter.sendMail({
	    from: 'no-reply@' + config.get('domain'),
	    to: data.to,
	    subject: data.subject,
	    html: files[data.fileName](obj)
	}, callback);
	
};

module.exports = Mailer;