var BusBoy = require('busboy'),
    Path = require('path'),
    base64url = require('base64url'),
    createHash = require('crypto').createHash,
    Buffer = require('buffer').Buffer,
    fs = require('fs-extra'),
    AWS = require('aws-sdk'),
    moment = require('moment'),
    Video = require('../models/Video'),
    getParams = require('../utils/params'),
    s3 = new AWS.S3(config.get('aws.s3'));

function dathBusBoy(req, res, next) {
    var busboy,
        stream,
        tmpDir;

    // busboy is only used for POST requests
    if (req.method && !/post/i.test(req.method.toLowerCase())) {
        return next();
    }

    if (!/multipart/i.test(req.headers['content-type'])) return next();

    var params = getParams(req.params.id);

    if (!params.vid && !params.savedIn) return next();

    function parsePath(path) {
        var extname = Path.extname(path);
        return {
            dirname: Path.dirname(path),
            basename: Path.basename(path, extname),
            extname: extname
        };
    }

    busboy = new BusBoy({
        headers: req.headers
    });

    var bufs = [],
        fileObj = {
            size: 0,
            buffer: null,
            newName: '',
            fieldname: '',
            mimetype: '',
            encoding: ''
        },
        newfilename,
        d,
        path;

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

        // If the filename is invalid, skip the stream
        if (!filename) {
            return file.resume();
        }
        // console.log(file.stat);

        file.on('data', function(data) {
            
            bufs.push(data);

            fileObj.size += data.length;

        });

        file.on('end', function() {

            fileObj.buffer = Buffer.concat(bufs, fileObj.size);

            var hash = createHash('md5').update(fileObj.buffer, null);

            hash.update('' + Date.now(), 'utf8');

            d = hash.digest('base64');
            newfilename = base64url.fromBase64(d);
            path = parsePath(filename);
            fileObj.name = (newfilename + path.extname);
            fileObj.fieldname = fieldname;
            fileObj.mimetype = mimetype;
            fileObj.encoding = encoding;
            fileObj.md5 = newfilename;

        });

        file.on('error', function(error) {

            console.log('Error', 'Something went wrong uploading the file', error);

        });

    });

    busboy.on('error', function(error) {
        console.log('Error', 'Something went wrong parsing the form', error);
        res.status(500).send({
            code: 500,
            message: 'Could not parse upload completely.'
        });
    });

    busboy.on('field', function(fieldname, val) {
        req.body[fieldname] = val;
    });

    busboy.on('finish', function() {

        Video.get({
            vid: params.vid,
            savedIn: (req.body.live) ? params.savedIn : '/draft' + params.savedIn,
        }, function(err, data){

            if (err) return next(err);

            if (!data) return next();

            var video = data.get();
        
            if (process.env.NODE_ENV === 'production') {

                fileObj.name = moment().format('YYYY[/]MM[/]') + fileObj.name;

                if (video.poster) {

                    video.poster = JSON.parse(video.poster);

                    var params = {
                        Bucket: config.get('buckets.assets'),
                        Key: 'posters/' + video.poster.src
                    };

                    return s3.headObject(params, function (err, metadata) {  
                        if (err && err.code === 'NotFound') {  

                            s3.putObject({
                                Bucket: config.get('buckets.assets'),
                                Key: 'posters/' + fileObj.name,
                                Body: fileObj.buffer,
                                ACL: 'public-read',
                                ContentType: fileObj.mimetype,
                                CacheControl: 'max-age=31536000'
                            }, function (err, data) {
                                if (err) return next(err);
                                req.file = {
                                    fileId: fileObj.name,
                                    md5: fileObj.md5
                                };
                                next();
                            });
                              
                        } else {

                            if (fileObj.name === video.poster.src) return next();

                            s3.deleteObject(params, function(err, data) {
                                if (err) return next(err);

                                s3.putObject({
                                    Bucket: config.get('buckets.assets'),
                                    Key: 'posters/' + fileObj.name,
                                    Body: fileObj.buffer,
                                    ACL: 'public-read',
                                    ContentType: fileObj.mimetype,
                                    CacheControl: 'max-age=31536000'
                                }, function (err, data) {
                                    if (err) return next(err);
                                    req.file = {
                                        fileId: fileObj.name,
                                        md5: fileObj.md5
                                    };
                                    next();
                                });

                            });                            

                        }
                    });

                }

                return s3.putObject({
                    Bucket: config.get('buckets.assets'),
                    Key: 'posters/' + fileObj.name,
                    Body: fileObj.buffer,
                    ACL: 'public-read',
                    ContentType: fileObj.mimetype,
                    CacheControl: 'max-age=31536000'
                }, function (err, data) {
                    if (err) return next(err);
                    req.file = {
                        fileId: fileObj.name,
                        md5: fileObj.md5
                    };
                    next();
                });

            } else {

                var oldPoster,
                    newPoster = Path.join(__dirname, '../../../content/posters/', fileObj.name);

                if (video.poster) {

                    video.poster = JSON.parse(video.poster);

                    oldPoster = Path.join(__dirname, '../../../content/posters/', video.poster.src);

                }

                if (oldPoster && fs.existsSync(oldPoster) && fileObj.name !== video.poster.src) {

                    fs.unlinkSync(oldPoster);

                }

                if (fs.existsSync(newPoster) && fileObj.name === video.poster.src) {

                    return next();

                }

                fs.writeFile(newPoster, fileObj.buffer, function() {
                    req.file = {
                        fileId: fileObj.name,
                        md5: fileObj.md5
                    };
                    next();
                });

            }

        });

    });

    req.pipe(busboy);
}

module.exports = dathBusBoy;
