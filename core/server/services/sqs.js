var AWS = require('aws-sdk'),
    opts = {
        accessKeyId: config.get('aws.sqs.accessKeyId'),
        secretAccessKey: config.get('aws.sqs.secretAccessKey'),
        region: config.get('aws.sqs.region'),
        apiVersion: '2012-11-05',
        endpoint: new AWS.Endpoint(config.get('queueEndpoint'))
    };

if (process.env.NODE_ENV === 'production') delete opts.endpoint;

module.exports = new AWS.SQS(opts);