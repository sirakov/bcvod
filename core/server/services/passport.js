var passport = require('passport'),
	User = require('../models/User'),
	params = require('../utils/userParams'),
	TwitterStrategy = require('passport-twitter').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
	uuid = require('../utils/uuid'),
	moment = require('moment');


passport.use(new TwitterStrategy({
    consumerKey: config.get('auth.twitter.consumerKey'),
    consumerSecret:  config.get('auth.twitter.consumerSecret'),
    callbackURL:  config.get('auth.twitter.callbackURL'),
	userProfileURL: config.get('auth.twitter.userProfileURL')
  },
  function(token, tokenSecret, profile, done) {
	
	User.query(profile._json.email)
		.limit(1)
		.usingIndex('EmailIndex')
		.exec(function(err, data){

			if (err) return done(err, null);

			if (data.Items.length) {

				var user = data.Items[0].get();

				return done(null, user);

			} else {

				User.create({
					uid: uuid(12),
					savedIn: moment().format('[/]YYYY[/]MM'),
					type: '/twitter/' + profile.id,
					username: profile._json['screen_name'],
					email: profile._json.email,
					fullname: profile._json.name,
					location: profile._json.location,
					avatar: profile._json['profile_image_url_https'],
					startedAt: Date.now(),
					active: true
				}, function(err, user){

					if (err) return done(err, null);

					return done(null, user.get());

				})

			}

		});

  }
));

passport.use(new FacebookStrategy({
    clientID: config.get('auth.facebook.clientID'),
    clientSecret: config.get('auth.facebook.clientSecret'),
    callbackURL: config.get('auth.facebook.callbackURL'),
    enableProof: false,
    profileFields: ['id', 'displayName', 'link', 'photos', 'email']
  },
  function(accessToken, refreshToken, profile, done) {

  	// console.log(profile);

  	// return done();
    
	User.query(profile._json.email)
		.limit(1)
		.usingIndex('EmailIndex')
		.exec(function(err, data){

			if (err) return done(err, null);

			if (data.Items.length) {

				var user = data.Items[0].get();

				return done(null, user);

			} else {

				User.create({
					uid: uuid(12),
					savedIn: moment().format('[/]YYYY[/]MM'),
					type: '/facebook/' + profile.id,
					fullname: profile._json.name,
					email: profile._json.email,
					avatar: profile._json['picture'].data.url,
					startedAt: Date.now(),
					active: true
				}, function(err, user){

					if (err) return done(err, null);

					return done(null, user.get());

				})

			}

		});
  }
));

passport.serializeUser(function(user, cb) {

	var uid = params({
		savedIn: user.savedIn,
		uid: user.uid
	});

	return cb(null, uid);
});

passport.deserializeUser(function(uid, cb) {

	var us = params(uid);

	User.get({
		savedIn: us.savedIn,
		uid: us.uid
	}, function(err, user){

		if (err) return cb(err, null);

		if (!user) return cb('Error: not found', null);

		var user = user.get();

		user.uid = params({
			savedIn: user.savedIn,
			uid: user.uid
		});

		return cb(null, user);

	})
});

module.exports = passport;