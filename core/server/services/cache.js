var cache = require('rediscache');

cache.connect(config.get('redis.port'), config.get('redis.host')).configure({
    expiry: 90
});

module.exports = cache;