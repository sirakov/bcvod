var Token = require('../models/Token'),
	bcrypt = require('bcryptjs');

function DynamoStore(){};

 DynamoStore.prototype.storeOrUpdate = function(token, uid, msToLive, originUrl, callback) {

 	if(!token || !uid || !msToLive || !callback) {
		throw new Error('TokenStore:storeOrUpdate called with invalid parameters');
	}
	
	if (!originUrl) originUrl = null;

 	return Token
 				.create({
			 		uid: uid,
			 		undef: 0,
			 		ttl: Date.now() + msToLive + '',
			 		originUrl: originUrl,
			 		token: token
			 	}, function(err, data){
			 		if (err) return callback(err);

			 		return callback();
			 	});
 };

 DynamoStore.prototype.authenticate = function(token, uid, callback) {

 	if(!token || !uid || !callback) {
		throw new Error('TokenStore:authenticate called with invalid parameters');
	}

	return Token.query(uid)
				.where('ttl')
				.gt(Date.now())
				.usingIndex('uid-ttl-index')
				.exec(function(err, data){
					if (err) {
						return callback(err, false);
					} else if (!data.Items.length) {
		              return callback(null, false, null);
		            } else {
		            	var ref = data.Items[0];

		            	bcrypt.compare(token, ref.get('token'), function(err, valid){
		            		if (err) {
			                  return callback(err, false);
			                } else if (valid) {
			                  return callback(null, true, ref.get('originUrl'));
			                } else {
			                  return callback(null, false);
			                }
		            	});
		            }

				});
 };

 DynamoStore.prototype.invalidateUser = function(uid, callback) {

 	if(!uid || !callback) {
		throw new Error('TokenStore:invalidateUser called with invalid parameters');
	}

 	return Token.destroy(uid, 0, function(err){
			 		if (err) return callback(err);

			 		return callback();

			 	});

 };

 DynamoStore.prototype.clear = function(callback) {

 	if(!callback) {
		throw new Error('TokenStore:clear called with invalid parameters');
	}

 	return Token.deleteTable(function(err){
 		if (err) return callback(err);

 		Token.createTable(function(err, data){

 			if (err) return callback(err);

 			// console.log(data);
 			// 
 			Token.describeTable(function(err, resp){
 				if (err) return callback(err);

 				if (resp.Table.TableStatus === 'ACTIVE') return callback();

 			})

 		});
 	});

 };

 DynamoStore.prototype.length = function(callback) {

 	Token.scan().select('COUNT').exec(function(err, data){

 		if (err) return callback(err);

 		callback(null, data.Count);

 	});
 };

module.exports = DynamoStore;