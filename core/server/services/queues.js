var env = (process.env.NODE_ENV === 'production') ? 'remote': 'local';

var Queues = {
	s3: require('./jobs/'+ env +'/s3'),
	transcoder: require('./jobs/'+ env +'/transcoder'),
	delete: require('./jobs/'+ env +'/delete'),
	views: require('./jobs/' + env + '/views'),
	history: require('./jobs/' + env + '/history'),
	restore: require('./jobs/' + env + '/restore')
};

module.exports = Queues;