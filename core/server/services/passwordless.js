var passwordless = require('passwordless'),
    isProduction = process.env.NODE_ENV === 'production',
	Store = require((isProduction) ? 'passwordless-redisstore' : 'passwordless-redisstore-bcryptjs'),
	Mailer = require('./mailer'),
    checkAdmin = require('../utils/checkAdmin');

passwordless.init(new Store(config.get('redis.port'), config.get('redis.host')),{
    skipForceSessionSave: true,
    userProperty: 'adminUser'
});

passwordless.addDelivery(
    function(token, uid, email, callback) {

        var loginUrl,
            isAdmin = checkAdmin(uid);

        if (isAdmin) {

            loginUrl = config.get('url') + '/admin?token=' + token+ '&uid=' 
            + encodeURIComponent(uid);

        } else {

            loginUrl = config.get('url') + '/login?token=' + token+ '&uid=' 
            + encodeURIComponent(uid);

        }
        
        Mailer({
            loginUrl:   loginUrl , 
            to:      email,
            subject: 'KukuoTv Login',
            fileName: 'login'
        });

        return callback();
}, {
    ttl: 4 * 60 * 1000
});


module.exports = passwordless;