var db = require('../connections/dynamo'),
    pseudoRandomBytes = require('crypto').pseudoRandomBytes,
	joi = require('joi');


var UserLikes = db.define('UserLikes', {
    hashKey: 'user',
    rangeKey: 'video',
    tableName: 'user_likes',
    timestamps: true,
    schema:{
        user: joi.string(),
        video: joi.string()
    },
    indexes : []
});

module.exports = UserLikes;