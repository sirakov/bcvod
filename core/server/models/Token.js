var db = require('../connections/dynamo'),
    bcrypt = require('bcryptjs'),
    pseudoRandomBytes = require('crypto').pseudoRandomBytes,
	joi = require('joi');


var Token = db.define('Token', {
    hashKey: 'uid',
    rangeKey: 'undef',
    schema:{
        uid: joi.string(),
        token: joi.string(),
        undef: joi.number().default(0),
        ttl: joi.number(),
        originUrl: joi.string().allow(null)
    },
    indexes : [{
        hashKey : 'uid',
        rangeKey : 'ttl',
        type : 'local',
        name : 'uid-ttl-index',
        projection: { ProjectionType: 'ALL' }
    }]
});

Token.before('create', function(data, next){

    return bcrypt.hash(data.token, 10, function(err, hashedToken) {
        if (err) {

            return next(err, null);

        } else {

            data.token = hashedToken;

            return next(null, data);
        }
    });

});

Token.before('update', function(data, next){

    return bcrypt.hash(data.token, 10, function(err, hashedToken) {
        if (err) {

            return next(err, null);
            
        } else {

            data.token = hashedToken;

            return next(null, data);
        }
    });

});

module.exports = Token;