var db = require('../connections/dynamo'),
    bcrypt = require('bcryptjs'),
    pseudoRandomBytes = require('crypto').pseudoRandomBytes,
	joi = require('joi');


var UserHistory = db.define('UserHistory', {
    hashKey: 'user',
    rangeKey: 'video',
    tableName: 'user_history',
    timestamps: true,
    schema:{
        user: joi.string(),
        video: joi.string(),
        idt: joi.number()
    },
    indexes : [{
        hashKey : 'user',
        rangeKey: 'idt',
        type : 'local',
        name : 'TimeIndex',
        projection: {ProjectionType: 'ALL'}
    }]
});

module.exports = UserHistory;