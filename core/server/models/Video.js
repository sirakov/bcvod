var db = require('../connections/dynamo'),
	joi = require('joi'),
    Promise = require('bluebird'),
    async = require('async'),
    moment = require('moment'),
    merge = require('mout/object/merge'),
    UserLikes = require('./UserLikes'),
    params = require('../utils/params'),
    autoKeyWord = require('auto-keywords'),
    CatGenres = _.sortBy(require('../../client/components/category/genres'), function(genre){
        return genre.toLowerCase();
    }),
    CatMusicGenres = _.sortBy(require('../../client/components/category/musicGenres'), function(genre){
        return genre.toLowerCase();
    });

var Video = db.define('Video', {
    hashKey: 'savedIn',
    rangeKey: 'vid',
    timestamps: true,
    schema: {
    	vid: joi.string(),
        savedIn: joi.string(),
        state: joi.string(),
        oFileName: joi.string().allow(null),
        oFile: joi.string().allow(null),
        s3FileName: joi.string().allow(null),
        status: joi.string(),
        size: joi.number().allow(null),
        title: joi.string().allow(null),
        description: joi.string().allow(null),
        description_long: joi.string().allow(null),
        category: joi.string().allow(null),
        idt: joi.number().allow(null),
        poster: joi.string().allow(null),
        likes: joi.number().allow(null),
        views: joi.number().allow(null),
        viewLikes: joi.string().allow(null),
        origin: joi.string().allow(null),
        year: joi.string().allow(null),
        language: joi.string().allow(null),
        genres: db.types.stringSet(),
        sequel: joi.string().allow(null),
        episode: joi.boolean().allow(null),
        likedBy: db.types.stringSet(),
        keywords: db.types.stringSet(),
        oldVid: joi.string().allow(null),
        duration: joi.number().allow(null),
        thumbnail: joi.string().allow(null),
        transcodeId: joi.string().allow(null)
	},
    indexes : [{
        hashKey : 'oFileName',
        rangeKey: 'idt',
        type : 'global',
        name : 'FileIndex',
        projection: { NonKeyAttributes: [ 'vid' ], ProjectionType: 'INCLUDE' }
    },{
        hashKey : 'state',
        rangeKey: 'idt',
        type : 'global',
        name : 'StateIndex',
        projection: { ProjectionType: 'KEYS_ONLY' }
    },{
        hashKey : 'category',
        rangeKey : 'idt',
        type : 'global',
        name : 'CategoryIndex',
        readCapacity: 5,
        writeCapacity: 5,
        projection: { ProjectionType: 'ALL' }
    },{
        hashKey : 'savedIn',
        rangeKey : 'idt',
        type : 'local',
        name : 'SavedInIndex',
        projection: { ProjectionType: 'ALL' }
    },{
        hashKey : 'savedIn',
        rangeKey : 'views',
        type : 'local',
        name : 'ViewsIndex',
        projection: {ProjectionType: 'ALL' }
    },{
        hashKey : 'savedIn',
        rangeKey : 'viewLikes',
        type : 'local',
        name : 'RecommendIndex',
        projection: {ProjectionType: 'ALL' }
    },{
        hashKey : 'state',
        rangeKey : 'views',
        type : 'global',
        name : 'state-views-index',
        projection: {ProjectionType: 'ALL' }
    }]
});

Video.episode = function(obj){

    return new Promise(function(resolve, reject){

        if (!obj.savedIn && !obj.vid) return resolve();

        Video.get({
            savedIn: obj.savedIn,
            vid: obj.vid
        }, function(err, episode){

            var episode = (episode) ? episode.get() : {};

            if (err) return reject(err);

            if (!Object.keys(episode).length) return resolve(null);

            episode.episodes = obj.episodes;

            return resolve(episode);

        });

    });

};

Video.popular = function(){

    return new Promise(function(resolve, reject){

        Video.query('public').limit(10).descending().usingIndex('state-views-index').exec(function(err, data){

            if (err)  return reject(err);

            return resolve(_.pluck(data.Items, 'attrs'));

        });

    });

};

Video.incLikes = function(video){

    return new Promise(function(resolve, reject){

        Video.get({
             savedIn: video.savedIn,
             vid: video.vid
        }, {ConsistentRead: true, AttributesToGet : ['likes','views']}, function(err, vid){

             if (err) return reject(err);

             var likes = vid.get('likes') || 0,
                 views = vid.get('views') || 0,
                 newLikes = likes + 1;

             Video.update({
                 savedIn: video.savedIn,
                 vid: video.vid,
                 likes: newLikes,
                 viewLikes: views + '/' + newLikes 
             }, function(err, data){

                 if (err) return reject(err);

                 return resolve();

             });

        });

    });

};

Video.decLikes = function(video){

    return new Promise(function(resolve, reject){

        Video.get({
             savedIn: video.savedIn,
             vid: video.vid
         }, {ConsistentRead: true, AttributesToGet : ['likes','views']}, function(err, vid){

             if (err) return reject(err);

             var likes = vid.get('likes') || 0,
                 views = vid.get('views') || 0,
                 newLikes = (likes > 0) ? likes - 1 : 0;

             Video.update({
                 savedIn: video.savedIn,
                 vid: video.vid,
                 likes: newLikes,
                 viewLikes: views + '/' + newLikes 
             }, function(err, data){

                 if (err) return reject(err);

                 return resolve();

             })

         });

    });

};

Video.search = function(q, last){

    return new Promise(function(resolve, reject){

        if (!q) return resolve({videos: []});

        var keywords = autoKeyWord(q);

        if (!keywords.length) return resolve({videos: []});

        Video.latest({
            limit: 12,
            last: last,
            filters:{
                keywords: keywords
            }
        }).then(function(videos){

            return resolve(videos);

        }).catch(function(err){

            return reject(err);

        });

    });

};

Video.getGenres = function(){
   return new Promise(function(resolve, reject){

        var genres = ['Romance', 'Afro-Pop', 'Drama', 'Gospel', 'Hiphop'];

        genres = _.map(genres, function(genre){

                return function(callback){
                    Video.latest({
                        limit: 2,
                        filters:{
                            genres: genre
                        }
                    }).then(function(data){
                        
                        var toReturn;

                        if (data.videos.length) toReturn = _.pluck(data.videos, 'attrs')[0];
                        else toReturn = null;

                        if (toReturn) toReturn.type = genre;

                        return callback(null, toReturn);

                    }).catch(function(err){

                        return callback(err, null);

                    });
                };

            });

        async.parallel(genres, function(err, results){
            
            if (err) return reject(err);

            return resolve(results);

        });

   }); 
};

Video.next = function(obj){

    return new Promise(function(resolve, reject){

        if (obj.isSeries || obj.sequel) return resolve(null);
        
        var category = obj.category;

        delete obj.category;

        var filters = {};

        if (obj.genres) {


            filters = {
                genres: obj.genres
            };

        }

        delete obj.genres;

        Video.latest({
            limit: 1,
            last: obj,
            filters: filters
        }).then(function(data){

            if (!data.videos.length) {

                var nextVideo,
                    cont = true;

                if (!category) return resolve(null);

                async.whilst(function(){
                    return !nextVideo && cont;
                }, function(cb){

                    var Me = new RegExp('music', 'g'),
                        isMusic = Me.test(category);

                    if (isMusic && filters.genres) {

                        var genreIndex = CatMusicGenres.indexOf(filters.genres);

                        if (genreIndex > -1 && genreIndex !== (CatMusicGenres.length - 1)) {

                            filters.genres = CatMusicGenres[genreIndex + 1];

                            if ((genreIndex + 1) === (CatMusicGenres.length - 1)){

                                cont = false;

                            }

                        }

                    } else if (filters.genres) {

                        var genreIndex = CatGenres.indexOf(filters.genres);

                        if (genreIndex > -1 && genreIndex !== (CatGenres.length - 1)) {

                            filters.genres = CatGenres[genreIndex + 1];

                            if ((genreIndex + 1) === (CatGenres.length - 1)){

                                cont = false;

                            }

                        }

                    } else {

                        cont = false;

                        return cb();

                    }

                    Video.latest({
                        limit: 1,
                        last: obj,
                        filters: filters
                    }).then(function(data){

                        if (!data.videos.length) {

                            cb();

                            return null;

                        };

                        var next = _.pluck(data.videos, 'attrs')[0],
                            nextId = params({
                                vid: next.vid,
                                savedIn: next.savedIn
                            });

                        nextVideo = nextId;

                        cont = false;

                        cb();

                        return null;

                    }).catch(function(err){

                        return cb(err);

                    });



                }, function(err){

                    if (err) return reject(err);

                    return resolve(nextVideo);

                });


            } else {

                var next = _.pluck(data.videos, 'attrs')[0],
                    nextId = params({
                        vid: next.vid,
                        savedIn: next.savedIn
                    });

                return resolve(nextId);

            }

            return null;


        }).catch(function(err){

            return reject(err);

        });

    });

};

Video.latest = function(opts){

    return new Promise(function(resolve, reject){
        var now = moment(),
            videos = [],
            LIMIT = config.get('db_limit'),
            FIRST = config.get('first_month'),
            last = opts.last,
            savedIn = (last && last.savedIn) ? last.savedIn.replace('/draft', '') : now.format('[/]YYYY[/]MM');
            cont = true,
            limit = opts.limit || LIMIT;

        return async.whilst(
            function(){
                return (videos.length < (opts.limit || limit)) && cont;
            },
            function(cb){

                var QUERY,
                    vidLimit = (opts.limit - videos.length),
                    kue = (opts.draft) ? '/draft' + savedIn :  savedIn;

                // console.log(savedIn, opts.type, vidLimit, videos.length);

                QUERY = Video.query((opts.prefix) ? opts.prefix + kue :  kue);

                // console.log(opts.filters);

                // console.log(last);
                // 
                // console.log(opts.filters);

                if (opts.filters && Object.keys(opts.filters).length) {

                    for(key in opts.filters){

                        if (key === 'keywords') {

                            _.forEach(opts.filters[key], function(keyword){

                                QUERY = QUERY.filter(key).contains(keyword);

                            });

                        } else if (key === 'category') {

                            QUERY = QUERY.filter(key).beginsWith('/' + opts.filters[key]);

                        } else if (_.contains(['genres', 'title', 'description'], key)) {

                            QUERY = QUERY.filter(key).contains(opts.filters[key]);

                        } else {

                            QUERY = QUERY.filter(key).equals(opts.filters[key]);

                        }

                    }

                }

                if (last && Object.keys(last).length) {

                    if (last.idt) last.idt = parseInt(last.idt);

                    QUERY = QUERY.startKey(last);

                }

                var Index,
                    AttrsToGet;

                Index = (opts.prefix) ? 'CategoryIndex' :'SavedInIndex';

                AttrsToGet = [
                    'vid',
                    'poster',
                    'title',
                    'description',
                    'category',
                    'idt',
                    'savedIn',
                    'status',
                    'origin',
                    'year',
                    'language',
                    'genres'
                ];

                if (Index !== 'CategoryIndex') {

                    AttrsToGet.push('views');
                    AttrsToGet.push('thumbnail');

                }

                if (Index === 'SavedInIndex') {

                    AttrsToGet.push('state');

                }

                if (opts.index) {
                    
                    Index = opts.index;

                    AttrsToGet = [
                        'vid',
                        'poster',
                        'title',
                        'category',
                        'description',
                        'idt',
                        'savedIn',
                        'status',
                        'origin',
                        'year',
                        'language',
                        'genres',
                        'viewLikes',
                        'views'
                    ];
                }

                // console.log(opts, Index);

                QUERY = QUERY
                .limit(vidLimit)
                .attributes(AttrsToGet)
                .usingIndex(Index);

                if (!opts.next) QUERY = QUERY.descending();
                else QUERY = QUERY.ascending();
                
                QUERY.exec(function(err, data){

                    if (err) return cb(err);

                    var check = (data.Count + videos.length) < (opts.limit || limit);

                    if (savedIn === FIRST) cont = false;


                    if ((data.Count === 0 && !data.LastEvaluatedKey && cont) || (check && cont)) {


                        if (data.LastEvaluatedKey && opts.filters && Object.keys(opts.filters).length) {

                            last = data.LastEvaluatedKey;

                            // console.log(savedIn, last);

                        } else {

                            var spl = savedIn.split('/');

                            spl.shift();

                            savedIn = moment(spl.join('-') + '-01').subtract(1, 'month').format('[/]YYYY[/]MM');

                            last = undefined;

                            // if (opts.type && opts.type === 'recently') console.log(videos.length, data, savedIn, videos.length, check, cont);

                        }

                    } else {

                        if (data.LastEvaluatedKey) {

                            last = data.LastEvaluatedKey;

                        } else {

                            last = undefined;

                        }

                    }

                    videos = videos.concat(data.Items);

                    return cb();

                });
            },
            function(err){

                if (err) return reject(err);

                return resolve({
                    last: last,
                    videos: videos
                });

            }
        );
    });

};

Video.getLiked = function(obj){
    return new Promise(function(resolve, reject){

        if (!obj.user) return resolve(null);

        Video.query(obj.vid)
                .where('vid').beginsWith('/likes')
                .filter('likedBy').contains(obj.user)
                .exec(function(err, liked){

                    if (err) return reject(err);

                    if (liked.Items.length) return resolve(true);
                    else return resolve(null);

                });

        // UserLikes.get({
        //     user: obj.user.split('-').join('/'),
        //     video: obj.vid
        // }, function(err, liked){

        //     if (err) return reject(err);

        //     return resolve((liked) ? liked.get() : null);

        // })

    });
};

Video.episodes = function(vid, attrs){

    if (!attrs) attrs = ['vid'];
    
    return new Promise(function(resolve, reject){

        Video.query(vid)
             .attributes(attrs)
             .where('vid').beginsWith('/episode')
             .exec(function(err, data){

                if (err) return reject(err);

                var videos = _.map(_.pluck(data.Items, 'attrs'), function(video){

                    video.episodeId = parseInt(video.vid.replace('/episode/', ''));

                    return video;

                });

                return resolve(_.sortBy(videos, function(o){
                    return o.episodeId;
                }));

        });

    });
};

Video.destroyLikes = function(vid){

    return new Promise(function(resolve, reject){

        Video.query(vid).where('vid').beginsWith('/likes').exec(function(err, data){

            if (err) return reject(err);

            var videos = _.map(_.pluck(data.Items, 'attrs'), function(item){

                return function(callback){

                    Video.destroy({
                        savedIn: item.savedIn,
                        vid: item.vid
                    }, function(err){

                        if (err) return callback(err, null);

                        return callback(null, null);

                    });

                };

            });

            async.parallel(videos, function(err, results){
            
                if (err) return reject(err);

                return resolve(results);

            });

        });

    });

};

Video.counts = function() {
    return new Promise(function(resolve, reject){
        return async.series([
            function(callback){
                Video.query('draft')
                    .select('COUNT')
                    .usingIndex('StateIndex')
                    .exec(function(err, data){

                        if (err) return callback(err, null);

                        return callback(null, data.Count);

                    });
            },
            function(callback){
                Video.query('public')
                    .select('COUNT')
                    .usingIndex('StateIndex')
                    .exec(function(err, data){

                        if (err) return callback(err, null);

                        return callback(null, data.Count);

                    });
            }
        ],
        // optional callback
        function(err, results){
            
                
            if (err) return reject(err);

            return resolve(results);

        });
    });
};

module.exports = Video;