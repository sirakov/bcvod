var db = require('../connections/dynamo'),
	joi = require('joi'),
    Promise = require('bluebird');

var Admin = db.define('Admin', {
    hashKey: 'savedIn',
    rangeKey: 'uid',
    timestamps: true,
    schema: {
    	uid: joi.string(),
        email: joi.string(),
        master: joi.boolean(),
        savedIn: joi.string(),
        idt: joi.number()
	},
    indexes : [{
        hashKey : 'email',
        rangeKey: 'uid',
        type : 'global',
        name : 'EmailIndex',
        projection: { NonKeyAttributes: [ 'uid', 'savedIn' ], ProjectionType: 'INCLUDE' }
    }, {
        hashKey : 'savedIn',
        rangeKey : 'idt',
        type : 'local',
        name : 'SavedIndex',
        projection: { ProjectionType: 'ALL' }
    }]
});

module.exports = Admin;