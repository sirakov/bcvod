var db = require('../connections/dynamo'),
	joi = require('joi'),
    Promise = require('bluebird');

var User = db.define('User', {
    hashKey: 'savedIn',
    rangeKey: 'uid',
    timestamps: true,
    schema: {
    	uid: joi.string(),
    	savedIn: joi.string(),
        type: joi.string().allow(null),
        username: joi.string().allow(null),
        email: joi.string().allow(null),
        fullname: joi.string().allow(null),
        bio: joi.string().allow(null),
    	avatar: joi.string().allow(null),
        location: joi.string().allow(null),
        startedAt: joi.number().allow(null),
        active: joi.boolean().allow(null),
        blocked: joi.boolean().allow(null)
	},
    indexes : [{
        hashKey : 'email',
        rangeKey: 'startedAt',
        type : 'global',
        name : 'EmailIndex',
        projection: { NonKeyAttributes: [ 'uid' ], ProjectionType: 'INCLUDE' }
    }]
});

module.exports = User;