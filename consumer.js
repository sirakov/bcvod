GLOBAL._ = require('lodash');
GLOBAL.logger = require('winston');

require('./core/server/env')();

GLOBAL.package = require('fs-extra').readJsonSync(__dirname + '/package.json');

GLOBAL.GLOBALS = {};

GLOBAL.config = require('config');

var async = require('async'),
    sqs = require('./core/server/services/sqs'),
    socket = require('./core/server/services/socket'),
    server = socket.server,
    io = socket.io,
    QueueList = require('./core/server/services/queues'),
    Consumer = require('sqs-consumer'),
    fs = require('fs-extra'),
    uuid = require('./core/server/utils/uuid'),
    Video = require('./core/server/models/Video'),
    setParams = require('./core/server/utils/params'),
    moment = require('moment'),
    Path = require('path'),
    async = require('async'),
    find = require('mout/object/find'),
    Tmp = config.get('tmp_dir'),
    shasum = require('shasum'),
    base64url = require('base64url'),
    Upload = require('./core/server/services/addQueue'),
    AWS = require('aws-sdk'),
    s3 = new AWS.S3(config.get('aws.s3')),
    Files = {};

var Queues = _.map(config.get('queues'), function(queue){

    return function(cb){

        sqs.createQueue({
            QueueName: queue + 'Queue'
        }, function(err, data) {

            if (err) return cb(err);

            logger.info(queue + 'Queue consumer created or started');
         
            var Queue = Consumer.create({
                queueUrl: config.get('queueEndpoint') + '/' + queue + 'Queue',
                handleMessage: function (message, done) {

                    // console.log(JSON.parse(message.Body));

                    // return done();
                    
                    // var domain = require('domain').create();

                    // domain.on('error', function(err){
                    //     return done(err);
                    // });

                    // domain.run(function(){
                        // console.log(queue);

                        return QueueList[queue](JSON.parse(message.Body), done);

                    // });

                },
                sqs: sqs
            });
             
            Queue.on('error', function (err) {
                return logger.error(err.message);
            });
             
            Queue.start();

        });

    };

});

async.parallel(Queues, function(err){

    if (err) return logger.error(err, err.stack);

});

io.sockets.on('connection', function(socket){

    socket.on('start', function (data) {

        var name = base64url.fromBase64(shasum(data.name, 'md5', 'base64')) + Path.extname(data.name),
            oFile = data.name;
            size = data.size,
            place = 0;

        // console.log(data.size);

        return async.waterfall([
            function(cb){
                Video.query(name)
                    .limit(1)
                    .usingIndex('FileIndex')
                    .exec(function(err, video){
                        if (err) return cb(err, null);

                        if (video.Count) {

                            Video.get({
                                savedIn: video.Items[0].get('savedIn'),
                                vid: video.Items[0].get('vid')
                            }, function(err, video){

                                if (err) return cb(err, null);

                                return cb(null, (video) ? video.get(): null);

                            });

                        } else {

                            var uid = uuid(12),
                                savedIn = moment().format('[/]YYYY[/]MM');

                            Video.create({
                                savedIn: '/draft' + savedIn,
                                category: '/movies' + savedIn,
                                vid: uid,
                                state: 'draft',
                                title: oFile,
                                oFileName: name,
                                oFile: oFile,
                                size: data.size,
                                status: 'local',
                                idt: Date.now()
                            }, function(err, video){

                                if (err) return cb(err, null);

                                return cb(null, video.get());

                            })

                        }


                    });
            },
            function(file, cb){

                var fpath = Tmp + file.oFileName;

                if (fs.existsSync(fpath) && file.status === 'local') {

                    var Stat = fs.statSync(fpath);

                    if (Stat && Stat.isFile()) {

                        file.downloaded = Stat.size;

                        return cb(null, file);

                    } else {

                        return cb('Invalid File', null);
                    }

                } else {

                    file.downloaded = 0;

                    return cb(null, file);

                }

            },
            function(file, cb){

                if (file.status !== 'local') {

                    return cb(null, file);  

                } else {

                    fs.open(Tmp + file.oFileName, 'a', 0755, function(err, fd){

                        if (err) return cb(err, null);

                        file.handler = fd;

                        return cb(null, file);

                    });

                }

            }
        ], function(err, file){

            if (err) return logger.error(err);

            // console.log(file, file.downloaded === file.size);

            if (file.status === 's3' && !file.transcodeComplete) {

                // Upload('transcoder', file);

            } else if ((file.downloaded === file.size && file.status === 'local') || file.status !== 'local') {

                if (file.status === 'local' && !file.s3FileName) {

                    var File = {
                        size : file.size,
                        socketId : socket.id,
                        downloaded : file.downloaded,
                        handler: file.handler,
                        name: file.oFile,
                        oFileName: file.oFileName,
                        vid: file.vid,
                        savedIn: file.savedIn
                    };

                    Upload('s3', File);

                }

                return socket.emit('done', {
                    'percent': 100,
                    uploaded: file.size,
                    vid: file.vid,
                    meta: file
                });

            } else {

                // console.log('not done');

                Files[file.vid] = {
                    size : file.size,
                    socketId : socket.id,
                    downloaded : file.downloaded,
                    handler: file.handler,
                    name: file.oFile,
                    oFileName: file.oFileName,
                    vid: file.vid,
                    savedIn: file.savedIn
                };

                place = file.downloaded;

                var percent = (file.downloaded / file.size) * 100;

                socket.emit('setMeta', {
                    meta: file
                });

                socket.emit('moreData', {
                    'place' : place,
                    percent : percent ,
                    uploaded: file.downloaded,
                    name: file.oFile,
                    vid: file.vid
                });

            }

        });
    });

    socket.on('pause', function (data){
        var vid = data.vid;

        fs.closeSync(Files[vid].handler);

    });

    socket.on('cancel', function (socVid){
        var vid = socVid.vid,
            savedIn = socVid.savedIn;

        if (Files[vid]) {

            fs.closeSync(Files[vid].handler);

            fs.unlink(Tmp + Files[vid].oFileName, function () {

               Video.destroy({
                vid: vid,
                savedIn: savedIn
               }, function(err){

                    if (err) return logger.error(err);

                    if (socVid.poster) {

                        if (process.env.NODE_ENV === 'production') {

                            s3.deleteObject({
                                Bucket: config.get('buckets.assets'),
                                Key: 'posters/' + socVid.poster
                            }, function(err, data) {
                                if (err) return logger.error(err);

                                delete Files[vid];

                                return socket.emit('cancelDone', {vid: vid});

                            });

                        } else {

                            var posterUrl = Path.join(__dirname, './content/posters/', socVid.poster);

                            fs.unlinkSync(posterUrl);

                            delete Files[vid];

                            return socket.emit('cancelDone', {vid: vid});

                        }

                    }

               });

            });

        }

    });

    socket.on('upload', function (data){
        
        var vid = data.vid,
            File = Files[vid];

        // console.log(Files, File, vid);

        File.downloaded += data.buffer.length;

        if (File.downloaded === File.size) {

            // console.log(Files);

            fs.write(File.handler, data.buffer, 0, data.buffer.length, null, function(err, Writen){
                
                fs.closeSync(File.handler);

                Video.update({
                    savedIn: File.savedIn,
                    vid: vid,
                    status: 's3'
                }, function(err, video){

                    if (err) return logger.log(video);

                    Upload('s3', File);

                    delete Files[File.vid];

                    socket.emit('done', {
                        'percent': 100,
                        uploaded: File.size,
                        vid: File.vid
                    });

                });

            });

        } else {

            fs.write(File.handler, data.buffer, 0, data.buffer.length, null, function(err, Writen){
                
                var percent = (File.downloaded / File.size) * 100;

                // console.log(Files[name]);

                socket.emit('moreData', {
                    'percent': percent,
                    uploaded: File.downloaded,
                    name: File.name,
                    vid: vid,
                    savedIn: File.savedIn
                });
            });

        }
    });

    socket.on('disconnect', function(){

        // console.log(socket.id, Files);

        var file = find(Files, function(file){

            return file.socketId === socket.id;

        });

        // console.log(file);

        if (file) {

            fs.closeSync(file.handler);

            delete Files[file.vid];

        }

        // console.log(Files);
    })
    
});

server.listen(config.get('queue.port'), function(){

    console.log('listening on ' + config.get('queue.port'));

});
